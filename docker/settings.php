<?php
global $CONFIG;
if (!isset($CONFIG)) {
    $CONFIG = new stdClass;
}

$CONFIG->dbuser = getenv("DB_USER");
$CONFIG->dbpass = getenv("DB_PASS");
$CONFIG->dbname = getenv("DB_NAME");
$CONFIG->dbhost = getenv("DB_HOST");
$CONFIG->dbprefix = getenv("DB_PREFIX") ? getenv("DB_PREFIX") : "elgg_";

$CONFIG->dataroot = getenv("DATAROOT");

$CONFIG->broken_mta = false;
$CONFIG->db_disable_query_cache = false;
$CONFIG->minusername = 1;
$CONFIG->min_password_length = 8;
$CONFIG->exception_include = "";

$CONFIG->pleio = new \stdClass;
$CONFIG->pleio->client = getenv("PLEIO_CLIENT");
$CONFIG->pleio->secret = getenv("PLEIO_SECRET");
$CONFIG->pleio->url = getenv("PLEIO_URL");

$CONFIG->env = getenv("PLEIO_ENV");

$host = getenv("SMTP_DOMAIN");
if ($host) {
    $CONFIG->email_from = "noreply@" . $host;
}

if (getenv("BLOCK_MAIL")) {
    $CONFIG->block_mail = true;
}

if (getenv("MEMCACHE_ENABLED")) {
    $CONFIG->memcache = getenv("MEMCACHE_ENABLED");
    $CONFIG->memcache_prefix = getenv("MEMCACHE_PREFIX");
    $CONFIG->memcache_servers = [];

    if (getenv("MEMCACHE_SERVER_1")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_1"), 11211];
    }

    if (getenv("MEMCACHE_SERVER_2")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_2"), 11211];
    }

    if (getenv("MEMCACHE_SERVER_3")) {
        $CONFIG->memcache_servers[] = [getenv("MEMCACHE_SERVER_3"), 11211];
    }
}

$CONFIG->elasticsearch = ["hosts" => []];
$CONFIG->elasticsearch_index = getenv("ELASTIC_INDEX");

if (getenv("ELASTIC_SERVER_1")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_1");
}

if (getenv("ELASTIC_SERVER_2")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_2");
}

if (getenv("ELASTIC_SERVER_3")) {
    $CONFIG->elasticsearch["hosts"][] = getenv("ELASTIC_SERVER_3");
}

$CONFIG->amqp_host = getenv("AMQP_HOST");
$CONFIG->amqp_user = getenv("AMQP_USER");
$CONFIG->amqp_pass = getenv("AMQP_PASS");
$CONFIG->amqp_vhost = getenv("AMQP_VHOST");
$CONFIG->amqp_task_queue = getenv("AMQP_TASK_QUEUE") ? getenv("AMQP_TASK_QUEUE") : "task";
$CONFIG->amqp_log_queue = getenv("AMQP_LOG_QUEUE") ? getenv("AMQP_LOG_QUEUE") : "log";