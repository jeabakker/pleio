# Pleio

This codebase is the main Pleio repository. Installation of the dependencies is done through [Composer](https://getcomposer.org/).

## Installation

### Manually

This is the prefered workflow for developers on OSX. This is because mounting a volume inside a Docker container is slow and therefore not fun developing ;)

- Make sure [brew](https://brew.sh/) and [Sequel Pro](https://sequelpro.com/) is installed.
- Install composer, MariaDB, Apache2.4, PHP and Memcached using brew:

```
    brew install composer mariadb httpd24 libmemcached
    brew install php@7.1
    brew link --force php@7.1
    brew services start mariadb
    brew services restart httpd24
```

- Clone the repository on your local machine.

```
    cd ~/dev
    git clone git@github.com:Pleio/pleio.git
    cd pleio
```

- Install all the PHP dependencies using Composer:

```
    composer install
```

- Install memcached from the pleio directory

```
    echo 'no' | pecl install memcached
```

- Use Sequal Pro to create a new username and password for pleio and create a new database Pleio and restore the database dump in `docker/database.sql` to your database.
- Change the variable path and dataroot in elgg_datalists to a path on your system, for example `/Users/{username}/dev/pleio/` and `/Users/{username}/dev/pleio-data/` and also the url in `elgg_sites_entity`, for example to `http://www.pleio.test:8080/`.

- Create a `settings.php` file in engine/ (using the following template) and configure the correct SQL user, password, host and database.

```
    <?php
    global $CONFIG;
    if (!isset($CONFIG)) {
      $CONFIG = new stdClass;
    }

    $CONFIG->dbuser = "pleio";
    $CONFIG->dbpass = "yourpassword";
    $CONFIG->dbname = "pleio";
    $CONFIG->dbhost = "localhost";
    $CONFIG->dbprefix = "elgg_";

    $CONFIG->dataroot = "/Users/{username}/dev/pleio-data/";

    $CONFIG->broken_mta = false;
    $CONFIG->db_disable_query_cache = false;
    $CONFIG->minusername = 1;
    $CONFIG->min_password_length = 8;
    $CONFIG->exception_include = "";

    $CONFIG->memcache = getenv("MEMCACHE_ENABLED");
    $CONFIG->memcache_prefix = getenv("MEMCACHE_PREFIX");
    $CONFIG->memcache_servers = ["localhost"];

    $CONFIG->elasticsearch = ["hosts" => ["localhost"]];
    $CONFIG->elasticsearch_index = "pleio";
```

- Edit your hosts file `/etc/hosts` so that www.pleio.test points to your local machine or use [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) to forward `*.test` to `127.0.0.1`. For dnsmasq use these steps:

```
    brew install dnsmasq
    echo 'address=/test/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf
    brew services start dnsmasq
    sudo mkdir -v /etc/resolver
    sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/test'
```

- Configure httpd24 with the following parameters:

In the file `/usr/local/etc/httpd/extra/httpd-vhosts.conf` add the following:

    <VirtualHost *:8080>
      ServerName www.pleio.test
      DocumentRoot "/Users/{username}/dev/pleio"
    </VirtualHost>

In the file `/usr/local/etc/httpd/httpd.conf` uncomment this line:

    LoadModule rewrite_module lib/httpd/modules/mod_rewrite.so

Also make sure a `FileMatch .php` exists in the config. If not there, add it below the `LoadModule php7_module` directive that is created by brew:

    LoadModule php7_module /usr/local/opt/php@7.1/lib/httpd/modules/libphp7.so

    <FilesMatch \.php$>
        SetHandler application/x-httpd-php
    </FilesMatch>

To allow the webserver to touch the upload folder, change user and group to your local user:

    User {username}
    Group staff

Change the default documentroot to your dev folder, and make sure `AllowOverride` is set to All:

    DocumentRoot "/Users/{username}/dev"
    <Directory "/Users/{username}/dev">
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

Add `index.php` to the DirectoryIndex

    <IfModule dir_module>
        DirectoryIndex index.html index.php
    </IfModule>

Uncomment the virtual hosts include:

    Include /usr/local/etc/httpd/extra/httpd-vhosts.conf

- Enable all changes by restarting the services

```
    brew services restart mariadb
    brew services restart httpd24
```

- Now browse to http://www.pleio.test:8080 and login with `admin/adminadmin`.

To use the new Pleio theme, enable the `pleio_template` plugin in `/admin/plugins`. You probably need to enable and disable some other plugins first due to the plugin dependencies.

There is also a `user/useruser` available.

### Using Docker

When you would like to setup a development environment using Docker with an application container and a database container, use the provided docker-compose configuration, first download all PHP plugins using:

    composer install

Then start all containers using:

    docker-compose up

Then point your browser to http://127.0.0.1:8080/, login using: admin/adminadmin. Changes in the code on the development environment are automatically synced with the app container.

## Building a production container

The repository also contains a Dockfile that allows you to build a production container base on Ubuntu 16.04, PHP7 and Apache2. First download all PHP plugins using:

    composer install

Then build the container by running (inside this directory)

    docker build -t pleio/pleio .

Then boot the container by running

    docker run -i -t -p 80:80 \
    --env DB_USER="" \
    --env DB_PASS="" \
    --env DB_NAME="" \
    --env DB_HOST="" \
    --env PLEIO_CLIENT="" \
    --env PLEIO_SECRET="" \
    --env PLEIO_URL="" \
    --env PLEIO_ENV="test" \
    --env SMTP_HOST="mail.example.com" \
    --env SMTP_DOMAIN="example.com" \
    --env MEMCACHE_ENABLED="true" \
    --env MEMCACHE_SERVER_1="" \
    pleio/pleio

## Tests

All unit tests can be run with the following command

    php console.php test

or within a Docker container

    docker run -it \
    --env DB_USER="" \
    --env DB_PASS="" \
    --env DB_NAME="" \
    --env DB_HOST="" \
    --env PLEIO_CLIENT="" \
    --env PLEIO_SECRET="" \
    --env PLEIO_URL="" \
    --env PLEIO_ENV="test" \
    --env SMTP_HOST="mail.example.com" \
    --env SMTP_DOMAIN="example.com" \
    --env MEMCACHE_ENABLED="true" \
    --env MEMCACHE_SERVER_1="127.0.0.1" \
    --env BLOCK_MAIL="true" \
    pleio/pleio php /app/console.php test

End-to-end testing is performed in [Selenium](http://www.seleniumhq.org/).

## Acknowledgements

[![image](https://cloud.githubusercontent.com/assets/5213690/24361644/b78b7866-130a-11e7-91a8-6ecdf7045bb3.png)](https://www.browserstack.com)

Special thanks to [Browserstack](https://www.browserstack.com) for providing the infrastructure to test Pleio in real browsers.
