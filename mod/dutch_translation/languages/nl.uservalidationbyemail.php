<?php
$language = array (
  'email:validate:subject' => '%s bevestig je e-mailadres voor %s!',
  'email:validate:body' => 'Beste %s,

Voordat je gebruik kunt maken van %s, moet je je e-mailadres bevestigen.
Bevestig je e-mailadres door op onderstaande link te klikken:
%s

Als je niet op de link kunt klikken, kopieer de url dan in de adres balk van je browser. %s %s

Het kan zijn dat de beheerder van de site eerst je lidmaatschap goed moet keuren,  dit kan enkele werkdagen duren.

Neem anders contact op met de community master: support@pleio.org',
  'email:confirm:success' => 'Je hebt je e-mailadres bevestigd!',
  'email:confirm:fail' => 'Helaas, je e-mailadres kon niet worden geverifieerd ...

Neem anders contact op met de community master: support@pleio.org',
  'uservalidationbyemail:registerok' => 'Om je account te activeren moet je je e-mailadres bevestigen. Klik daarvoor op de link in de e-mail die je zojuist ontvangen hebt. Heb je geen e-mail ontvangen, kijk dan of de mail niet per ongeluk in je spambox terechtgekomen is. Als je account niet binnen 8 uur is gevalideerd zal deze worden verwijderd en zul je opnieuw een account aan moeten maken! Neem bij vragen of problemen contact op met <a href="mailto:support@pleio.org">support@pleio.org</a>',
  'uservalidationbyemail:admin:unvalidated' => 'Ongevalideerde gebruikers',
  'uservalidationbyemail:admin:resend_validation' => 'Stuur de validatie nog een keer',
  'uservalidationbyemail:admin:validate' => 'Valideer',
  'uservalidationbyemail:admin:delete' => 'Verwijder',
  'uservalidationbyemail:confirm_resend_validation' => 'Wil je nog een keer een e-mail met validatiemogelijkheid naar %s versturen?',
  'uservalidationbyemail:confirm_resend_validation_checked' => 'Stuur de geselecteerde gebruikers opnieuw het validatiebericht?',
  'uservalidationbyemail:errors:could_not_validate_user' => 'Helaas, het lukte niet om deze gebruiker te valideren. Excuus voor het ongemak. Kun je het nog een keer proberen?',
  'uservalidationbyemail:errors:could_not_validate_users' => 'Helaas, het lukte niet om de geselecteerde gebruikers te valideren. Excuus voor het ongemak. Kun je het nog een keer proberen?


<p>Neem bij vragen of problemen contact op met <a href="mailto:support@pleio.org">support@pleio.org</a> </p>',
  'uservalidationbyemail:errors:could_not_delete_user' => 'Kan gebruiker niet verwijderen.',
  'uservalidationbyemail:errors:could_not_delete_users' => 'Kan niet alle geselecteerde gebruikers verwijderen.',
  'uservalidationbyemail:errors:could_not_resend_validation' => 'Kan validatie-aanvraag niet opnieuw verzenden.',
  'uservalidationbyemail:errors:could_not_resend_validations' => 'Niet alle geselecteerde gebruikers kregen het validatiebericht',
  'uservalidationbyemail:messages:deleted_users' => 'Alle gebruikers verwijderd',
  'uservalidationbyemail:messages:resent_validation' => 'Validatieverzoek opnieuw verzonden',
  'uservalidationbyemail:messages:resent_validations' => 'Validatieberich opnieuw verzonden naar alle geselecteerde gebruikers',
  'uservalidationbyemail:admin:no_unvalidated_users' => 'Geen ongevalideerde gebruikers',
  'uservalidationbyemail:confirm_validate_checked' => 'Valideer geselecteerde gebruikers?',
  'uservalidationbyemail:confirm_delete_checked' => 'Verwijder geselecteerde gebruikers?',
  'uservalidationbyemail:errors:unknown_users' => 'Onbekende gebruikers',
  'uservalidationbyemail:messages:validated_users' => 'Alle geselecteerde gebruikers gevalideerd.',
  'admin:users:unvalidated' => 'Ongevalideerd',
  'uservalidationbyemail:login:fail' => 'Je account is nog niet gevalideerd dus het aanmelden is mislukt. Er is nog een validatie mail verstuurd.',
  'uservalidationbyemail:confirm_validate_user' => 'Valideer %s?',
  'uservalidationbyemail:messages:validated_user' => 'Gebruiker gevalideerd.',
);
add_translation("nl", $language);
