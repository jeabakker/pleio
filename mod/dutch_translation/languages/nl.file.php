<?php
$language = array (
  'groups:enablefiles' => 'Zet de groepsbestanden aan',
  'file:user' => 'Bestanden van %s',
  'file:friends' => 'Bestanden van contacten',
  'file:all' => 'Alle bestanden',
  'file:edit' => 'Bestand bewerken',
  'file:list' => 'Toon in een lijst',
  'file:group' => 'Bestanden',
  'file:gallery' => 'Toon in een galerij',
  'file:gallery_list' => 'Toon als galerij of als lijst',
  'file:upload' => 'Bestand uploaden',
  'file:replace' => 'Vervang de inhoud van een bestand (laat dit leeg als je het bestand niet wil vervangen)',
  'file:title:friends' => 'Contacten',
  'file:type:image' => 'Afbeeldingen',
  'file:user:type:video' => '%ss video\'s',
  'file:user:type:document' => '%ss documenten',
  'file:user:type:audio' => '%ss audio',
  'file:user:type:image' => '%ss afbeeldingen',
  'file:user:type:general' => '%ss algemene bestanden',
  'file:friends:type:video' => 'Video\'s van je contacten',
  'file:friends:type:document' => 'Documenten van je contacten',
  'file:friends:type:audio' => 'Audio van je contacten',
  'file:friends:type:image' => 'Afbeeldingen van je contacten',
  'file:friends:type:general' => 'Algemene bestanden van je contacten',
  'file:widget:description' => 'Toon je nieuwste bestanden',
  'file:download' => 'Downloaden',
  'file:tagcloud' => 'Wolk met Tags',
  'file:newupload' => 'Bestand geupload:',
  'file:embed' => 'Een bestand embedden',
  'file:saved' => 'Het bestand is opgeslagen.',
  'file:deleted' => 'Het bestand is verwijderd.',
  'file:none' => 'Er zijn geen bestanden geüpload.',
  'file:uploadfailed' => 'Helaas, we konden het bestand niet opslaan.',
  'file:downloadfailed' => 'Helaas, het bestand is op dit moment niet beschikbaar.',
  'file:deletefailed' => 'Het bestand kan op dit moment niet worden verwijderd.',
  'file:noaccess' => 'Je hebt niet de juiste rechten om dit bestand aan te passen.',
  'file:cannotload' => 'Er is een fout opgetreden bij het laden van het bestand.',
  'file:notification' => '%s heeft een bestand geupload:

%s
%s

Bekijk en reageer op het nieuwe bestand:
%s',
  'file:tags' => 'Tag(s) (komma gescheiden)',
  'file' => 'Bestanden',
  'file:more' => 'Meer bestanden',
  'file:type:' => 'Bestanden',
  'file:type:all' => 'Alle bestanden',
  'item:object:file' => 'Bestanden',
);
add_translation("nl", $language);
