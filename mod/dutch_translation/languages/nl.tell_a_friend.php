<?php
$language = array (
  'tell_a_friend:share:subject:default' => '%s wil iets met je delen',
  'tell_a_friend:share:message:default' => 'Hallo,

%s raad je aan om eens te kijken naar: %s

%s',
  'tell_a_friend:share' => 'Delen',
  'tell_a_friend:share_title' => 'Delen',
  'tell_a_friend:share:recipient' => 'Ontvanger(s)',
  'tell_a_friend:share:subject' => 'Onderwerp',
  'tell_a_friend:share:message' => 'Bericht',
  'tell_a_friend:share:required' => 'Velden gemarkeerd met een * zijn verplicht',
  'tell_a_friend:action:share:error:recipients' => 'Geef ten minste 1 ontvanger op',
  'tell_a_friend:action:share:error:subject' => 'Geef een onderwerp op',
  'tell_a_friend:action:share:error:message' => 'Geef een bericht op',
  'tell_a_friend:action:share:success' => 'Je bericht is verstuurd',
);
add_translation("nl", $language);
