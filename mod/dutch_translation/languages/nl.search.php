<?php
$language = array (
  'search:no_results' => 'Helaas, er zijn geen resultaten gevonden. Probeer een andere zoekterm.',
  'search:unavailable_entity' => 'Niet beschikbaar onderdeel',
  'search_types:tags' => 'Tags',
);
add_translation("nl", $language);
