<?php
$language = array (
  'messages' => 'Bericht',
  'messages:unreadcount' => '%s ongelezen',
  'messages:back' => 'terug naar je postbus',
  'messages:user' => '%s\'s postbus',
  'messages:posttitle' => '%ss berichten: %s',
  'messages:inbox' => 'Berichten',
  'messages:send' => 'Verstuur',
  'messages:sent' => 'Verstuur',
  'messages:message' => 'Bericht',
  'messages:title' => 'Onderwerp',
  'messages:to' => 'Aan',
  'messages:from' => 'Van',
  'messages:fly' => 'Versturen',
  'messages:replying' => 'Bericht beantwoorden',
  'messages:sendmessage' => 'Verstuur een bericht',
  'messages:compose' => 'Samenstellen van een bericht',
  'messages:add' => 'Samenstellen van een bericht',
  'messages:sentmessages' => 'Verstuur bericht',
  'messages:recent' => 'Recente berichten',
  'messages:original' => 'Originele bericht',
  'messages:yours' => 'Jouw bericht',
  'messages:answer' => 'Beantwoord',
  'messages:toggle' => 'Alles selecteren',
  'messages:markread' => 'Markeren als gelezen',
  'messages:recipient' => 'Kies een ontvanger�',
  'messages:to_user' => 'Aan: %s',
  'messages:new' => 'Nieuw bericht',
  'notification:method:site' => 'Deelsite',
  'messages:error' => 'Er ging iets mis bij het opslaan van je bericht. Excuus daarvoor. Kun je het nog een keer proberen?',
  'item:object:messages' => 'Bericht',
  'messages:posted' => 'Je bericht is verzonden.',
  'messages:success:delete:single' => 'Bericht is verwijderd',
  'messages:success:delete' => 'Bericht verwijderd',
  'messages:success:read' => 'Berichten als gelezen gemarkeerd',
  'messages:error:messages_not_selected' => 'Geen berichten geselecteerd',
  'messages:error:delete:single' => 'Kan het bericht te verwijderen',
  'messages:email:subject' => 'Je hebt een nieuw bericht',
  'messages:email:body' => 'Je hebt een nieuw bericht van %s. Het bericht is:

%s

Om jouw berichten te bekijken, klik hier:

%s

Om %s een bericht te sturen, klik hier:

%s

Je kunt niet antwoorden op deze email.',
  'messages:blank' => 'Bericht heeft geen inhoud',
  'messages:notfound' => 'Sorry, het opgegeven bericht kon niet worden gevonden.',
  'messages:notdeleted' => 'Sorry, het bericht kon niet worden verwijderd.',
  'messages:nopermission' => 'Je hebt niet genoeg rechten om dit bericht te verwijderen.',
  'messages:nomessages' => 'Er zijn geen berichten',
  'messages:user:nonexist' => 'De geadresseerde kon niet worden gevonden in de gebruikerslijst.',
  'messages:user:blank' => 'Je moet nog iemand toevoegen om dit bericht aan te versturen.',
  'messages:deleted_sender' => 'Verwijder gebruiker',
);
add_translation("nl", $language);
