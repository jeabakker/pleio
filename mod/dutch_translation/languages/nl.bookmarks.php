<?php
$language = array (
  'bookmarks:notification' => '%s heeft een nieuwe snelkoppeling toegevoegd:

%s - %s
%s

Bekijk en reageer op de nieuwe snelkoppeling:
%s',
  'bookmarks:edit' => 'Bewerk favoriet',
  'bookmarks:owner' => '%s\'s favorieten',
  'bookmarks:none' => 'Er zijn nog geen favorieten aangemaakt',
  'river:create:object:bookmarks' => '%s maakte %s aan als favoriet',
  'river:comment:object:bookmarks' => '%s reageerde op een favoriet %s',
  'bookmarks:river:annotate' => 'een reactie op deze favoriet',
  'bookmarks:nogroup' => 'Er zijn nog geen favoriete sites toegevoegd aan deze groep',
  'bookmarks:no_title' => 'Geen titel toegevoegd',
  'bookmarks:save:invalid' => 'Het adres van de favoriet is ongeldig en kon niet worden opgeslagen.',
  'bookmarks:morebookmarks' => 'Meer favorieten',
  'bookmarks:this:group' => 'Favoriet in %s',
  'bookmarks:bookmarklet:group' => 'Browserplugin om groepsfavorieten toe te voegen',
  'bookmarks:group' => 'Favoriete sites',
  'bookmarks:enablebookmarks' => 'Wil je gebruik maken van de mogelijkheid favoriete internetsites toe te voegen?',
  'bookmarks' => 'Favoriete internetsites',
  'bookmarks:add' => 'Favoriet toevoegen',
  'bookmarks:friends' => 'Favorieten van contacten',
  'bookmarks:everyone' => 'Alle favorieten',
  'bookmarks:this' => 'Voeg deze pagina toe als favoriet',
  'bookmarks:bookmarklet' => 'Browserplugin om favorieten toe te voegen',
  'bookmarks:inbox' => 'Favorietenpostbus',
  'bookmarks:more' => 'Meer favorieten',
  'bookmarks:with' => 'Deel met',
  'bookmarks:new' => 'Een nieuwe favoriet',
  'bookmarks:address' => 'Adres van de favoriet',
  'bookmarks:delete:confirm' => 'Weet je zeker dat je deze favoriet wilt verwijderen?',
  'bookmarks:numbertodisplay' => 'Aantal favorieten om weer te geven',
  'bookmarks:shared' => 'Favoriet',
  'bookmarks:visit' => 'Ga naar de pagina',
  'bookmarks:recent' => 'Recente favorieten',
  'bookmarks:river:item' => 'een favoriet',
  'item:object:bookmarks' => 'Favorieten',
  'bookmarks:widget:description' => 'Deze widget toont je meest recente favorieten.',
  'bookmarks:bookmarklet:description' => 'De favorieten Browser plugin maakt het mogelijk om iedere willekeurige pagina te delen met je contacten, of om gewoon voor jezelf om te onthouden. Om het te gebruiken sleep je de Browser plugin naar je Links balk van je browser:',
  'bookmarks:bookmarklet:descriptionie' => 'Als je Internet Explorer gebruikt, moet je met de rechter muisknop op de browserplugin klikken en dan kiezen voor \'Toevoegen aan favorieten\'.',
  'bookmarks:bookmarklet:description:conclusion' => 'Je kunt dan iedere pagina die je bezoekt markeren door op de link te klikken.',
  'bookmarks:save:success' => 'Je favoriet is opgeslagen.',
  'bookmarks:delete:success' => 'De favoriet is verwijderd.',
  'bookmarks:save:failed' => 'Je favoriet kon niet worden opgeslagen. Excuus daarvoor. Zou je het nog een keer kunnen proberen?',
  'bookmarks:delete:failed' => 'De favoriet kon niet worden verwijderd. Zou je het nog een keer kunnen proberen?',
);
add_translation("nl", $language);
