<?php
$language = array (
  'croncheck:info' => 'Laatste keer dat Cron heeft gedraaid',
  'croncheck:registered' => 'Gebeurtenissen geregistreerd per cron-interval',
  'croncheck:interval' => 'Interval',
  'croncheck:timestamp' => 'Timestamp',
  'croncheck:friendly_time' => 'Vriendelijke tijd',
  'croncheck:no_run' => 'Dit cron is nooit uitgevoerd voordat (sinds deze plugin is ingeschakeld)',
  'croncheck:none_registered' => 'Er zijn geen evenementen aangemeld voor deze periode',
  'croncheck:widget:title' => 'Croncheck',
  'croncheck:widget:description' => 'Geeft informatie over CRON, wanneer het voor het laatst is uitgevoerd en welke functies zijn geregistreerd bij de verschillende intervals',
);
add_translation("nl", $language);
