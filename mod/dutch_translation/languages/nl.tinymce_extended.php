<?php
$language = array (
  'tinymce_extended:settings' => 'TinyMCE instellingen',
  'tinymce_extended:settings:plugins' => 'komma gescheiden lijst van extra plugins die moeten worden geladen',
  'tinymce_extended:settings:menu1' => 'Eerste menu knoppen',
  'tinymce_extended:settings:menu2' => 'Tweede menu knoppen',
  'tinymce_extended:settings:menu3' => 'Derde menu knoppen',
  'tinymce_extended:settings:version' => 'Versie',
  'tinymce_extended:settings:more_info' => 'Meer informatie over de standaard configureerbare knoppen en plugins is hier te vinden <a href="http://wiki.moxiecode.com/index.php/TinyMCE:Control_reference" target="_blank">http://wiki.moxiecode.com/index.php/TinyMCE:Control_reference</a>',
  'tinymce_extended:defaults:plugins' => 'lists,spellchecker,autosave,fullscreen,paste',
  'tinymce_extended:defaults:menu1' => 'bold,italic,underline,separator,strikethrough,bullist,numlist,undo,redo,link,unlink,image,blockquote,code,pastetext,pasteword,more,fullscreen',
  'tinymce_extended:defaults:valid_elements' => 'a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]',
);
add_translation("nl", $language);
