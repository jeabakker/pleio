<?php
$language = array (
  'expages:terms' => 'Algemene voorwaarden',
  'expages:posted' => 'De pagina is aangepast.',
  'expages:error' => 'Er is een probleem opgetreden. Excuus daarvoor. Probeer het nogmaals.
Als het probleem blijft bestaan, neem dan contact op met de beheerder.',
);
add_translation("nl", $language);
