<?php
$language = array (
  'content_subscriptions:subscribe' => 'Volgen',
  'content_subscriptions:unsubscribe' => 'Ontvolgen',
  'content_subscriptions:sidebar:title' => 'Blijf op de hoogte',
  'content_subscriptions:sidebar:owner' => 'Je bent de eigenaar van deze discussie en blijft altijd op de hoogte.',
  'content_subscriptions:sidebar:notifications' => 'Je blijft al op de hoogte door dat je de meldingen in de groep hebt ingesteld.',
  'content_subscriptions:sidebar:counter' => '%s leden volgen dit.',
  'content_subscriptions:sidebar:no_subscriptions' => 'Niemand volgt dit.',
  'content_subscriptions:subscribe:description' => 'Als je op de hoogte wilt blijven van deze discussie, klik Volgen',
  'content_subscriptions:unsubscribe:description' => 'Als je niet langer op de hoogte wilt blijven van deze discussie, klik Ontvolgen.',
  'content_subscriptions:action:subscribe:error:owner' => 'Je bent de eigenaar van deze discussie en kan niet (ont)volgen',
  'content_subscriptions:action:subscribe:error:subscribe' => 'Er is een onbekende fout opgetreden tijdens het volgen, probeer het nogmaals',
  'content_subscriptions:action:subscribe:error:unsubscribe' => 'Er is een onbekende fout opgetreden tijdens het ontvolgen, probeer het nogmaals',
  'content_subscriptions:action:subscribe:success:subscribe' => 'Je blijft nu op de hoogte van deze discussie',
  'content_subscriptions:action:subscribe:success:unsubscribe' => 'Je blijft niet langer op de hoogte van deze discussie',
  'content_subscriptions:notification:comment:subject' => 'Er is een nieuwe reactie op: %s',
  'content_subscriptions:notification:comment:message' => 'Er is een nieuwe reactie op het item "%s" door %s. De reactie is:

%s

Om te antwoorden of het originele item te zien, klik hier:

%s

Om %s\'s Profile te bekijken, klik hier:

%s

Je kunt niet antwoorden op deze e-mail.',
);
add_translation("nl", $language);
