<?php
$dbprefix = elgg_get_config("dbprefix");

# check for group errors
$group_result = get_data("SELECT * FROM elgg_groups_entity WHERE guid NOT IN (SELECT guid FROM elgg_metadata md left
                        JOIN elgg_metastrings ms ON md.name_id = ms.id LEFT JOIN elgg_groups_entity ge ON md.entity_guid = ge.guid
                        WHERE string = 'isFeatured')");

$errors = FALSE;

if ($group_result) {
    $group_count = count($group_result);
    $errors = TRUE;
} else {
    $group_count = 0;
}


# check for wiki errors
$wiki_result = get_data("SELECT guid FROM elgg_entities ee
                         LEFT JOIN elgg_entity_subtypes es ON ee.subtype = es.id
                         LEFT JOIN elgg_metadata md ON ee.guid = md.entity_guid
                         LEFT JOIN elgg_metastrings ms ON md.name_id = ms.id
                         WHERE (ms.string = 'parent_guid' AND es.subtype = 'page') OR es.subtype = 'page_top'");

if ($wiki_result) {
    $wiki_count = count($wiki_result);
    $errors = TRUE;
} else {
    $wiki_count = 0;
}



$options = [
    "type" => "object",
    "subtypes" => ["static", "static_top"],
    "limit" => false
];
$static_result = elgg_get_entities($options);


if ($static_result) {
    $static_count = count($static_result);
    $errors = TRUE;
} else {
    $static_result = 0;
}

?>




<?php if ($errors): ?>
    <?php if ($group_count > 0 || $wiki_count > 0|| $static_count > 0) : ?>
        Er zijn problemen met de database: <br><br>
        <?php if ($group_count > 0): ?>
            Er zijn <?php echo $group_count; ?> groepen waarvan de aangeraden vlag nog niet bekend is in nieuwe template<br><br>
        <?php endif; ?>
        <?php if ($wiki_count > 0): ?>
            Er zijn <?php echo $wiki_count; ?> wiki pagina's welke niet beschikbaar zijn in het nieuwe template<br><br>
        <?php endif; ?>
        <?php if ($static_count > 0): ?>
            Er zijn <?php echo $static_count; ?> cms pagina's welke niet beschikbaar zijn in het nieuwe template<br><br>
        <?php endif; ?>
        <?php echo elgg_view("output/confirmlink", [
            "href" => "/action/admin/ready_database_for_new_template",
            "text" => "Maak database klaar",
            "class" => "elgg-button elgg-button-submit",
            "is_action" => true
        ]); ?>
    <?php endif; ?>

<?php else: ?>
    Database klaar voor nieuwe template
<?php endif; ?>
