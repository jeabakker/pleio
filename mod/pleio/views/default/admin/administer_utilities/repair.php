<p>
    <?php echo elgg_echo("admin:administer_utilities:repair:description"); ?>
</p>

<?php
echo elgg_view_module(
    "inline",
    elgg_echo("pleio:hidden_users"),
    elgg_view("admin/administer_utilities/repair/hidden_users")
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio:hidden_groups"),
    elgg_view("admin/administer_utilities/repair/hidden_groups")
);

echo elgg_view_module(
    "inline",
    elgg_echo("pleio:broken_plugins"),
    elgg_view("admin/administer_utilities/repair/broken_plugins")
);

if(elgg_is_active_plugin("pleio_template")) {
    echo elgg_view_module(
        "inline",
        elgg_echo("pleio:ready_database"),
        elgg_view("admin/administer_utilities/repair/ready_database")
    );
}