<?php
$dbprefix = elgg_get_config("dbprefix");

# repair featured groups
$group_result = get_data("SELECT * FROM elgg_groups_entity WHERE guid NOT IN (SELECT guid FROM elgg_metadata md left
                    JOIN elgg_metastrings ms ON md.name_id = ms.id LEFT JOIN elgg_groups_entity ge ON md.entity_guid = ge.guid
                    WHERE string = 'isFeatured');");
if ($group_result) {
    foreach ($group_result as $row) {
        $group = get_entity($row->guid);

        if (isset($group->featured_group)) {
            if ($group->featured_group == 'yes') {
                $group->isFeatured = '1';
            }
        } else {
            $group->isFeatured = '0';
        }
        $group->save();
    }
}

# repair wiki pages
$dbprefix = elgg_get_config("dbprefix");
$ia = elgg_set_ignore_access(true);
$subtype_id = add_subtype("object", "wiki");

$options = [
    "type" => "object",
    "subtypes" => ["page", ],
    "limit" => false
];


foreach (elgg_get_entities($options) as $page) {
    if ($page->parent_guid) {
        $page->container_guid = $page->parent_guid;
        $page->save();
        update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$page->guid}");
        _elgg_invalidate_memcache_for_entity($page->guid);
    }
}
$options = [
    "type" => "object",
    "subtypes" => ["page_top", ],
    "limit" => false
];

foreach (elgg_get_entities($options) as $page) {
    update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$page->guid}");
    _elgg_invalidate_memcache_for_entity($page->guid);
}

# repair static pages
$dbprefix = elgg_get_config("dbprefix");
$subtype_id = add_subtype("object", "page");

$options = [
    "type" => "object",
    "subtypes" => ["static", "static_top"],
    "limit" => false
];

foreach (elgg_get_entities($options) as $page) {
    update_data("UPDATE {$dbprefix}entities SET subtype = {$subtype_id} WHERE guid = {$page->guid}");
    _elgg_invalidate_memcache_for_entity($page->guid);
}

elgg_set_ignore_access($ia);

system_message("Database gereed voor nieuwe template");

forward(REFERER);