<?php
namespace ModPleio;

class ProfileHandler {
    public function __construct(\ElggUser $user) {
        global $CONFIG;

        if (!$user || !$user instanceof \ElggUser) {
            throw new Exception("no_user_available");
        }

        $token = $user->getPrivateSetting("pleio_token");
        if (!$token) {
            throw new Exception("no_token_available");
        }

        $this->user = $user;
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => $CONFIG->pleio->url,
            'timeout' => 2,
            'headers' => [
                'Authorization' => "Bearer {$token}"
            ]
        ]);
    }

    private function getToken() {
        if (!$this->user) {
            throw new Exception("could_not_find_user");
        }

        $token = $this->user->getPrivateSetting("pleio_token");

        if (!$token) {
            throw new Exception("could_not_find_token");
        }

        return $token;
    }

    public function changeAvatar($file) {
        return $this->client->request("POST", "api/users/me/change_avatar", [
            "multipart" => [
                [
                    "name" => "avatar",
                    "contents" => fopen($file['tmp_name'], 'r'),
                    "filename" => $file['name']
                ]
            ]
        ]);
    }

    public function removeAvatar() {
        return $this->client->request("POST", "api/users/me/remove_avatar");
    }

    public function changeName($new_name) {
        return $this->client->request("POST", "api/users/me/change_name", [
            "form_params" => [
                "name" => $new_name
            ]
        ]);
    }

    public function changeEmail($email) {
        return $this->client->request("POST", "api/users/me/change_email", [
            "form_params" => [
                "email" => $email
            ]
        ]);
    }

    public function changePassword($old_password, $new_password) {
        return $this->client->request("POST", "api/users/me/change_password", [
            "form_params" => [
                "old_password" => $old_password,
                "new_password" => $new_password
            ]
        ]);
    }
}