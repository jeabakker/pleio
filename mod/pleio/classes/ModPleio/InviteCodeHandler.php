<?php
namespace ModPleio;

class InviteCodeHandler {
    public static function getInvites() {
        $site = elgg_get_site_entity();

        $options = [
            "guid" => $site->guid,
            "annotation_name" => "site_invitation",
            "annotation_owner_guid" => $site->guid,
            "limit" => false
        ];

        return elgg_get_annotations($options);
    }

    public static function generateCode($email) {
        $site = elgg_get_site_entity();

        $options = [
            "guid" => $site->guid,
            "annotation_name" => "site_invitation",
            "where" => "v.string LIKE '%|{$email}'",
            "annotation_owner_guid" => $site->guid,
            "limit" => 1
        ];

        $annotations = elgg_get_annotations($options);
        if ($annotations) {
            $annotation = $annotations[0];
            return explode("|", $annotation->value)[0];
        }

        $code = \ElggCrypto::getRandomString(32, \ElggCrypto::CHARS_PASSWORD);

        $ia = elgg_set_ignore_access(true);

        $site->annotate(
            "site_invitation",
            "{$code}|{$email}",
            ACCESS_PUBLIC,
            $site->guid
        );

        elgg_set_ignore_access($ia);

        return $code;
    }

    public static function isValid($code) {
        $site = elgg_get_site_entity();
        $code = sanitise_string($code);

        $options = [
            "guid" => $site->guid,
            "annotation_name" => "site_invitation",
            "where" => "v.string LIKE '{$code}|%'",
            "annotation_owner_guid" => $site->guid,
            "limit" => 1
        ];

        $annotations = elgg_get_annotations($options);
        if ($annotations) {
            return true;
        }

        return false;
    }

    public static function revokeInvite($email) {
        $site = elgg_get_site_entity();
        $email = sanitise_string($email);

        $options = [
            "guid" => $site->guid,
            "annotation_name" => "site_invitation",
            "where" => "v.string LIKE '%|{$email}'",
            "annotation_owner_guid" => $site->guid,
            "limit" => false
        ];

        $ia = elgg_set_ignore_access(true);
        foreach (elgg_get_annotations($options) as $annotation) {
            $annotation->delete();
        }
        elgg_set_ignore_access($ia);

        return true;
    }

    public static function revokeCode($code) {
        $site = elgg_get_site_entity();
        $code = sanitise_string($code);

        $options = [
            "guid" => $site->guid,
            "annotation_name" => "site_invitation",
            "where" => "v.string LIKE '{$code}|%'",
            "annotation_owner_guid" => $site->guid,
            "limit" => false
        ];

        $ia = elgg_set_ignore_access(true);
        foreach (elgg_get_annotations($options) as $annotation) {
            $annotation->delete();
        }
        elgg_set_ignore_access($ia);

        return true;
    }
}