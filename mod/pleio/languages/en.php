<?php
$en = array(
    "admin:server:pleio_template:env" => "Environment",
    "pleio_template:type" => "Type",
    "pleio_template:send_mail" => "Send mail",
    "admin:users:access_requests" => "Access requests",
    "admin:users:import" => "Import users",
    "admin:administer_utilities:repair" => "Repair tasks",
    "admin:administer_utilities:repair:description" => "In this panel you can repair certain problems with Elgg.",
    "pleio:hidden_users" => "Hidden users",
    "pleio:hidden_groups" => "Hidden groups",
    "pleio:broken_plugins" => "Broken plugins",
    "pleio:ready_database" => "Ready database for new template",
    "pleio:site_permission" => "Permission of the site:",
    "pleio:not_configured" => "The Pleio login plugin is not configured.",
    "pleio:registration_disabled" => "Registration is disabled, create an account on Pleio.",
    "pleio:walled_garden" => "Welcome to %s",
    "pleio:walled_garden_description" => "Access to this site is restricted to users. Log in to access the site or request membership.",
    "pleio:request_access" => "Request access",
    "pleio:request_access:description" => "To enter this site, you must request access from the admin. Click the button to request access.",
    "pleio:enable_frontpage_indexing:label" => "Indexing",
    'pleio:enable_frontpage_indexing' => "Enable indexing of main pages by search engines",
    "pleio:validate_access" => "Validate access",
    "pleio:validate_access:description" => "Your email domain is whitelisted for this website. Please check your details and request access. We will send you an e-mail with a link that provides direct access to the site.",
    "pleio:validate_access:error" => "Something went wrong during validation. Please try again",
    "pleio:change_settings" => "Change settings",
    "pleio:change_settings:description" => "To change your settings please go to %s. After you changed the settings, please login again to effectuate your settings.",
    "pleio:access_requested" => "Requested access",
    "pleio:could_not_find" => "Could not find access request.",
    "pleio:access_requested:wait_for_approval" => "Access is requested. You will receive an e-mail when the request is accepted.",
    "pleio:access_requested:check_email" => "Check your e-mail and follow the link to activate your account.",
    "pleio:no_requests" => "Currently there are no requests.",
    "pleio:approve" => "Approve",
    "pleio:decline" => "Decline",
    "pleio:settings:notifications_for_access_request" => "Send all admins a notification when somebody requests access to the site",
    "pleio:admin:access_request:subject" => "New access request for %s",
    "pleio:admin:access_request:body" => "Hello %s,
        Somebody with the name %s has performed an access request to %s.
        To review the request please visit:

        %s",
    "pleio:approved:subject" => "You are now member of: %s",
    "pleio:approved:body" => "Hello %s,

    The administrator approved your access request to %s. Go to this link to get access to the site:

    %s",
    "pleio:declined:subject" => "Membership request declined for: %s",
    "pleio:declined:body" => "Hello %s,

Unfortunately the site administrator of %s decided to decline your membership request. Please contact the administrator if you think this is a mistake.",


    "pleio:membership:description" => "Here you can find all the users who have requested membership to this Subsite. You can eighter approve or decline their request, the user will be notified about eighter action.",
    "pleio:membership:list:title" => "Pending membership requests",
    "pleio:title:membership" => "Pending memberships",
	"admin:users:membership" => "Membership requests",
    "pleio:closed" => "Closed",
    "pleio:open" => "Open",
    "pleio:settings:email_from" => "When not configured, all mail is send from %s.",
    "pleio:settings:idp" => "When using SAML2 login, provide the unique ID of the SAML2 Identity Provider",
    "pleio:settings:idp_name" => "Identity Provider display name",
    "pleio:settings:login_through" => "Login through %s",
    "pleio:settings:login_credentials" => "Allow to login with credentials as well",
    "pleio:settings:walled_garden_description" => "Description on login page of closed site",
    "pleio:login_with_credentials" => "Or, login using credentials",
    "pleio:is_banned" => "Unfortunately, your account is banned. Please contact the site administrator.",
    "pleio:imported" => "Imported %s users, updated %s users and an error occured while importing for %s users.",
    "pleio:users_import:step1:description" => "This functionality allows you to import users using a CSV file. Please choose the CSV file in the first step. Make sure the first line of the CSV contains the field names and the fields are delimited by a semicolon ;. The permissionlevel of the fields will be set to the default site level. Please make sure the CSV is encoded with UTF-8.",
    "pleio:users_import:step2:description" => "Please link the source fields in the CSV file to the target fields in this platform. Make sure that users within the platform are ",
    "pleio:users_import:choose_field" => "Choose a field",
    "pleio:users_import:source_field" => "Source field",
    "pleio:users_import:target_field" => "Target field",
    "pleio:users_import:step1:file" => "CSV file",
    "pleio:users_import:step1:file" => "Continue to the next step",
    "pleio:users_import:step1:success" => "CSV is uploaded succesfully",
    "pleio:users_import:step1:error" => "There was an error while uploading the CSV file. Please check the file and try again.",
    "pleio:users_import:sample" => "sample",
    "pleio:users_import:started_in_background" => "Import started in the background. You will receive an e-mail after completion.",
    "pleio:users_import:email:success:subject" => "Import was a success",
    "pleio:users_import:email:success:body" => "Dear %s,

    The import of users succeeded. Here are the stats:

    %s users added
    %s users updated
    %s users failed
    ",
    "pleio:users_import:email:failed:subject" => "Import failed",
    "pleio:users_import:email:failed:body" => "Dear %s,

    The import of users failed. Here is the error message of the server:

    %s
    ",
    "profile:gender" => "Gender",
    "pleio:settings:domain_whitelist" => "Domain whitelist",
    "pleio:settings:domain_whitelist:explanation" => "You can enter a comma-seperated list of domains, e.g. example.com, example2.com",
    "pleio:validation_email:subject" => "Please validate your account for %s",
    "pleio:validation_email:body" => "Hello %s,

    You requested access to %s. Please follow this link to get direct access:

    %s",
    "pleio:no_token_available" => "No token available. Please try to login again.",
    "pleio:avatar:upload:server_error" => "Server error. Please try again.",
    "pleio:avatar:upload:user_error" => "Could not update the avatar due to an expired session. Please logout and login again and try again.",
    'admin:users:all' => 'All users',
    'admin:users:edit' => 'Edit user',
    'admin:users:invite' => 'Invite user',
	'admin:users:invite_csv' => "Invite users by CSV (step 2)",
    'admin:users:invitations' => 'Open invitations',
    'admin:profile:details' => 'Profile details',
    'admin:profile:edit' => 'Edit profile',
    'members:search' => 'Search user',
    'view_profile' => 'View profile',
    'users' => 'Users',


	'pleio:action:invite:csv:error:content' => "No CSV was available to invite users from, please upload it again",
	'pleio:action:invite:csv:error:email_column' => "Please provide an email column",
	'pleio:action:invite:csv:error:email_column:invalid' => "The provided email column is invalid",
	'pleio:action:invite:csv:error:csv' => "There was an error while opening the CSV file",
	'pleio:action:invite:csv:error:users' => "No (new) users were invited",
	'pleio:action:invite:csv:success' => "Successfully invited %s users",

	'pleio:invite_csv:column:label' => "Please tell us which column hold which information",
	'pleio:invite_csv:column:description' => "We need to know which column hold the email addresses of the users. Optionaly you can select a column which holds the name of the user.",
	'pleio:invite_csv:column:select' => "Please select a column",
    'pleio:invite:description' => 'Here you can invite users to join the site.',
    'pleio:invite:users:label' => 'E-mailaddress',
    'pleio:invite:users:description' => 'Enter a valid e-mailaddress and click on the autofilled address to add the user to the list.',
    'pleio:invite:csv:tab' => 'CSV file',
    'pleio:invite:csv:label' => 'CSV file',
    'pleio:invite:csv:description' => 'Select a CSV file with name and e-mailaddress, comma-seperated.',
    'pleio:invite:message:label' => 'Personal message',
    'pleio:invitation:subject' => 'You are invited to join %s',
    'pleio:invitation:message' => '%s invited you to join %s. If you would like to join the site directly, please enter the following URL:

    %s

    %s
    ',
    'pleio:walled_garden_invite' => 'Please login with your Pleio account (or register one) to accept the invitation.',
    'pleio:invitations:description' => 'Here you can find an overview of all open invitations.',
    'pleio:date_invited' => 'Invitation date',
    'pleio:invited' => '%s invitations sent, %s users unbanned and %s where already on site.',
    'pleio:invalid_invitecode' => 'The used code is invalid. You can try to login and request access again.',
    'pleio:revoke' => 'Revoke',
    'pleio:invitations:revoke:confirm' => 'Are you sure you want to revoke the invite?',


    'pleio:export:list:include_banned_users' => 'Include non members of the site and banned users'
);

add_translation("en", $en);