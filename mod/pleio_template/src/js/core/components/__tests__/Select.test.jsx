import React from "react"
import Select from "../Select"
import renderer from "react-test-renderer"

const e = { preventDefault: () => {} };

const options = {
    1: "First option",
    2: "Second option"
}

const component = renderer.create(
    <Select options={options} />
)

let tree = component.toJSON()
const ul = tree.children[2]


it("Renders correctly", () => {
    expect(tree).toMatchSnapshot()
})

it("Toggles on click", () => {
    const firstLi = ul.children[0].props.onClick(e)

    tree = component.toJSON()
    expect(tree).toMatchSnapshot()
})

it("Changes value on clicking an item", () => {
    const secondLi = ul.children[1].props.onClick(e)

    tree = component.toJSON()
    expect(tree).toMatchSnapshot()
})