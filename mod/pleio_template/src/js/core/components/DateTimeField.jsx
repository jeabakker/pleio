import React from "react"
import classnames from "classnames"
import Validator from "validatorjs"
import PropTypes from "prop-types"
import autobind from "autobind-decorator"
import moment from "moment"

moment.locale("nl")
moment.weekdays(true)

class DateTimeField extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            value: this.toMoment(this.props.value),
            timeText: this.toMoment(this.props.value).format("HH:mm"),
            isOpenDate: false,
            isOpenTime: false
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }

    handleClickOutside = (e) => {
        const date = this.refs.date
        if (date && !date.contains(e.target)) {
            this.setState({ isOpenDate: false })
        }

        const time = this.refs.time
        if (time && !time.contains(e.target)) {
            this.setState({ isOpenTime: false })
        }
    }

    toMoment(value) {
        const start = moment(value)
        const remainder = start.minute() % 30
        return moment(start).add(remainder, "minutes")
    }

    openDate = (e) => {
        e.preventDefault()
        this.setState({ isOpenDate: true })
    }

    closeDate = (e) => {
        e.preventDefault()
        this.setState({ isOpenDate: false })
    }

    openTime = (e) => {
        e.preventDefault()
        this.setState({ isOpenTime: true })
    }

    closeTime = (e) => {
        e.preventDefault()
        this.setState({ isOpenTime: false })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({
                value: this.toMoment(nextProps.value)
            })
        }
    }

    componentWillMount() {
        if (this.context.attachToForm) {
            this.context.attachToForm(this)
        }
    }

    componentWillUnmount() {
        if (this.context.detachFromForm) {
            this.context.detachFromForm(this)
        }
    }

    resetTime = (e) => {
        this.setState({ value: this.toMoment() })
    }

    previousMonth = (e) => {
        e.preventDefault()
        this.setState({ value: moment(this.state.value).subtract(1, "month") })
    }

    nextMonth = (e) => {
        e.preventDefault()
        this.setState({ value: moment(this.state.value).add(1, "month") })
    }

    isValid() {
        return true
    }

    getValue = () => {
        return this.state.value.toISOString()
    }

    clearValue = () => {
        this.setState({ value: "" })
    }

    blurTime = (e) => {
        const time = this.state.timeText.split(":")
        this.setState({ value: moment(this.state.value).set("hour", time[0]).set("minute", time[1]) })
    }

    changeTime = (e) => {
        const target = e.target
        const value = target.value
        const selectionStart = target.selectionStart
        const key = e.key

        var regex = /[0-9]|\./;
        if ( regex.test(key) ) {
            e.preventDefault()

            // prevent more than 4 numbers and prevent typing over ":"
            if (selectionStart < value.length ) {
                if (
                    selectionStart === 0 && key > 2 // first number cannot be higher than 2
                    || selectionStart === 2 && key > 5 // third number cannot be higher than 5
                    || selectionStart === 3 && key > 5 // third number cannot be higher than 5
                ) {
                    return
                }

                if (selectionStart === 1) {
                    const newValue = value.substring(0, selectionStart) + key + value.substring(selectionStart + 1)
                    this.setState(
                        {
                            timeText: newValue
                        },
                        () => {
                            this.refs.timeInput.selectionStart = this.refs.timeInput.selectionEnd = selectionStart + 2
                        }
                    )
                } else if (selectionStart === 2) {
                    const newValue = value.substring(0, selectionStart + 1) + key + value.substring(selectionStart + 2)
                    this.setState(
                        {
                            timeText: newValue
                        },
                        () => {
                            this.refs.timeInput.selectionStart = this.refs.timeInput.selectionEnd = selectionStart + 2
                        }
                    )
                } else {
                    const newValue = value.substring(0, selectionStart) + key + value.substring(selectionStart + 1)
                    this.setState(
                        {
                            timeText: newValue
                        },
                        () => {
                            this.refs.timeInput.selectionStart = this.refs.timeInput.selectionEnd = selectionStart + 1
                        }
                    )
                }
            }
        }
    }

    render() {
        const days = moment.weekdaysMin(true).map((i) => (
            <span key={i}>{i}</span>
        ))

        const begin = moment(this.state.value).clone().startOf("month")
        const prefix = begin.clone().weekday(0)

        const end = moment(this.state.value).clone().endOf("month")
        const postfix = end.clone().weekday(6)

        const numbers = []
        for (const i = prefix; i.isBefore(postfix); i.add(1, "day")) {
            const current = i.clone().hours(this.state.value.hours()).minutes(this.state.value.minutes())

            numbers.push((
                <button
                    key={current.toISOString()}
                    type="button"
                    className={classnames({
                        "___other-month": current.isBefore(begin, "day") || current.isAfter(end, "day"),
                        "___is-selected": current.isSame(this.state.value, "day"),
                    })}
                    onClick={(e) => { this.setState({ value: current, isOpenDate: false }) }}
                >
                    {current.format("D")}
                </button>
            ))
        }

        const startOfDay = this.state.value.clone().startOf("day")
        const endOfDay = this.state.value.clone().endOf("day")

        const times = []
        for (const i = startOfDay; i.isBefore(endOfDay); i.add(0.25, "hour")) {
            const current = i.clone()
            times.push((
                <button
                    key={current.toISOString()}
                    type="button"
                    onClick={(e) => { this.setState({ value: current, isOpenTime: false, timeText: current.format("HH:mm") }) }}
                >{current.format("HH:mm")}</button>
            ))
        }

        return (
            <div className="row">
                <div className="col-xs-6">
                    <div className="form__date" ref="date">
                        <input placeholder="Datum" type="text" value={this.state.value.format("ddd D MMM YYYY")} readOnly onClick={this.openDate} readOnly />
                        <div ref="calendar" className={classnames({"calendar": true, "___is-open": this.state.isOpenDate})}>
                            <div className="calendar__months">
                                <button type="button" onMouseDown={this.previousMonth} />
                                <span onDoubleClick={this.resetTime}>{this.state.value.format("MMMM YYYY")}</span>
                                <button type="button" onMouseDown={this.nextMonth} />
                            </div>
                            <div className="calendar__days">{days}</div>
                            <div className="calendar__numbers">{numbers}</div>
                        </div>
                    </div>
                </div>
                <div className="col-xs-6">
                    <div className="form__time" ref="time">
                        <input ref="timeInput" placeholder="Tijd" type="text" value={this.state.timeText} onKeyDown={this.changeTime} onFocus={this.openTime} onBlur={this.blurTime} />
                        <div className={classnames({"option-list": true, "___is-open": this.state.isOpenTime})}>
                            {times}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

DateTimeField.contextTypes = {
    attachToForm: PropTypes.func,
    detachFromForm: PropTypes.func
}

export default DateTimeField