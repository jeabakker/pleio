import React from "react"
import { Link } from "react-router-dom"
import showDate from "../../lib/showDate"
import Likes from "./Likes"
import CommentEdit from "./CommentEdit"
import classnames from "classnames"
import RichTextView from "../../core/components/RichTextView"
import CommentVote from "./CommentVote"
import CommentBestAnswer from "./CommentBestAnswer"
import ShowMore from "../../core/components/ShowMore"

export default class Comment extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            editing: false
        }

        this.toggleEdit = (e) => this.setState({editing: !this.state.editing})
    }

    render() {
        let { entity, canUpvote, inActivityFeed } = this.props

        let editButton
        if (entity.canEdit) {
            editButton = (
                <div className="button ___small ___line card__edit" onClick={this.toggleEdit}>Bewerk<span>&nbsp;reactie</span></div>
            )
        }

        let vote
        if (!canUpvote) {
            vote = (
                <Likes entity={entity} />
            )
        }

        let upvote
        if (canUpvote && window.__SETTINGS__.showUpDownVoting) {
            upvote = (
                <CommentVote entity={entity} />
            )
        }

        const meta = (
            <div className="card-topic__meta">
                <span>
                    Reactie door&nbsp;
                </span>
                <Link to={entity.owner.url}>
                    {entity.owner.name}
                </Link>
                <span> op {showDate(entity.timeCreated)}</span>
            </div>
        )

        if (this.state.editing) {
            return (
                <div className="comment-container">
                    <div className="comment-edit__wrapper ___is-open" style={{maxHeight:"100%"}}>
                        <CommentEdit entity={entity} toggleEdit={this.toggleEdit} />
                    </div>
                </div>
            )
        } else {
            return (
                <div className={classnames({"comment-container": true, " ___is-editable": entity.canEdit})}>
                    <div className="comment__side">
                        {upvote}
                        <CommentBestAnswer entity={entity} />
                    </div>
                    <div className={classnames({comment: true, "___can-edit": entity.canEdit})}>
                        {editButton}
                        <div className="comment__header">
                            <a href={entity.owner.url}
                                title="Bekijk profiel"
                                style={{"backgroundImage": "url(" + entity.owner.icon + ")"}}
                                className="comment__picture">
                            </a>
                            {meta}
                            <div className="comment__actions">
                                {upvote}
                                <CommentBestAnswer entity={entity} />
                            </div>
                        </div>
                        <div className="comment__body">
                            <ShowMore lines={inActivityFeed ? 3 : 10}>
                                <RichTextView richValue={entity.richDescription} value={entity.description} />
                            </ShowMore>
                        </div>
                        {vote}
                    </div>
                </div>
            )
        }
   }
}