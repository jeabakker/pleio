import React from "react"
import Comment from "../../core/components/Comment"
import LoggedInButton from "../../core/components/LoggedInButton"
import AddComment from "../../core/containers/AddComment"
import ShowMore from '@tedconf/react-show-more'

export default class CardComments extends React.Component {
    render() {
        const {
            entity,
            viewer,
            inActivityFeed,
            canUpvote
        } = this.props

        return (
            <div className="card-comments">
                <div className="card-comments__button">
                    <LoggedInButton
                        title="Schrijf een reactie"
                        className="button article-action ___comment"
                        viewer={viewer}
                        onClick={(e) => this.refs.addComment.toggle()}
                        fromComment
                    >
                        Schrijf een reactie
                    </LoggedInButton>
                </div>
                <AddComment ref="addComment" viewer={viewer} object={entity} refetchQueries={["InfiniteList", "GroupActivityList"]} />
                <ShowMore
                    items={entity.comments}
                    by={3}
                >
                    {({
                    current,
                    onMore,
                    }) => (
                    <React.Fragment>
                        {current.map(item => (
                            <Comment
                                key={item.guid}
                                entity={item}
                                inActivityFeed={inActivityFeed}
                                canUpvote={canUpvote}
                            />
                        ))}
                        {onMore &&
                            <div className="card-comments__button">
                                <button
                                    className="button ___line ___small"
                                    disabled={!onMore}
                                    onClick={() => { if (!!onMore) onMore(); }}
                                >
                                    Meer reacties
                                </button>
                            </div>
                        }
                    </React.Fragment>
                    )}
                </ShowMore>
            </div>
        )
   }
}