import React from "react"
import classnames from "classnames"
import autobind from "autobind-decorator"

export default class LinkContextualMenu extends React.Component {
    onClick = (e) => {
        e.preventDefault()
        this.props.onUnlink()
    }

    render() {
        const {
            isVisible,
        } = this.props

        return (
            <div className={classnames({
                    "contextual": true,
                    "___is-visible": isVisible
                })}
                contentEditable={false}
            >
                <div className="contextual__tool ___unlink" onClick={this.onClick}></div>
            </div>
        )
    }
}