import React from "react"
import classnames from "classnames"

export default class ImageContextualMenu extends React.Component {

    constructor(props) {
        super(props)

        this.onUpdateSize = this.onUpdateSize.bind(this)
    }

    onUpdateSize(size) {
        this.props.onUpdateData({ size: size })
    }

    render() {
        const { size } = this.props

        return (
            <div className={classnames({"contextual": true, "___is-visible":this.props.isVisible})} style={{left: this.props.left}}>
                <div className={classnames({"contextual__tool ___small": true, "___is-active": size === "small"})} onClick={() => this.onUpdateSize("small")}/>
                <div className={classnames({"contextual__tool ___medium": true, "___is-active": size === "medium"})} onClick={() => this.onUpdateSize("medium")}/>
                <div className={classnames({"contextual__tool ___large": true, "___is-active": size === "large"})} onClick={() => this.onUpdateSize("large")}/>
                <div className="contextual__tool ___info" onClick={() => this.props.onClickInfo()} />
                <div className="contextual__tool ___delete" onClick={this.props.onDelete}></div>
            </div>
        )
    }
}
