import React from "react"
import autobind from "autobind-decorator"
import { EditorState, Modifier, SelectionState } from "draft-js"
import ImageContextualMenu from "./ImageContextualMenu"
import classnames from "classnames"
import ImageContextualInfoModal from "./ImageContextualInfoModal"
import { parseURL } from "../../../lib/helpers"
import SocialBlock from "./SocialBlock"

export default class AtomicBlock extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            showMenu: false
        }
    }

    render() {
        const { block, contentState } = this.props

        const entityKey = block.getEntityAt(0)
        if (!entityKey) {
            console.error(`Could not find atomic block entity.`)
            return ( <div /> )
        }

        const entity = contentState.getEntity(entityKey)
        const type = entity.getType()

        switch (type) {
            case "IMAGE":
                return this.renderImage(entity.getData())
            case "VIDEO":
                return this.renderVideo(entity.getData())
            case "SOCIAL":
                return this.renderSocial(entity.getData())
            default:
                console.error(`Trying to render an unknown atomic block type ${type}.`)
                return ( <div /> )
        }
    }

    onDelete = () => {
        const { block } = this.props
        const { onChange, editorState } = this.props.blockProps

        const contentState = editorState.getCurrentContent()

        const newContentState = contentState.merge({
            blockMap: contentState.blockMap.delete(block.key)
        })

        onChange(EditorState.push(editorState, newContentState, "remove-range"))
    }

    onClickInfo = () => {
        const { makeReadOnly } = this.props.blockProps
        makeReadOnly(true)
        this.refs.infoModal.toggle()
    }

    onUpdateData = (data) => {
        const { block, contentState } = this.props
        const { onChange, editorState } = this.props.blockProps

        const entityKey = block.getEntityAt(0)

        const newContentState = contentState.mergeEntityData(entityKey, data)

        onChange(EditorState.push(editorState, newContentState, "change-block-data"))
    }

    onCloseModal = () => {
        const { makeReadOnly } = this.props.blockProps
        makeReadOnly(false)
    }

    onMouseOver = (e) => {
        this.setState({showMenu: true})
    }

    onMouseOut = (e) => {
        this.setState({showMenu: false})
    }

    renderImage = (data) => {
        const { isEditor } = this.props.blockProps

        let { src, size, alt } = data

        const regex = /youtube.com\/watch\?v=(.*)/.exec(src)

        let content
        if (regex) {
            content = (
                <p className="video">
                    <iframe ref="media" src={`https://www.youtube.com/embed/${regex[1]}`} frameBorder="0" allowFullScreen />
                </p>
            )
        } else {
            content = (
                <img ref="media" src={src} alt={alt} title={alt} />
            )
        }

        if (isEditor) {
            return (
                <div className={`content__image ___${size}`} onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}>
                    {content}
                    {alt &&
                        <small>{alt}</small>
                    }
                    <ImageContextualMenu
                        isVisible={this.state.showMenu}
                        onDelete={this.onDelete}
                        onClickInfo={this.onClickInfo}
                        onUpdateData={this.onUpdateData}
                        size={size}
                    />
                    <ImageContextualInfoModal ref="infoModal" data={data} onUpdateData={this.onUpdateData} onClose={this.onCloseModal} />
                </div>
            )
        } else {
            return (
                <div className={`content__image ___${size}`}>
                    {content}
                    {alt &&
                        <small>{alt}</small>
                    }
                </div>
            )
        }
    }

    renderVideo = (data) => {
        switch (data.platform) {
            case "youtube":
                return (
                    <p className="video">
                        <iframe ref="media" src={`https://www.youtube.com/embed/${data.guid}`} frameBorder="0" allowFullScreen />
                    </p>
                )
            default:
                console.error("Trying to render invalid video platform.")
        }

        return ( <div /> )
    }

    renderSocial = (data) => {
        return ( <SocialBlock url={data.url} /> )
    }
}
