import React from "react"
import classnames from "classnames"
import { getVideoFromUrl } from "../../../lib/helpers"

export default class VideoModal extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: false,
            hasError: false,
            url: ""
        }

        this.onKeyPress = this.onKeyPress.bind(this)
        this.onChangeUrl = (e) => this.setState({ url: e.target.value })
        this.toggle = () => this.setState({ isOpen: !this.state.isOpen })
        this.onSubmit = this.onSubmit.bind(this)
    }

    onKeyPress(e) {
        const keyCode = e.keyCode ? e.keyCode : e.which
        if (keyCode !== 13) { // Enter button
            return;
        }

        e.preventDefault()
        this.onSubmit()
    }

    onSubmit(e) {
        const video = getVideoFromUrl(this.state.url)

        if (!video) {
            this.setState({ hasError: true })
            return
        }

        this.props.onSubmit("VIDEO", {
            platform: video["type"],
            guid: video["id"]
        })

        this.setState({
            url: "",
            hasError: false,
            isOpen: false
        })
    }

    render() {
        let error
        if (this.state.hasError) {
            error = (
                <div className="form__error">Dit is geen geldige link</div>
            )
		}

        return (
            <div tabIndex="0" className={classnames({"modal ___small ___middle": true, "___is-open": this.state.isOpen})}>
                <div className="modal__wrapper">
                    <div className="modal__background" onClick={this.toggle}></div>
                    <div className="modal__box">
                        <div className="modal__close" onClick={this.toggle}></div>
                        <h3 className="modal__title">Video invoegen</h3>
                        <p>Plaats hieronder de link van de Youtube video die je wilt toevoegen.</p>

                        <div className="form">
							<label className={classnames({"form__item": true, "___error": this.state.hasError})}>
                            	<input className={classnames({"___error": this.state.hasError})} type="text" ref="url" placeholder="Link"  onKeyPress={this.onKeyPress} onChange={this.onChangeUrl} value={this.state.url} />
								{error}
							</label>
                            <div className="buttons ___end">
                                <div className="button" onClick={this.onSubmit}>
                                    Invoegen
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
