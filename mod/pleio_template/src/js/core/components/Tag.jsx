import React from "react"
import classnames from "classnames"
import autobind from "autobind-decorator"
import Validator from "validatorjs"
import { generateUniqueId } from "../../lib/helpers"
import { Set } from "immutable"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import PropTypes from "prop-types"
import { Tooltip } from 'react-tippy'

class Tag extends React.Component {
    constructor(props) {
        super(props)

        this.tagRef = React.createRef()
    }

    onKeyDown = (e) => {
        const keyCode = e.keyCode ? e.keyCode : e.which
        const onRemove = this.props.onRemove

        if (keyCode === 8) { // Backspace
            e.preventDefault()
            if (this.tagRef.current.focus) {
                onRemove && onRemove()
            }
        }
    }

    onClick = (e) => {
        e.preventDefault()
        this.props.onClick && this.props.onClick()
    }

    focus() {
        this.tagRef.current.focus()
    }

    render() {
        const {
            children,
            removable
        } = this.props

        const Element = this.props.onClick ? 'button' : 'div'

        return (
            <Element
                ref={this.tagRef}
                tabIndex="0"
                className={classnames({
                    "tag": true,
                })}
                onClick={this.onClick}
                onKeyDown={this.onKeyDown}
            >
                <span>{children}</span>
                {removable &&
                    <button tabIndex="-1" type="button" className="tag__remove" onClick={this.props.onRemove} />
                }
            </Element>
        )
    }
}

export default Tag