import React from "react"
import autobind from "autobind-decorator"
import classnames from "classnames"

import LinkContextualMenu from "./RichText/LinkContextualMenu"

export default class Link extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            showContextual: false
        }
    }

    onMouseOver = (e) => {
        this.setState({showContextual: true})
    }

    onMouseOut = (e) => {
        this.setState({showContextual: false})
    }

    render() {
        const {
            href,
            target,
            children,
            onUnlink
        } = this.props

        return (
            <a
                className="editor__link"
                href={href}
                target={target}
                onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}
            >
                {children}
                <LinkContextualMenu
                    isVisible={this.state.showContextual}
                    onUnlink={onUnlink}
                />
            </a>
        )
    }
}