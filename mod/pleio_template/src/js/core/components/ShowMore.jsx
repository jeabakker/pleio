import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Truncate from 'react-truncate';
import autobind from 'autobind-decorator';

export default class ShowMore extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = {
            expanded: false,
            isTruncated: true
        };
    }

    handleTruncate = (isTruncated) => {
        this.setState({
            isTruncated
        })
    }

    expandLines = (e) => {
        this.setState({
            expanded: true
        });
    }

    render() {
        const {
            children,
            more,
            lines
        } = this.props;

        const {
            expanded,
            isTruncated
        } = this.state;

        if (!isTruncated || expanded) {
            return children
        }

        return (
            <Truncate
                lines={lines}
                ellipsis={(
                    <span>...&nbsp;&nbsp;<button className="button ___line ___small" onClick={this.expandLines}>{more}</button></span>
                )}
                onTruncate={this.handleTruncate}
            >
                {children}
            </Truncate>
        );
    }
}

ShowMore.defaultProps = {
    lines: 3,
    more: 'Lees meer'
};

ShowMore.propTypes = {
    children: PropTypes.node.isRequired,
    text: PropTypes.node,
    lines: PropTypes.number
};