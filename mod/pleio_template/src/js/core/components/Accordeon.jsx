import React from "react"
import classnames from "classnames"

export default class Accordeon extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: false
        }

        this.toggleOpen = (e) => this.setState({isOpen: !this.state.isOpen})
        this.calculateHeight = this.calculateHeight.bind(this)
    }

    calculateHeight() {
        if (this.state.isOpen) {
            return this.refs["items"].firstChild.offsetHeight;
        }

        return 0
    }

    render() {
        let defaultClassnames = {
            "card": true,
            "___is-open": this.state.isOpen,
            "___side": this.props.side
        }

        if (this.props.additionalClassname) {
            defaultClassnames = Object.assign({}, defaultClassnames, {
                [this.props.additionalClassname]: true
            })
        }

        return (
            <div className={classnames(defaultClassnames)}>
                <div className="card__content">
                    <div className={"card__title accordion__trigger ___tablet"} onClick={this.toggleOpen}>
                        {this.props.title}
                    </div>
                    <div ref="items" className={"card__items accordion__content"} style={this.calculateHeight() ? {height: this.calculateHeight()} : {}} data-accordion-content>
                        <div>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}