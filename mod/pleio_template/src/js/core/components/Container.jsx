import React from "react"
import TopMenu from "../containers/TopMenu"
import Logo from "./Logo"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import autobind from "autobind-decorator"
import { withGlobalState } from 'react-globally'
import CookieNotification from './CookieNotification'

class Container extends React.Component {
    constructor(props) {
        super(props)
    }

    toggleEditMode = () => {
        this.props.setGlobalState({
            editMode: !this.props.globalState.editMode
        })
    }

    render() {
        const editMode = this.props.globalState.editMode
        const { site, viewer } = this.props.data

        let cookieNotification
        if (window.__SETTINGS__.site['cookieConsent'] && viewer && !viewer.loggedIn) {
            cookieNotification = (
                <CookieNotification />
            )
        }

        return (
            <div className="page-layout">
                <header className="page-layout__header">
                    {site && site.theme === "rijkshuisstijl" &&
                        <Logo  data={this.props.data} />
                    }
                    <TopMenu data={this.props.data} />
                </header>
                <main id="skip-navigation" className="page-layout__main ___no-padding">
                    {cookieNotification}
                    {this.props.children}
                </main>
                {viewer && viewer.isSubEditor &&
                    <div className="cms__panel">
                        {!editMode &&
                            <button className="___edit" onClick={this.toggleEditMode} />
                        }
                        {editMode &&
                            <React.Fragment>
                                { viewer.isAdmin &&
                                    <a href="/admin/" className="___settings" />
                                }
                                <button className="___close" onClick={this.toggleEditMode} />
                            </React.Fragment>
                        }
                    </div>
                }
            </div>
        )
    }
}

const Query = gql`
    query TopMenu {
        site {
            guid
            logo
            showLogo
            theme
            menu {
                title
                link
                children {
                    title
                    link
                }
            }
            footer {
                title
                link
            }
        }
        viewer {
            guid
            loggedIn
            isAdmin
            isSubEditor
            user {
                guid
                username
                name
                icon
            }
        }
    }
`

export default withGlobalState(graphql(Query)(Container))