import React, { Component } from 'react'
import Validator from 'validatorjs'
import PropTypes from 'prop-types'

class InputField extends Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
        this.forceUpdate = this.forceUpdate.bind(this)
        this.state = {
            value: this.props.value ? this.props.value : ''
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({
                value: nextProps.value
            })
        }
    }

    componentWillMount() {
        if (this.context.attachToForm) {
            this.context.attachToForm(this)
        }
    }

    componentWillUnmount() {
        if (this.context.detachFromForm) {
            this.context.detachFromForm(this)
        }
    }

    onChange(e) {
        this.setState(
            { value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e)
                }
            }
        )
    }

    isValid() {
        const { type, rules } = this.props
        const { value } = this.state

        if (rules) {
            const typedValue = ['number', 'tel'].includes(type) ? Number(value) : value
            const validation = new Validator({ field: typedValue }, { field: rules })
            return validation.passes()
        }

        return true
    }

    forceUpdate() {
        this.setState({
            value: this.refs.field.value
        })
    }

    getValue() {
        return this.state.value
    }

    clearValue() {
        this.setState({
            value: ''
        })
    }

    render() {
        const {
            name,
            type,
            className,
            placeholder,
            disabled,

            // Omit from otherProps
            value,
            onChange,
            ...otherProps
        } = this.props

        return (
            <input
                ref="field"
                name={name}
                type={type}
                className={className}
                placeholder={placeholder}
                onChange={this.onChange}
                value={this.state.value}
                disabled={disabled}
                id={this.props.id}
                {...otherProps}
            />
        )
    }
}

InputField.contextTypes = {
    attachToForm: PropTypes.func,
    detachFromForm: PropTypes.func
}

InputField.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf([
        'text',
        'number',
        'tel',
        // Others not supported yet
    ]),
    className: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    value: PropTypes.string,
    onChange: PropTypes.func,
    // Others allowed and passed through. Will be caught by React.
}

InputField.defaultProps = {
    type: 'text',
    className: '',
    placeholder: '',
    disabled: false,
    value: '',
    onChange: null
}

export default InputField
