import React from "react"
import Validator from "validatorjs"
import PropTypes from "prop-types"
import autobind from "autobind-decorator"

class MultipleInputField extends React.Component {
    constructor(props) {
        super(props)

        let values = {}
        let i = 2

        if (this.props.value) {
            i = 1
            this.props.value.forEach((value) => {
                values[i] = value
                i += 1
            })

            i -= 1
        }

        this.state = {
            values: values,
            size: i
        }
    }

    componentWillMount() {
        if (this.context.attachToForm) {
            this.context.attachToForm(this)
        }
    }

    componentWillUnmount() {
        if (this.context.detachFromForm) {
            this.context.detachFromForm(this)
        }
    }

    addField = (e) => {
        e.preventDefault()
        this.setState({ size: Math.min(this.state.size+1, 6)})
    }

    deleteField = (e) => {
        e.preventDefault()
        this.setState({ size: Math.max(this.state.size-1, 2)})
    }

    onChange = (i, e) => {
        this.setState({
            values: Object.assign({}, this.state.values, {
                [i]: e.target.value
            })
        })

        if (this.props.onChange) {
            this.props.onChange()
        }
    }

    isValid = () => {
        for (let i = 1; i <= this.state.size; i++) {
            if (!this.state.values[i]) {
                return false
            }
        }

        return true
    }

    getValue = () => {
        let returnValue = []
        for (let i = 1; i <= this.state.size; i++) {
            returnValue.push(this.state.values[i])
        }

        return returnValue
    }

    clearValue = () => {
        this.setState({ values: {} })
    }

    render() {
        let inputs = []
        for (let i = 1; i <= this.state.size; i++) {
            inputs.push((
                <label key={i} className="form__item">
                    <input
                        name={this.props.name}
                        type={this.props.type}
                        className={this.props.className}
                        placeholder={`${this.props.placeholder} ${i}`}
                        onChange={(e) => this.onChange(i, e)}
                        value={this.state.values[i] || ""}
                        disabled={this.props.disabled}
                    />
                </label>
            ))
        }

        return (
            <React.Fragment>
                {inputs}
                <div className="buttons ___space-between">
                    <button className="button ___line ___margin-top ___margin-bottom" onClick={this.addField}>Optie toevoegen</button>
                    <button className="button ___line ___margin-top ___margin-bottom" onClick={this.deleteField}>Optie verwijderen</button>
                </div>
            </React.Fragment>
        )
    }
}

MultipleInputField.contextTypes = {
    attachToForm: PropTypes.func,
    detachFromForm: PropTypes.func
}

export default MultipleInputField