import React from "react"
import { Link } from "react-router-dom"

export default class Logo extends React.Component {
    render() {
        const { site } = this.props.data

        return (
            <div className="header__top">
                <div className="container">
                    <div className="header__logo">
                        <Link to="/" title="Terug naar home">
                            <img src={site.logo} alt={site.name} />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}