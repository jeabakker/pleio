import React from "react"
import { Link } from "react-router-dom"

export default class UserCard extends React.Component {
    render() {
        const { entity } = this.props

        return (
            <Link to={entity.url} className="list-members__member">
                <div className="list-members__picture" style={{ backgroundImage: `url('${entity.icon}')` }}  />
                <div className="list-members__name">
                    {entity.name}
                </div>
            </Link>
        )
    }
}
