import React from "react"
import PropTypes from "prop-types"
import autobind from "autobind-decorator"
import { generateUniqueId } from "../../lib/helpers"
import { Set } from "immutable"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { Tooltip } from "react-tippy"
import Tag from "./Tag"

class TagField extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            selectedTags: this.props.selectedTags ? this.props.selectedTags : [],
            suggestedTags: [],
            inputValue: '',
            showTooltip: false
        }

        this.onChangeInput = (e) => this.setState(
            {inputValue: e.target.value}
        )

        this.inputRef = React.createRef()
        this.lastTagRef = React.createRef()
    }

    componentWillMount() {
        if (this.context.attachToForm) {
            this.context.attachToForm(this)
        }
    }

    componentWillUnmount() {
        if (this.context.detachFromForm) {
            this.context.detachFromForm(this)
        }
    }

    toggleTooltip = () => {
        this.setState({
            showTooltip: !this.state.showTooltip
        })
    }

    hideTooltip = () => {
        this.setState({
            showTooltip: false
        })
    }

    onKeyDown = (e) => {
        const keyCode = e.keyCode ? e.keyCode : e.which

        if (keyCode === 13) { // Enter
            e.preventDefault()
            const value = this.state.inputValue
            let tags = value.split(',').filter(Boolean)
            this.addTag(tags)
        }
        if (keyCode === 8) { // Backspace
            const lastTag = this.lastTagRef.current
            if (lastTag && !this.state.inputValue) {
                lastTag.focus()
            }
        }
    }

    addTag = (tags) => { // Expects array
        const newSelectedTags = [...new Set([...this.state.selectedTags ,...tags])]
        this.setState({
            selectedTags: newSelectedTags,
            inputValue: ''
        },
        () => {
            this.props.onChange && this.props.onChange(newSelectedTags)
            this.inputRef.current.focus()
        })
    }

    removeFromArray(array, element) {
        const index = array.indexOf(element)
        let newArray = array
        if (index !== -1) {
            newArray.splice(index, 1)
            return newArray
        }
        return newArray
    }

    removeTag = (tag) => {
        const newSelectedTags = this.removeFromArray(this.state.selectedTags, tag)

        this.setState({ selectedTags: newSelectedTags },
        () => {
            this.props.onChange && this.props.onChange(newSelectedTags)
            this.inputRef.current.focus()
        })
    }

    escapeString(input) {
        return input.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    getValue() {
        return this.state.selectedTags
    }

    isValid() {
        return true
    }

    render() {
        const {
            label,
            helper,
            icon
        } = this.props

        const { site } = this.props.data

        let showSuggestions
        if (site) {
            const selectedTags = this.state.selectedTags

            const predefinedTags = selectedTags.length === 0 ? site.predefinedTags : site.predefinedTags.filter(function(predefinedTag) {
                if (selectedTags.indexOf(predefinedTag.tag) < 0) {
                    return predefinedTag.tag
                }
            })

            showSuggestions = predefinedTags.map((tag, i) => (
                <Tag key={i}
                    onClick={() => this.addTag([tag.tag])}
                >
                    {tag.tag}
                </Tag>
            ))
        }

        const filterTooltip = (
            <div className="card__content">
                <div className="flexer ___gutter-small" style={{flexWrap: 'wrap'}}>
                    {showSuggestions}
                </div>
            </div>
        )

        const selectedTags = this.state.selectedTags.map((tag, i) => {
        const isLastTag = this.state.selectedTags.length === (i + 1) ? true : false

        return (
            <Tag key={i}
                ref={isLastTag && this.lastTagRef}
                removable
                onRemove={() => this.removeTag(tag)}
            >
                {tag}
            </Tag>
            )
        })

        const id = generateUniqueId("field-")

        return (
            <div>
                {label &&
                    <label htmlFor={id} className="form__label">{label}</label>
                }
                <div className="flexer ___nowrap">
                    {icon &&
                        <div className="tagfield_icon" />
                    }
                    <label htmlFor={id} className="tagField">
                        <Tooltip
                            open={this.state.showTooltip}
                            onRequestClose={() => this.hideTooltip()}
                            html={filterTooltip}
                            position="bottom"
                            trigger="click"
                            duration={200}
                            arrow={true}
                            theme="light"
                            interactive={true}
                        >
                            <div className="tagfield_tags">
                                {selectedTags}
                                <input
                                    ref={this.inputRef}
                                    name={this.props.name}
                                    type={this.props.type}
                                    className="tagfield_input"
                                    id={id}
                                    autoComplete="off"
                                    placeholder={this.props.placeholder}
                                    onKeyDown={this.onKeyDown}
                                    onChange={this.onChangeInput}
                                    value={this.state.inputValue}
                                />
                            </div>
                            <button type="button" className="tagfield_expand button__icon ___dropdown" onClick={() => this.toggleTooltip()} style={this.state.showTooltip ? {transform: 'scaleY(-1)'} : {}} />
                        </Tooltip>
                    </label>
                </div>
                {helper &&
                    <small>{helper}</small>
                }
            </div>
        )
    }
}

TagField.contextTypes = {
    attachToForm: PropTypes.func,
    detachFromForm: PropTypes.func
}

const Query = gql`
    query PredefinedTags {
        site {
            guid
            predefinedTags {
                tag
            }
        }
    }
`

export default graphql(Query)(TagField)