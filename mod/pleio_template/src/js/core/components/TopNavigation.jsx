import React from "react"
import { Link, withRouter } from "react-router-dom"
import Tooltip from "../../core/components/Tooltip"
import autobind from "autobind-decorator"

class TopNavigation extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isVisible: false
        }
    }

    toggleVisibility = (e) => {
        this.setState({ isVisible: !this.state.isVisible })
    }

    onBlur = (e) => {
        this.setState({ isVisible: false })
    }

    render() {
        const { location } = this.props
        const { viewer } = this.props.data

        let userMenu
        if (viewer.loggedIn) {
            const toolTipList = [
                [ { title: "Bewaard", link: "/saved", className: "___bookmarks" }, ],
                [
                    { title: "Profiel", link: `/profile/${viewer.user.username}` },
                    { title: "Meldingen", link: `/profile/${viewer.user.username}/interests` },
                    { title: "Instellingen", link: `/profile/${viewer.user.username}/settings` }
                ],
                [
                    { title: "Naar Pleio.nl", link: `https://www.pleio.nl`, external:true },
                    { title: "Uitloggen", link: "/logout" }
                ]
            ]
            userMenu = (
            <div tabIndex="0" className="___right top-navigation__action ___account" onClick={this.toggleVisibility} onBlur={this.onBlur}>
                    <img src={viewer.user.icon} alt={viewer.user.name} title={viewer.user.name} />
                    {viewer.user.name}
                <Tooltip lists={toolTipList} isVisible={this.state.isVisible} />
            </div>
            )



        } else {
            userMenu = (
                <Link to={{pathname: "/login", state: { next: location.pathname }}} title="Inloggen" className="top-navigation__link ___right">
                    Inloggen
                </Link>
            )
        }

        return (
            <div className="top-navigation">
                <div className="container">
                    <div className="dropdown ___left">
                        <a href="https://www.pleio.nl" title="Pleio" className="top-navigation__link ___pleio">
                            Pleio
                        </a>
                    </div>
                    {userMenu}
                </div>
            </div>
        )
    }
}

export default withRouter(TopNavigation)