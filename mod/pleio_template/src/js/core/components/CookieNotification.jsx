import React from "react"
import autobind from 'autobind-decorator'
import { createCookie, readCookie } from '../../lib/cookies'

export default class CookieNotification extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            hidden: readCookie('cookieConsent')
        }
    }

    hideNotification = () => {
        createCookie('cookieConsent', true, 365)
        this.setState({
            hidden: true
        })
    }

    render() {
        if (this.state.hidden) {
            return null
        }
        else {
            return (
                <div className="cookie-notification">
                    <div className="container">
                        <div className="flexer ___gutter ___nowrap">
                            <span>
                                Om content te kunnen personaliseren en gebruik te kunnen analyseren gebruikt Pleio cookies. Door de te klikken of door de site te navigeren, gaat u akkoord met het verzamelen van gegevens. Meer informatie over het cookie beleid van Pleio is te zien via de link: <a href="https://www.pleio.nl/cookieverklaring" target="_blank">Cookieverklaring</a>.
                            </span>
                            <button className="button__icon ___close" onClick={this.hideNotification}></button>
                        </div>
                    </div>
                </div>
            )
        }
    }
}