import React from "react"
import gql from "graphql-tag"
import autobind from "autobind-decorator"
import { graphql } from "react-apollo"
import { convertToRaw } from "draft-js"
import Form from "../../core/components/Form"
import RichTextField from "../../core/components/RichTextField"
import { logErrors } from "../../lib/helpers"
import classnames from 'classnames'

class AddCommentForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: [],
            loading: false
        }
    }

    onSubmit = (e) => {
        e.preventDefault()

        this.setState({
            errors: null,
            loading: true
        })

        const values = this.refs.form.getValues()

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    type: "object",
                    subtype: "comment",
                    description: values.description.getPlainText(),
                    richDescription: JSON.stringify(convertToRaw(values.description)),
                    containerGuid: this.props.object.guid
                }
            },
            refetchQueries: this.props.refetchQueries
        }).then(({data}) => {
            if (this.props.onSuccess) {
                this.props.onSuccess()
            }
            this.refs.description.clearValue()
            this.setState({
                loading: false
            })
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors,
                loading: false
            })
        })
    }

    render() {
        let icon, name, url
        if (this.props.user) {
            ({icon, name, url} = this.props.user)
        }

        return (
            <Form ref="form" className="comment-add" onSubmit={this.onSubmit}>
                <div className="flexer ___space-between">
                    <h3 className="comment-add__title">Reageer</h3>
                    <div title="Terug" className="comment-add__close" onClick={this.props.toggle}></div>
                </div>
                <div className="comment-add__top">
                    <img src={icon} className="comment-add__image" />
                    <div href={url} title="Bekijk profiel" className="comment-add__name">{name}</div>
                </div>
                <RichTextField ref="description" name="description" placeholder="Voeg een reactie toe..." className="comment-add__content" rules="required" />
                <div className="comment-add__bottom form__actions">
                    <button type="submit" className={classnames({"button": true, "___is-loading": this.state.loading})}>
                        Reageer
                        <div className="button__loader"></div>
                    </button>
                </div>
            </Form>
        )
    }
}

const AddComment = gql`
    mutation AddComment($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on Object {
                    isFollowing
                }
            }
        }
    }
`
const withQuery = graphql(AddComment)
export default withQuery(AddCommentForm)