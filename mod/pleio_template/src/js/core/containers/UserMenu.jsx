import React from "react"
import { NavLink, withRouter } from "react-router-dom"
import NotificationsTop from "../../notifications/components/NotificationsTop"
import Tooltipp from "../../core/components/Tooltip"
import autobind from "autobind-decorator"
import { Tooltip } from 'react-tippy'
import { withGlobalState } from 'react-globally'

class UserMenu extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            q: "",
            isVisible: false
        }

        this.onChange = (e) => this.setState({ q: e.target.value })
    }

    toggleVisibility = (e) => {
        this.props.setGlobalState({
            savedItem: false
        })
        this.setState({ isVisible: !this.state.isVisible })
    }

    onBlur = (e) => {
        this.setState({ isVisible: false })
    }

    onSearch = (e) => {
        e.preventDefault()

        const { history } = this.props
        history.push(`/search/results?q=${this.state.q}`)
    }

    render() {
        const savedItem = this.props.globalState.savedItem
        const { viewer, site } = this.props

        if (!viewer) {
            return (
                <div></div>
            )
        }

        if (viewer.loggedIn) {
            const userMenu = [
                [ { title: "Bewaard", link: "/saved", className: "___bookmarks" }, ],
                [
                    { title: "Profiel", link: `/profile/${viewer.user.username}` },
                    { title: "Meldingen", link: `/profile/${viewer.user.username}/interests` },
                    { title: "Instellingen", link: `/profile/${viewer.user.username}/settings` }
                ],
                [
                    { title: "Naar Pleio.nl", link: `https://www.pleio.nl`, external:true },
                    { title: "Uitloggen", link: "/logout" }
                ]
            ]

            return (
                <div className="navigation__actions">
                    <form className="navigation__search" onSubmit={this.onSearch}>
                        <div className="search-bar">
                            <input name="q" onChange={this.onChange} value={this.state.q} placeholder="Zoeken" />
                            <button className="search-bar__button" type="submit" />
                        </div>
                    </form>
                    <NotificationsTop />
                    <Tooltip
                        title="Bewaard in profiel"
                        position="bottom"
                        trigger="manual"
                        open={savedItem}
                        duration={200}
                        arrow={true}
                    >
                        <div tabIndex="0" className="navigation__action ___account" onClick={this.toggleVisibility} onBlur={this.onBlur}>
                            <div style={{backgroundImage: "url('" + viewer.user.icon + "')"}} className="navigation__picture"></div>
                            <span>{viewer.user.name}</span>
                            <Tooltipp lists={userMenu} isVisible={this.state.isVisible} />
                        </div>
                    </Tooltip>
                </div>
            )
        } else {
            return (
                <div className="navigation__actions">
                    <form className="navigation__search" onSubmit={this.onSearch}>
                        <div className="search-bar">
                            <input name="q" onChange={this.onChange} value={this.state.q} placeholder="Zoeken" />
                            <button className="search-bar__button" type="submit" />
                        </div>
                    </form>
                    <div className="navigation__action navigation__login-or-register">
                        <NavLink to={{pathname: "/login", state: { next: location.pathname }}} title="Inloggen" className="___login">
                            Inloggen
                        </NavLink>
                        <NavLink to="/register" title="Registeren" className="___register">
                            Registreren
                        </NavLink>
                    </div>
                </div>
            )
        }
    }
}

export default withGlobalState(withRouter(UserMenu))