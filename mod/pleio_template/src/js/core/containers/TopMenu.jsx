import React from "react"
import { NavLink, withRouter } from "react-router-dom"
import UserMenu from "./UserMenu"
import UserMobileMenu from "./UserMobileMenu"
import classnames from "classnames"
import autobind from "autobind-decorator"

class TopMenu extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            submenuOpen: null,
            q: "",
            navHeight: null,
            navSticky: false
        }

        this.changeSearchField = (e) => this.setState({q: e.target.value})

        this.navRef = React.createRef();
        this.placeholderRef = React.createRef();
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleResize = () => {
        this.checkNavHeight()
    }

    handleScroll = () => {
        const placeholderTop = this.placeholderRef.current.getBoundingClientRect().top

        // Check height only once (after this rely on handleResize)
        if (!this.state.navHeight) {
            this.checkNavHeight()
        }

        this.setState({
            navSticky: placeholderTop < 0 ? true: false
        })
    }

    checkNavHeight = () => {
        const navHeight = this.navRef.current.getBoundingClientRect().height

        if (navHeight !== this.state.navHeight) {
            this.setState({
                navHeight: navHeight
            })
        }
    }

    toggleMobileMenu(e) {
        document.body.classList.toggle("mobile-nav-open")
        document.body.classList.toggle("mega-nav-open")
    }

    closeMobileMenu(e) {
        document.body.classList.remove("mobile-nav-open")
        document.body.classList.remove("mega-nav-open")
    }

    openSubmenu = (i) => {
        this.setState({ submenuOpen: i })
    }

    onSearch = (e) => {
        e.preventDefault()
        const { history } = this.props

        this.closeMobileMenu()
        history.push(`/search/results?q=${this.state.q}`)
    }

    render() {
        const { site, viewer } = this.props.data
        let footerItems, home, userMenu

        if (!site) {
            return (
                <div />
            )
        }

        if (site.showLogo) {
            home = (
                <li>
                    <NavLink exact to="/" onClick={this.closeMobileMenu} title="Home" className="navigation__link ___home" activeClassName="___is-active">Home</NavLink>
                </li>
            )
        } else {
            home = (
                <li>
                    <NavLink exact to="/" onClick={this.closeMobileMenu} title="Home" className="navigation__link" activeClassName="___is-active">Home</NavLink>
                </li>
            )
        }

        const menuItems = site.menu.map((item, i) => {
            const subItems = item.children.map((child, j) => (
                <li key={j} className="submenu__list-item">
                    <NavLink to={child.link || "#"} onClick={this.closeMobileMenu} title={child.title} activeClassName="___is-active">{child.title}</NavLink>
                </li>
            ))

            let link
            if (item.children.length > 0) {
                link = (
                    <NavLink to={"#"} onClick={(e) => this.openSubmenu(i)} title={item.title} className={classnames({"navigation__link": true, "___dropdown": (item.children.length > 0)})} activeClassName="___is-active">
                        {item.title}
                    </NavLink>
                )
            } else {
                link = (
                    <NavLink to={item.link || "#"} onClick={this.closeMobileMenu} title={item.title} className={classnames({"navigation__link": true, "___dropdown": (item.children.length > 0)})} activeClassName="___is-active">
                        {item.title}
                    </NavLink>
                )
            }

            return (
                <li key={i} className="navigation__dropdown">
                    {link}
                    {(item.children.length) > 0 && (
                        <div className={classnames({"submenu ___dropdown": true, "___open": (this.state.submenuOpen === i)})}>
                            <div className="submenu__back" onClick={(e) => this.openSubmenu(null)}>Terug</div>
                            <ul className="submenu__list">
                                <li className="submenu__list-subject">
                                    <a title={item.title}>{item.title}</a>
                                </li>
                                {subItems}
                            </ul>
                        </div>
                    )}
                </li>
            )
        })

        userMenu = (
            <UserMenu onClick={this.closeMobileMenu} viewer={viewer} site={site} />
        )

        if (site.footer.length) {
            footerItems = site.footer.map((item, i) => (
                <NavLink key={i} to={item.link || "#"} onClick={this.closeMobileMenu} title={item.title} activeClassName="___is-active">{item.title}</NavLink>
            ))
        }

        const placeholderStyle = {
            height: this.state.navHeight
        }

        const stickyStyle = {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            boxShadow: '0 1px 6px 0 rgba(0, 0, 0, 0.03), 0 20px 20px -15px rgba(0, 0, 0, 0.03)'
        }

        return (
            <React.Fragment>
            <div ref={this.placeholderRef} style={this.state.navSticky ? placeholderStyle : {}}></div>
            <nav
                ref={this.navRef} className={"navigation " + (this.props.className || "") + " " + classnames({"nav-level-one": (this.state.submenuOpen !== null)})}
                style={this.state.navSticky ? stickyStyle : {}}
            >
                <div className="container">
                    <div className="navigation-overlay" onClick={this.toggleMobileMenu} />
                    <div className="navigation__wrapper">
                        <div className="mobile-navigation__close" onClick={this.toggleMobileMenu}>
                            <span className="icon icon-cross"></span>
                        </div>
                        <div className="mobile-navigation__search">
                            <form onSubmit={this.onSearch}>
                                <label htmlFor="mobile-navigation-search">Zoeken</label>
                                <input id="mobile-navigation-search" placeholder="Zoeken" name="q" onChange={this.changeSearchField} value={this.state.q} />
                            </form>
                        </div>
                        <ul className="navigation__links">
                            {home}
                            {menuItems}
                            {footerItems &&
                                <li className="navigation__dropdown ___mobile">
                                    <a title="Meer" className="navigation__link ___dropdown" onClick={(e) => this.openSubmenu("footer")}>Meer</a>
                                    <div className={classnames({"submenu ___dropdown":true, "___open": (this.state.submenuOpen === "footer")})}>
                                        <div className="submenu__back" onClick={(e) => this.openSubmenu(null)}>Terug</div>
                                        <ul className="submenu__list">
                                            <li className="submenu__list-subject">
                                                <a title="Meer">Meer</a>
                                            </li>
                                            <li className="submenu__list-item">
                                                {footerItems}
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            }
                        </ul>
                        {userMenu}
                    </div>
                    <div className="mobile-navigation__bar">
                        <div className="mobile-navigation__trigger" onClick={this.toggleMobileMenu} />
                        <UserMobileMenu onClick={this.closeMobileMenu} viewer={this.props.data.viewer} />
                    </div>
                </div>
            </nav>
            </React.Fragment>
        )
    }
}

export default withRouter(TopMenu)