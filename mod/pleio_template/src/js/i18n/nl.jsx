export const Errors = {
    already_registered: 'Dit e-mailadres is reeds geregistreerd.',
    already_voted: 'Je hebt al gestemd.',
    could_not_login: 'Het inloggen is mislukt. Controleer de gebruikersnaam en het wachtwoord en probeer het opnieuw.',
    could_not_register: 'Het registreren is mislukt. Controleer de gegevens en probeer het opnieuw.',
    event_is_full: 'Het maximaal aantal aanwezigen is bereikt.',
    invalid_answer: 'Kies een geldig antwoord.',
    invalid_code: 'De code is niet meer geldig of is al gebruikt.',
    invalid_old_password: 'Het oude wachtwoord is niet correct.',
    not_logged_in: 'Je bent niet ingelogd. Log in en probeer het opnieuw.',
    passwords_not_the_same: 'De ingevulde wachtwoorden komen niet overeen.',
    unknown_error: 'Een onbekende fout is opgetreden. Probeer het opnieuw.',
}
