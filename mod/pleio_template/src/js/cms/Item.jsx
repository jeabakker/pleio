import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { Link } from "react-router-dom"
import NotFound from "../core/NotFound"
import RichTextView from "../core/components/RichTextView"
import Document from "../core/components/Document"
import AddModal from "./components/AddModal"
import update from "immutability-helper"
import AddRow from "./components/AddRow"
import Row from "./components/Row"
import SubNav from "./components/SubNav"
import { withGlobalState } from 'react-globally'
import { DragDropContext, Droppable } from "react-beautiful-dnd"

class Item extends React.Component {
    constructor(props) {
        super(props)

        this.state = { rows: [] }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.entity) {
            return
        }

        this.setState({ rows: data.entity.rows })
    }

    onDragEnd = (result) => {
        if (!result.destination) {
            return
        }

        this.setState({
            rows: update(this.state.rows, {
                $splice: [
                    [result.source.index, 1],
                    [result.destination.index, 0, this.state.rows[result.source.index]]
                ]
            })
        })

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    guid: result.draggableId,
                    sourcePosition: result.source.index,
                    destinationPosition: result.destination.index
                }
            }
        })
    }

    deleteWidget = (entity) => {
        this.setState({ deleteEntity: entity })
        this.refs.deleteWidget.toggle()
    }

    render() {
        const { globalState, data, match } = this.props
        const { editMode } = globalState
        const { entity } = data

        if (!entity) {
            return ( <div /> )
        }

        if (entity.status == 404) {
            return ( <NotFound /> )
        }

        if (entity.pageType == "text") {
            let subNav
            if (entity.hasChildren || match.params.containerGuid) {
                subNav = (
                    <div className="col-sm-4 col-lg-3">
                        <SubNav containerGuid={match.params.containerGuid || match.params.guid} guid={match.params.containerGuid || match.params.guid} />
                    </div>
                )
            }

            return (
                <React.Fragment>
                    <Document title={entity.title} />
                    {editMode &&
                        <section className="section">
                            <div className="container">
                                <div className="flexer ___gutter ___end">
                                    <Link to={`/cms/edit/${entity.guid}`}>
                                        <button className="button ___line ___large ___edit">Bewerk pagina</button>
                                    </Link>
                                    <button className="button ___add ___large" onClick={(e) => this.refs.addModal.toggle()}>
                                        Maak een subpagina
                                    </button>
                                </div>
                            </div>
                        </section>
                    }
                    <section className="section ___grey ___grow">
                        <div className="container">
                            <div className="row">
                                {subNav}
                                <div className={(entity.hasChildren || match.params.containerGuid) ? "col-sm-8 col-lg-9" : "col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2"}>
                                    <div className="card">
                                        <div className="card__content">
                                            <article className="article">
                                                <h3 className="article__title">{entity.title}</h3>
                                                <RichTextView richValue={entity.richDescription} value={entity.description} />
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <AddModal ref="addModal" entity={entity} />
                </React.Fragment>
            )
        } else {
            const rows = this.state.rows.map((row, i) => (
                <Row key={i} rowIndex={i} entity={row} firstRow={i === 0 ? true : false} />
            ))

            return (
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Document title={entity.title} />
                    {editMode &&
                        <section className="section">
                            <div className="container">
                                <div className="flexer ___gutter ___end">
                                    <Link to={`/cms/edit/${entity.guid}`}>
                                        <button className="button ___line ___large ___edit">Bewerk pagina</button>
                                    </Link>
                                </div>
                            </div>
                        </section>
                    }
                    <Droppable type="row" droppableId={entity.guid}>
                        {(provided, snapshot) => (
                            <div ref={provided.innerRef}>
                                {rows}
                                {editMode &&
                                    <AddRow firstRow={this.state.rows.length === 0 ? true : false} containerGuid={entity.guid} />
                                }
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            )
        }
    }
}

const Query = gql`
    query PageItem($guid: Int!) {
        entity(guid: $guid) {
            guid
            status
            ... on Page {
                title
                canEdit
                hasChildren
                pageType
                description
                richDescription
                rows {
                    guid
                    layout
                    canEdit
                    widgets {
                        guid
                        type
                        canEdit
                        position
                        settings {
                            key
                            value
                        }
                    }
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.guid
            }
        }
    }
}

const Mutation = gql`
    mutation Item($input: reorderInput!) {
        reorder(input: $input) {
            container {
                guid
                ... on Page {
                    rows {
                        guid
                    }
                }
            }
        }
    }
`

export default withGlobalState((graphql(Mutation)(graphql(Query, Settings)(Item))))