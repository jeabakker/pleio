import React from "react"
import { List } from "immutable"
import { NavLink, withRouter } from "react-router-dom"
import classnames from "classnames"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import autobind from "autobind-decorator"
import SubSubNav from "./SubSubNav"
import { withGlobalState } from 'react-globally'

class SubNav extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            menu: List()
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.entity) {
            return
        }

        const menu = data.entity.children.map((child) => {
            return {
                guid: child.guid,
                title: child.title,
                canEdit: child.canEdit,
                children: child.children.length ? List(child.children) : null
            }
        })

        this.setState({
            menu: List(menu)
        })
    }

    onDragEnd = (result) => {
        const { entity } = this.props.data

        if (!result.destination) {
            return
        }

        if (result.type === "subnav") {
            const sourceRemoved = this.state.menu.splice(result.source.index, 1)
            const newMenu = sourceRemoved.splice(result.destination.index, 0, this.state.menu.get(result.source.index))
            this.setState({ menu: newMenu })
        } else {
            const container = this.state.menu.find((object) => (object['guid'] === result.destination.droppableId))
            const containerIndex = this.state.menu.indexOf(container)

            const sourceRemoved = container.children.splice(result.source.index, 1)
            const newChildren = sourceRemoved.splice(result.destination.index, 0, container.children.get(result.source.index))

            const newContainer = {
                guid: container.guid,
                title: container.title,
                children: newChildren
            }

            const newMenu = this.state.menu.set(containerIndex, newContainer)
            this.setState({ menu: newMenu })
        }

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    guid: result.draggableId,
                    sourcePosition: result.source.index,
                    destinationPosition: result.destination.index
                }
            }
        })
    }

    render() {
        const editMode = this.props.globalState.editMode
        const { match } = this.props
        const { entity } = this.props.data

        if (!entity) {
            return (
                <div />
            )
        }

        const children = this.state.menu.map((child, i) => {
            let subsubNav
            if (child.children) {
                subsubNav = <SubSubNav entity={child} />
            }

            return (
                <Draggable key={child.guid} draggableId={child.guid} index={i} isDragDisabled={!editMode}>
                    {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps}>
                            <div className={classnames({"subnav__parent":subsubNav})} {...provided.dragHandleProps}>
                                <NavLink activeClassName="___is-active" className={classnames({"___is-grabbable":editMode})} to={`/cms/view/${match.params.containerGuid || match.params.guid}/${match.params.containerSlug || match.params.slug}/${child.guid}`}>{child.title}</NavLink>
                            </div>
                            {subsubNav}
                        </div>
                    )}
                </Draggable>
            )
        })

        return (
            <div className="subnav">
                <NavLink exact to={entity.url} activeClassName="___is-active">{entity.title}</NavLink>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Droppable type="subnav" droppableId={entity.guid}>
                        {(provided, snapshot) => (
                            <div ref={provided.innerRef}>
                                {children}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>
        )
    }
}

const Query = gql`
    query SubNav($guid: Int!) {
        entity(guid: $guid) {
            guid
            ... on Page {
                canEdit
                title
                url
                children {
                    guid
                    title
                    canEdit
                    children {
                        guid
                        title
                    }
                }
            }
        }
    }
`

const Mutation = gql`
    mutation SubNav($input: reorderInput!) {
        reorder(input: $input) {
            container {
                guid
                ... on Page {
                    children {
                        guid
                        title
                    }
                }
            }
        }
    }
`

export default withGlobalState(graphql(Mutation)(graphql(Query, { options: { fetchPolicy: 'cache-and-network' } })(withRouter(SubNav))))