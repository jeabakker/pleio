import React from "react"
import { Link } from "react-router-dom"
import Truncate from 'react-truncate';

export default class Card extends React.Component {
    render() {
        const { url, title, description } = this.props.entity

        return (
            <div className="card">
                <div className="card__content">
                    <Link to={url} className="card-blog-post__title">
                        {title}
                    </Link>
                    {/* <div className="card-topic__meta">
                        <span>
                            Pagina
                        </span>
                    </div> */}
                    <p>
                        <Truncate
                            lines={1}
                            ellipsis={(
                                <span>...</span>
                            )}
                            trimWhitespace
                            >
                            {description}
                        </Truncate>
                    </p>
                </div>
            </div>
        )
    }
}