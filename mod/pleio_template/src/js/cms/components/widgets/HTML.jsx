import React from "react"
import Form from "../../../core/components/Form"
import TextField from "../../../core/components/TextField"
import { convertToRaw } from "draft-js"
import autobind from "autobind-decorator"
import { loadScript } from "../../../lib/helpers"
import { resolve } from "url";

export default class HTML extends React.Component {
    getSetting = (key, defaultValue) => {
        const { entity } = this.props

        let value = defaultValue || ""
        entity.settings.forEach((setting) => {
            if (setting.key === key) {
                value = setting.value
            }
        })

        return value
    }

    onSave = () => {
        const values = this.refs.form.getValues()

        this.props.onSave([
            { key: "description", value: values.description}
        ])
    }

    getHTML = () => {
        const { entity } = this.props

        return {
            __html: this.getSetting("description")
        }
    }

    componentDidMount() {
        const el = document.createElement("html")
        el.innerHTML = this.getSetting("description")

        let scripts = []
        let evals = []

        const scriptTags = el.getElementsByTagName("script")
        for (let i = 0; i < scriptTags.length; i++) {
            if (scriptTags[i].src) {
                scripts.push(loadScript(scriptTags[i].src))
            } else {
                evals.push(scriptTags[i].innerText)
            }
        }

        Promise.all(scripts).then(() => {
            evals.map((code) => {
                window.eval(code)
            })
        })
    }

    componentDidUpdate() {
        this.componentDidMount()
    }

    render() {
        const { entity, isEditing } = this.props

        if (isEditing) {
            return (
                <Form ref="form" className="form">
                    <TextField name="description" placeholder="Hier komt de code..." value={this.getSetting("description")} />
                </Form>
            )
        }

        return (
            <div className="cms-block-html" dangerouslySetInnerHTML={this.getHTML()} />
        )
    }
}