import React from "react"
import Form from "../../../core/components/Form"
import TagField from "../../../core/components/TagField"
import SwitchField from "../../../core/components/SwitchField"
import autobind from "autobind-decorator"
import ObjectsList from "./ObjectsList";
import InputField from "../../../core/components/InputField";

export default class Objects extends React.Component {
    constructor(props) {
        super(props)

        this.subtypes = [
            { type: "blog", label: "Blog"},
            { type: "news", label: "Nieuws"},
            { type: "discussion", label: "Discussie"},
            { type: "event", label: "Agenda"},
            { type: "group", label: "Groepen"},
            { type: "question", label: "Vragen"}
        ]
    }

    getSetting = (key, defaultValue) => {
		const { entity } = this.props

		let setting = entity.settings.find(setting => { return setting.key === key})

        if (setting) {
            return setting.value
        }

        return defaultValue || ""
    }

    onSave = () => {
        const values = this.refs.form.getValues()

        let selectedSubtypes = []

        this.subtypes.forEach(item => {
            if (values[item.type]) {
                selectedSubtypes.push(item.type)
            }
        })

        this.props.onSave([
            { key: "tags", value: values.tags.join(',') },
            { key: "subtypes", value: selectedSubtypes.join(',') },
            { key: "title", value: values.title },
            { key: "limit", value: values.limit },
            { key: "showDate", value: values.showDate }
        ])
    }

    hasSubtype = (subtype) => {
        const subtypes = this.getSetting("subtypes").split(',');
        return (subtypes.indexOf(subtype) > -1)
    }

    render() {
        const { entity, isEditing } = this.props

        const subtypeItems = this.subtypes.map((item, index) =>
            <div className="col-sm-6" key={index}>
                <SwitchField ref="switch" name={item.type} label={item.label} value={this.hasSubtype(item.type)} />
            </div>
        )

        if (isEditing) {
            return (
                <Form ref="form" className="form">
                    <InputField label="Titel" name="title" placeholder="Titel (laat leeg om titel te verbergen)" type="text" value={ this.getSetting("title")}  />
                    <br />
                    <div className="form__label">Filter op type</div>
                    <div className="row">
                        {subtypeItems}
                    </div>
                    <br />
                    <TagField label="Filter op steekwoorden (tags)" name="tags" type="text" selectedTags={ this.getSetting("tags") ? this.getSetting("tags").split(',') : [] } placeholder="Tags" helper="Voeg tags toe door op Enter te drukken." />
                    <br />
                    <InputField label="Aantal items" name="limit" placeholder="Limiet" type="number" value={this.getSetting("limit")}  />
                    <br />
                    <SwitchField label="Datum weergeven" name="showDate" value={ this.getSetting("showDate") == "true" ? true : false } />
                </Form>
            )
        }

        return (
            <ObjectsList
                title={this.getSetting("title")}
                subtypes={this.getSetting("subtypes") ? this.getSetting("subtypes").split(",") : []}
                tags={this.getSetting("tags") ? this.getSetting("tags").split(",") : []}
                limit={this.getSetting("limit") ? this.getSetting("limit") : 5}
                date={this.getSetting("showDate") == 'true' ? true: false}
            />
        )
    }
}
