import React from "react"
import { Link } from "react-router-dom"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import showDate from "../../../lib/showDate"
import Accordeon from "../../../core/components/Accordeon"

class ObjectsList extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { title, subtypes, date } = this.props
        const { entities } = this.props.data

        if (!entities) {
            return (
                <div></div>
            )
        }

        const items = entities.edges.map((entity, i) => {
            return (
                <Link key={i} to={entity.url} className="card__list-item">
                    <div className="flexer ___space-between ___nowrap">
                        <div>
                            <span className="___colored">{entity.title}</span>
                            {subtypes.length != 1 &&
                                <small>&nbsp;&nbsp;{entity.subtype}</small>
                            }
                        </div>
                        {date === true &&
                            <small>{showDate(entity.timeCreated)}</small>
                        }
                    </div>
                </Link>
            )
        })

        return (
            <Accordeon title={title}>
				{items}
            </Accordeon>
        )
    }
}

const Query = gql`
  query WidgetObjects($subtypes: [String!], $tags: [String!], $limit: Int!) {
      entities(subtypes: $subtypes, offset: 0, tags: $tags, limit: $limit) {
          edges {
              guid
              ... on Object {
                  title
                  timeCreated
                  url
                  subtype
              }
          }
      }
  }
`;

export default graphql(Query)(ObjectsList)
