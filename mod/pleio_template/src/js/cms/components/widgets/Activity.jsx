import React from "react"
import autobind from "autobind-decorator"
import ActivityList from "../../../activity/containers/ActivityList"
import Card from "../../../activity/components/Card"

export default class Activity extends React.Component {
    getSetting = (key, defaultValue) => {
        const { entity } = this.props

        let value = defaultValue || ""
        entity.settings.forEach((setting) => {
            if (setting.key === key) {
                value = setting.value
            }
        })

        return value
    }

    render() {
        return (
            <ActivityList containerClassName="activity-list" childClass={Card} offset={0} limit={20} />
        )
    }
}