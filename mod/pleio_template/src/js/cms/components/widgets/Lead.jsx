import React from "react"
import { Link } from "react-router-dom"
import autobind from "autobind-decorator"
import Form from "../../../core/components/Form"
import InputField from "../../../core/components/InputField"
import SelectField from "../../../core/components/SelectField"

export default class Lead extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            visible: true,
            height: "auto"
        }
    }

    getSetting = (key, defaultValue) => {
        const { entity } = this.props

        let value
        entity.settings.forEach(setting => {
            if (setting.key === key) {
                value = setting.value
            }
        })

        if (value) {
            return value
        }

        return defaultValue || ""
    }

    onClose = (e) => {
        this.setState({
            height: this.refs.lead.offsetHeight
        })

        setTimeout(() => {
            this.setState({
                visible: false
            })
        }, 10)
    }

    onSave = () => {
        const values = this.refs.form.getValues()

        this.props.onSave([
            { key: "image", value: values.image },
            { key: "title", value: values.title },
            { key: "link", value: values.link },
            { key: "poshorizontal", value: values.poshorizontal },
            { key: "posvertical", value: values.posvertical }
        ])
    }

    render() {
        const { entity, isEditing } = this.props

        let style = {
            height: this.state.height,
            width: "100%",
            marginLeft: 0,
            marginRight: 0,
            backgroundColor: '#009ee3',
            backgroundImage: `url(${this.getSetting("image")})`
        }

        if (!this.state.visible) {
            style.marginTop = 0
            style.opacity = 0
            style.height = 0
        }

        if (isEditing) {
            return (
                <Form ref="form" className="form">
                    <InputField
                        label="Achtergrond"
                        name="image"
                        placeholder="Link naar afbeelding"
                        value={this.getSetting("image")}
                        type="text"
                    />
                    <br />
                    <InputField
                        label="Tekst"
                        name="title"
                        placeholder="Tekst"
                        value={this.getSetting("title")}
                        type="text"
                    />
                    <br />
                    <div className="row">
                        <div className="col-sm-6">
                            <SelectField label="Tekst positie (horizontaal)" name="poshorizontal" className="form__input" options={['Links', 'Gecentreerd', 'Rechts']} value={this.getSetting("poshorizontal") ? this.getSetting("poshorizontal") : "0"} rules="required" />
                            <br />
                        </div>
                        <div className="col-sm-6">
                            <SelectField label="Tekst positie (verticaal)" name="posvertical" className="form__input" options={['Boven', 'Midden', 'Beneden']} value={this.getSetting("posvertical") ? this.getSetting("posvertical") : "2"} rules="required" />
                            <br />
                        </div>
                    </div>
                    <InputField
                        label="'Lees meer' knop"
                        name="link"
                        placeholder="Link (laat leeg om knop te verbergen)"
                        value={this.getSetting("link")}
                        type="text"
                    />
                </Form>
            )
        }

        let title
        if (this.getSetting("title")) {
            title = (
                <h1 className="lead__title">{this.getSetting("title")}</h1>
            )
        }

        let readMore
        if (this.getSetting("link")) {
            readMore = (
                <Link to={this.getSetting("link")} className="read-more">
                    <div className="read-more__circle" />
                    <span>Lees meer</span>
                </Link>
            )
        }

        let horizontalPosition
        if (this.getSetting("poshorizontal")) {
            const setting = this.getSetting("poshorizontal")

            switch (setting) {
                case '0':
                    horizontalPosition = "col-sm-6 start-sm"
                    break
                case '1':
                    horizontalPosition = "col-sm-8 center-sm col-sm-offset-2"
                    break
                case '2':
                    horizontalPosition = "col-sm-6 end-sm col-sm-offset-6"
                    break
            }
        }

        let verticalPosition
        if (this.getSetting("posvertical")) {
            const setting = this.getSetting("posvertical")

            switch (setting) {
                case '0':
                    verticalPosition = "top-sm"
                    break
                case '1':
                    verticalPosition = "middle-sm"
                    break
                case '2':
                    verticalPosition = "bottom-sm"
                    break
            }
        }

        return (
            <div style={style} className="lead" ref="lead">
                <div className="lead__justify container">
                    <div className="row">
                        <div className={`bottom-xs start-xs ${horizontalPosition} ${verticalPosition}`}>
                            <div>
                                {title}
                                {readMore}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
