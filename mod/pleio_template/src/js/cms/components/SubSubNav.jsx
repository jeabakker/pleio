import React from "react"
import { NavLink, withRouter } from "react-router-dom"
import { Droppable, Draggable } from "react-beautiful-dnd"
import classnames from "classnames"
import { withGlobalState } from 'react-globally'

class SubSubNav extends React.Component {
    render() {
        const editMode = this.props.globalState.editMode
        const { entity, match } = this.props

        return (
            <Droppable type={entity.guid} droppableId={entity.guid}>
                {(provided, snapshot) => (
                    <div ref={provided.innerRef} className="subnav__children">
                        {entity.children.map((child, i) => (
                            <Draggable key={child.guid} draggableId={child.guid} index={i} isDragDisabled={!editMode}>
                                {(provided, snapshot) => (
                                    <div {...provided.dragHandleProps} ref={provided.innerRef} {...provided.draggableProps}>
                                        <NavLink activeClassName="___is-active" className={classnames({"___is-grabbable":editMode})} to={`/cms/view/${match.params.containerGuid || match.params.guid}/${match.params.containerSlug || match.params.slug}/${child.guid}`}>{child.title}</NavLink>
                                    </div>
                                )}
                            </Draggable>
                        ))}
                    </div>
                )}
            </Droppable>
        )
    }
}

export default withGlobalState(withRouter(SubSubNav))