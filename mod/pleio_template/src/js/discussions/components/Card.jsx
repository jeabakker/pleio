import React from "react"
import { Link } from "react-router-dom"
import classnames from "classnames"
import Bookmark from "../../bookmarks/components/Bookmark"
import showDate from "../../lib/showDate"
import CardComments from "../../core/components/CardComments"

export default class Card extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            commentsVisible: false
        }
    }

    render() {
        const { entity, viewer, inActivityFeed } = this.props

        const commentCount = (
            <button
                className={classnames({
                    'count-comments button__text': true,
                    '___show-comments': this.state.commentsVisible
                })}
                onClick={() => this.setState({commentsVisible: !this.state.commentsVisible})}
            >
                {entity.commentCount} {(entity.commentCount === 1) ? " reactie" : " reacties"}
            </button>
        )

        const actions = (
            <div className="card-topic__actions">
                <Bookmark entity={entity} />
                {commentCount}
            </div>
        )

        const meta = (
            <div className="card-topic__meta">
                <div>
                    <span>
                        Discussie gestart door&nbsp;
                    </span>
                    <Link to={entity.owner.url} className="card-topic__user">
                        {entity.owner.name}
                    </Link>
                    <span> op {showDate(entity.timeCreated)}</span>
                    {entity.group &&
                        <span> in <Link to={entity.group.url}>{entity.group.name}</Link></span>
                    }
                </div>
            </div>
        )

        return (
            <div className="card-topic ___feed">
                <Link to={entity.owner.url} title={entity.owner.name} style={{ backgroundImage: "url(" + entity.owner.icon + ")" }} className="card-topic__picture"></Link>
                <div className="card-topic__post">
                    <Link to={entity.url} className="card-topic__title">
                        {entity.title}
                    </Link>
                    {meta}
                    <div className="card-topic__content">
                        {entity.excerpt}
                    </div>
                </div>
                {actions}
                {this.state.commentsVisible &&
                    <CardComments
                        entity={entity}
                        viewer={viewer}
                        inActivityFeed={inActivityFeed}
                    />
                }
            </div>
        )
    }
}

