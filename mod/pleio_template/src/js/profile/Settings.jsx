import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { Link } from "react-router-dom"
import AccountPassword from "./components/AccountPassword"
import AccountEmail from "./components/AccountEmail"
import Wrapper from "./components/Wrapper"
import autobind from "autobind-decorator"

class Settings extends React.Component {
    onClose = (e) => {
        const { history } = this.props
        history.push("/")
    }

    render() {
        const { location } = this.props
        const { viewer, entity } = this.props.data

        if (viewer && !viewer.loggedIn) {
            return (
                <Modal ref="modal" title="Inloggen" noParent={true} onClose={this.onClose}>
                    Je dient eerst in te loggen voordat je jouw instellingen kunt aanpassen.
                    <div className="buttons ___margin-top">
                        <Link to={{pathname: "/login", state: { next: location.pathname }}}>
                            <button title="Inloggen" className="button">
                                Inloggen
                            </button>
                        </Link>
                    </div>
                </Modal>
            )
        }

        if (!entity || !entity.canEdit) {
            return (
                <div />
            )
        }

        return (
            <Wrapper match={this.props.match}>
                <section className="section ___grey ___grow">
                    <div className="container">
                        <AccountPassword entity={entity} />
                        <AccountEmail entity={entity} />
                    </div>
                </section>
            </Wrapper>
        )
    }
}

const Query = gql`
    query ProfileAccount($username: String!) {
        entity(username: $username) {
            guid
            status
            ... on User {
                canEdit
                email
                tags
            }
        }
    }
`;

const withQuery = graphql(Query, {
    options: (ownProps) => {
        return {
            variables: {
                username: ownProps.match.params.username
            }
        }
    }
})

export default withQuery(Settings)