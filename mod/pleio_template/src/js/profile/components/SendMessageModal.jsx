import React from "react"
import { graphql } from "react-apollo"
import { logErrors } from "../../lib/helpers"
import gql from "graphql-tag"
import Modal from "../../core/components/NewModal"
import RichTextField from "../../core/components/RichTextField"
import Form from "../../core/components/Form"
import InputField from "../../core/components/InputField"
import Errors from "../../core/components/Errors"
import autobind from "autobind-decorator"
import { convertToRaw } from "draft-js"
import draftToHtml from "draftjs-to-html"

class SendMessageForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: [],
            working: false,
            completed: false
        }
    }

    onSubmit = (e) => {
        e.preventDefault()

        this.setState({
            working: true
        })

        const values = this.refs.form.getValues()

        let input = {
            clientMutationId: 1,
            guid: this.props.entity.guid,
            subject: values.subject,
            message: draftToHtml(convertToRaw(values.message)),
        }

        this.props.mutate({
            variables: {
                input
            }
        }).then(({data}) => {
            this.setState({
                working: false,
                completed: true
            })
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors
            })
        })
    }

    render () {
        const { entity } = this.props

        if (this.state.completed) {
            return (
                <p>Het bericht is succesvol verstuurd.</p>
            )
        }

        let loading
        if (this.state.working) {
            loading = (
                <div className="infinite-scroll__spinner">
                    <img src="/mod/pleio_template/src/images/spinner.svg" />
                </div>
            )
        }

        return (
            <Form ref="form" method="POST" onSubmit={this.onSubmit} >
                <Errors errors={this.state.errors} />
                <div className="form">
                    <InputField label="Onderwerp" name="subject" type="text" placeholder="Onderwerp" className="form__input" rules="required" autofocus />
                    <RichTextField ref="richText" name="message" placeholder="Bericht" rules="required" />
                </div>
                {loading}
                <div className="buttons ___margin-top">
                    <button className="button" type="submit" disabled={this.state.working}>Verstuur</button>
                </div>
            </Form>
        )
    }
}

const Mutation = gql`
    mutation SendMessageModal($input: sendMessageToUserInput!) {
        sendMessageToUser(input: $input) {
            success
        }
    }
`

const SendMessageFormWithMutation = graphql(Mutation)(SendMessageForm)

export default class SendMessageModal extends React.Component {
    toggle = () => {
        this.refs.modal.toggle()
    }

    render() {
        const { entity } = this.props;

        let title = `Stuur bericht aan ${entity.name}`

        return (
            <Modal ref="modal" title={title}>
                <p>Het bericht hieronder wordt per e-mail verstuurd aan {entity.name}. Bij het versturen van het bericht wordt jouw e-mailadres zichtbaar gemaakt zodat de ontvanger op het bericht kan reageren.</p>
                <SendMessageFormWithMutation entity={entity} />
            </Modal>
        )
    }
}
