import React from "react"
import { List } from "immutable"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import autobind from "autobind-decorator"

class DirectLinks extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            directLinks: List()
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.site || this.props.data === nextProps.data) {
            return
        }

        this.setState({
            directLinks: List(data.site.directLinks)
        })
    }

    addLink = (e) => {
        e.preventDefault()

        this.setState({
            directLinks: this.state.directLinks.push({
                title: "Nieuw",
                link: "/nieuw"
            })
        })
    }

    onChangeField = (i, fieldName, e) => {
        e.preventDefault()

        this.setState({
            directLinks: this.state.directLinks.set(i, Object.assign({}, this.state.directLinks[i], {
                [fieldName]: e.target.value
            }))
        })
    }

    removeLink = (i, e) => {
        e.preventDefault()

        this.setState({
            directLinks: this.state.directLinks.delete(i)
        })
    }

    onDragEnd = (result) => {
        if (!result.destination) {
            return
        }

        const sourceRemoved = this.state.directLinks.splice(result.source.index, 1)
        const newDirectLinks = sourceRemoved.splice(result.destination.index, 0, this.state.directLinks.get(result.source.index))

        this.setState({ menu: newDirectLinks })
    }

    render() {
        const directLinks = this.state.directLinks.map((link, i) => {
            return (
                <Draggable key={i} draggableId={i.toString()} index={i}>
                    {(provided, snapshot) => (
                        <div>
                            <div ref={provided.innerRef} {...provided.draggableProps}>
                                <span {...provided.dragHandleProps} className="elgg-icon elgg-icon-drag-arrow"></span>
                                <input type="text" name={`directLinksTitle[${i}]`} onChange={(e) => this.onChangeField(i, "title", e)} value={link.title} />
                                <input type="text" name={`directLinksLink[${i}]`} onChange={(e) => this.onChangeField(i, "link", e)} value={link.link} />
                                <span className="elgg-icon elgg-icon-delete" onClick={(e) => this.removeLink(i, e)} />
                            </div>
                            {provided.placeholder}
                        </div>
                    )}
                </Draggable>
            )
        })

        return (
            <div>
                <div>
                    <button className="elgg-button elgg-button-submit" onClick={this.addLink}>
                        Link toevoegen
                    </button>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId="droppable-1">
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef}>
                                {directLinks}
                                {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
            </div>
        )
    }
}

const Query = gql`
    query DirectLinks {
        site {
            guid
            directLinks {
                title
                link
            }
        }
    }
`

export default graphql(Query)(DirectLinks)