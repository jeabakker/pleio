import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import update from "immutability-helper"
import autobind from "autobind-decorator"

class Menu extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            menu: []
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.site || this.props.data === nextProps.data) {
            return
        }

        this.setState({ menu: data.site.menu })
    }

    addItem = (e, i) => {
        e.preventDefault()

        if (i==null) {
            this.setState({
                menu: update(this.state.menu, {
                    $push: [{
                        title: 'Nieuw item',
                        link: 'Nieuwe link',
                        children: []
                    }]
                })
            })
        } else {
            this.setState({
                menu: update(this.state.menu, {
                    [i]: {
                        children: {
                            $push: [{
                                title: 'Nieuw item',
                                link: 'Nieuwe link'
                            }]
                        }
                    }
                })
            })
        }
    }

    onChangeField = (e, i, j, fieldName) => {
        e.preventDefault()

        if (j==null) {
            this.setState({
                menu: update(this.state.menu, {
                    [i]: {
                        $merge: {
                            [fieldName]: e.target.value
                        }
                    }
                })
            })
        } else {
            this.setState({
                menu: update(this.state.menu, {
                    [i]: {
                        children: {
                            [j]: {
                                $merge: {
                                    [fieldName]: e.target.value
                                }
                            }
                        }
                    }
                })
            })
        }
    }

    removeItem = (i, j) => {
        if (j==null) {
            this.setState({
                menu: update(this.state.menu, {
                    $splice: [
                        [i, 1]
                    ]
                })
            })
        } else {
            this.setState({
                menu: update(this.state.menu, {
                    [i]: {
                        children: {
                            $splice: [
                                [j, 1]
                            ]
                        }
                    }
                })
            })
        }
    }

    onDragEnd = (result) => {
        if (!result.destination) {
            return
        }

        if (result.type == "SUBITEM") {
            const source = JSON.parse(result.source.droppableId)
            const destination = JSON.parse(result.destination.droppableId)
            if (source.i !== destination.i) {
                return
            }

            this.setState({
                menu: update(this.state.menu, {
                    [source.i]: {
                        children: {
                            $splice: [
                                [result.source.index, 1],
                                [result.destination.index, 0, this.state.menu[source.i].children[result.source.index]]
                            ]
                        }
                    }
                })
            })
        } else {
            this.setState({
                menu: update(this.state.menu, {
                    $splice: [
                        [result.source.index, 1],
                        [result.destination.index, 0, this.state.menu[result.source.index]]
                    ]
                })
            })
        }
    }

    render() {
        const menu = this.state.menu.map((item, i) => {
            const children = item.children.map((child, j) => (
                <Draggable key={`${i}-${j}`} draggableId={JSON.stringify({
                    type: 'draggable',
                    i,
                    j
                })} index={j} type="SUBITEM">
                    {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps}>
                            <span {...provided.dragHandleProps} className="elgg-icon elgg-icon-drag-arrow"></span>
                            <input type="text" name={`menu[${i}][children][${j}][title]`} onChange={(e) => this.onChangeField(e, i, j, "title")} value={child.title} />
                            <input type="text" name={`menu[${i}][children][${j}][link]`} onChange={(e) => this.onChangeField(e, i, j, "link")} value={child.link} />
                            <span className="elgg-icon elgg-icon-delete" onClick={() => this.removeItem(i, j)} />
                        </div>
                    )}
                </Draggable>
            ))

            return (
                <Draggable key={i} draggableId={JSON.stringify({
                        type: 'draggable',
                        i
                    })} index={i} type="ITEM">
                    {(provided, snapshot) => (
                        <div>
                            <div ref={provided.innerRef} {...provided.draggableProps}>
                                <span {...provided.dragHandleProps} className="elgg-icon elgg-icon-drag-arrow"></span>
                                <input type="text" name={`menu[${i}][title]`} onChange={(e) => this.onChangeField(e, i, null, "title")} value={item.title} />
                                {(item.children.length === 0) && (
                                    <input type="text" name={`menu[${i}][link]`} onChange={(e) => this.onChangeField(e, i, null, "link")} value={item.link} />
                                )}
                                <span className="elgg-icon elgg-icon-delete" onClick={() => this.removeItem(i)} />
                            </div>
                            <div style={{marginLeft: "60px"}}>
                                <Droppable droppableId={JSON.stringify({
                                    type: 'droppable',
                                    i
                                })} type="SUBITEM">
                                    {(provided, snapshot) => (
                                        <div ref={provided.innerRef}>
                                            {children}
                                            {provided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                                <button className="elgg-button" onClick={(e) => this.addItem(e, i)}>Subitem toevoegen</button>
                            </div>
                            {provided.placeholder}
                        </div>
                    )}
                </Draggable>
            )
        })

        return (
            <div>
                <div>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId={JSON.stringify({
                            type: 'droppable',
                            i: 'main'
                        })} type="ITEM">
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef}>
                                    {menu}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
                <div className="mtm">
                    <button className="elgg-button elgg-button-submit" onClick={(e) => this.addItem(e)}>
                        Item toevoegen
                    </button>
                </div>
            </div>
        )
    }
}

const Query = gql`
    query Menu {
        site {
            guid
            menu {
                title
                link
                children {
                    title
                    link
                }
            }
        }
    }
`

export default graphql(Query)(Menu)