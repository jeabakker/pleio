import React from "react"
import { List } from "immutable"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import autobind from "autobind-decorator"

class PredefinedTags extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            predefinedTags: List()
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.site || this.props.data === nextProps.data) {
            return
        }

        this.setState({
            predefinedTags: List(data.site.predefinedTags)
        })
    }

    addLink = (e) => {
        e.preventDefault()

        this.setState({
            predefinedTags: this.state.predefinedTags.push({
                tag: "Nieuw"
            })
        })
    }

    onChangeField = (i, fieldName, e) => {
        e.preventDefault()

        this.setState({
            predefinedTags: this.state.predefinedTags.set(i, Object.assign({}, this.state.predefinedTags[i], {
                [fieldName]: e.target.value
            }))
        })
    }

    removeLink = (i, e) => {
        e.preventDefault()

        this.setState({
            predefinedTags: this.state.predefinedTags.delete(i)
        })
    }

    onDragEnd = (result) => {
        if (!result.destination) {
            return
        }

        const sourceRemoved = this.state.tagCategories.splice(result.source.index, 1)
        const newPredefinedTags = sourceRemoved.splice(result.destination.index, 0, this.state.predefinedTags.get(result.source.index))

        this.setState({ predefinedTags: newPredefinedTags })
    }

    render() {
        const predefinedTags = this.state.predefinedTags.map((predefinedTag, i) => {
            return (
                <Draggable key={i} draggableId={i.toString()} index={i}>
                    {(provided, snapshot) => (
                        <div>
                            <div ref={provided.innerRef} {...provided.draggableProps}>
                                <span {...provided.dragHandleProps} className="elgg-icon elgg-icon-drag-arrow"></span>
                                <input type="text" name={`predefinedTags[${i}][tag]`} onChange={(e) => this.onChangeField(i, "tag", e)} value={predefinedTag.tag} />
                                <span className="elgg-icon elgg-icon-delete" onClick={(e) => this.removeLink(i, e)} />
                            </div>
                            {provided.placeholder}
                        </div>
                    )}
                </Draggable>
            )
        })

        return (
            <div>
                <div>
                    <button className="elgg-button elgg-button-submit" onClick={this.addLink}>
                        Voorgedefiniëerde tag toevoegen
                    </button>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Droppable droppableId="droppable-1">
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef}>
                                {predefinedTags}
                                {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </div>
            </div>
        )
    }
}

const Query = gql`
    query PredefinedTags {
        site {
            guid
            predefinedTags {
                tag
            }
        }
    }
`

export default graphql(Query)(PredefinedTags)