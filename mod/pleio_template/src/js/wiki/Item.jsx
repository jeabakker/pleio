import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import NotFound from "../core/NotFound"
import { Link, withRouter } from "react-router-dom"
import RichTextView from "../core/components/RichTextView"
import Document from "../core/components/Document"
import SubNav from "./components/SubNav"
import Modal from "../core/components/NewModal"
import AddCore from "../core/Add"

class Item extends React.Component {
    getRootURL() {
        const { match } = this.props

        if (match.params.groupGuid && match.params.groupSlug) {
            return `/groups/view/${match.params.groupGuid}/${match.params.groupSlug}`
        }

        return ""
    }

    render() {
        let { entity, viewer } = this.props.data
        const { match } = this.props

        if (!entity) {
            // Loading...
            return (
                <div></div>
            )
        }

        if (entity.status == 404) {
            return (
                <NotFound />
            )
        }

        let edit
        if (entity.canEdit) {
            edit = (
                <Link to={`${this.getRootURL()}/wiki/edit/${entity.guid}`}>
                    <div className="button__text article-action ___edit-post">
                        Bewerken
                    </div>
                </Link>
            )
        }

        let actions
        if (viewer.loggedIn) {
            actions = (
                <div className="article-actions__buttons">
                    <div className="article-actions__justify">
                        {edit}
                    </div>
                </div>
            )
        }

        let subNav
        if (entity.hasChildren || match.params.containerGuid) {
            subNav = (
                <div className="col-sm-4 col-lg-3">
                    <SubNav containerGuid={match.params.containerGuid || match.params.guid} guid={match.params.containerGuid || match.params.guid} />
                </div>
            )
        }

        let add
        if (viewer.canWriteToContainer) {
            add = (
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 end-sm">
                            <button className="button ___add ___large ___margin-mobile-top ___margin-bottom" onClick={(e) => this.refs.addModal.toggle()}>
                                Maak een subpagina
                            </button>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <Document title={entity.title} group={this.props.group} />
                <section className="section">
                    {add}
                </section>

                <section className="section ___grey ___grow">
                    <div className="container">
                        <div className="row">
                            {subNav}
                            <div className={entity.hasChildren || match.params.containerGuid ? "col-sm-8 col-lg-9" : "col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2"}>
                                <div className="card">
                                    <div className="card__content">
                                        <article className="article">
                                            <h3 className="article__title">{entity.title}</h3>
                                            <RichTextView richValue={entity.richDescription} value={entity.description} />
                                            <div className="article-actions">
                                                {actions}
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <Modal ref="addModal" full title="Subpagina toevoegen">
                    <AddCore subtype="wiki" afterAdd={() => location.reload()} containerGuid={match.params.guid || match.params.containerGuid} />
                </Modal>
            </React.Fragment>
        )
    }
}

const Query = gql`
    query WikiItem($guid: Int!) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
            canWriteToContainer(containerGuid: $guid, type: object, subtype: "wiki")
        }
        entity(guid: $guid) {
            guid
            status
            ... on Wiki {
                title
                description
                richDescription
                accessId
                timeCreated
                hasChildren
                canEdit
                tags
                url
                isBookmarked
                canBookmark
            }
        }
    }
`;

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.guid
            }
        }
    }
}

export default graphql(Query, Settings)(withRouter(Item))