import React from "react"
import { graphql } from "react-apollo"
import { Link } from "react-router-dom"
import gql from "graphql-tag"
import ContentHeader from "../core/components/ContentHeader"
import Document from "../core/components/Document"
import WikiList from "./containers/WikiList"
import Card from "../wiki/components/Card"

class List extends React.Component {
    render() {
        const { viewer } = this.props.data

        let add
        if (viewer && viewer.canWriteToContainer) {
            add = (
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 end-sm">
                            <Link to={`wiki/add`} className="button ___add ___large ___margin-mobile-top ___margin-bottom">
                                Maak een pagina
                            </Link>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <Document title="Wiki" />
                <ContentHeader>
                    <div className="row">
                        <div className="col-sm-6">
                            <h3 className="main__title">Wiki</h3>
                        </div>
                        <div className="col-sm-6 end-sm">
                            {add}
                        </div>
                    </div>
                </ContentHeader>
                <section className="section ___grey ___grow">
                    <WikiList type="object" subtype="wiki" containerGuid={1} childClass={Card} offset={0} limit={20} match={this.props.match} />
                </section>
            </React.Fragment>
        )
    }
}


const Query = gql`
    query WikiList {
        viewer {
            guid
            loggedIn
            canWriteToContainer(type: object, subtype: "wiki")
        }
    }
`

export default graphql(Query)(List)