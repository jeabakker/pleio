import React from "react"
import { Link } from "react-router-dom"
import { getClassFromTags } from "../../lib/helpers"
import classnames from "classnames"
import Bookmark from "../../bookmarks/components/Bookmark"
import showDate from "../../lib/showDate"

export default class Card extends React.Component {
    render() {
        const { entity, inActivityFeed } = this.props

        var meta

        if (inActivityFeed) {
            meta = (
                <div className="card-topic__meta">
                    <span>
                        Wiki aangemaakt
                    </span>
                    <span> op {showDate(entity.timeCreated)}</span>
                    {entity.group &&
                        <span> in <Link to={entity.group.url}>{entity.group.name}</Link></span>
                    }
                </div>
            )
        }

        return (
            <div className={inActivityFeed ? "card ___indent" : "card"}>
                <div className="card__content">
                    <div className="card__title">
                        <Link to={entity.url} className="card-blog-post__title">
                            {entity.title}
                        </Link>
                    </div>

                    {inActivityFeed &&
                        meta
                    }
                    <p className="card__excerpt">
                        {entity.excerpt}
                    </p>
                    <div className="card-topic__actions">
                        <Bookmark entity={entity} />
                    </div>
                </div>
            </div>
        )
    }
}