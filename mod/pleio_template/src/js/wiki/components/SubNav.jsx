import React from "react"
import { List } from "immutable"
import { NavLink, withRouter } from "react-router-dom"
import classnames from "classnames"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import autobind from "autobind-decorator"
import SubSubNav from "./SubSubNav"

class SubNav extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            menu: List()
        }
    }

    componentWillReceiveProps(nextProps) {
        const { data } = nextProps

        if (!data.entity) {
            return
        }

        const menu = data.entity.children.map((child) => {
            return {
                guid: child.guid,
                title: child.title,
                url: child.url,
                canEdit: child.canEdit,
                children: child.children.length ? List(child.children) : null
            }
        })

        this.setState({
            menu: List(menu)
        })
    }

    onDragEnd = (result) => {
        const { entity } = this.props.data

        if (!result.destination) {
            return
        }

        if (result.type === "subnav") {
            const sourceRemoved = this.state.menu.splice(result.source.index, 1)
            const newMenu = sourceRemoved.splice(result.destination.index, 0, this.state.menu.get(result.source.index))
            this.setState({ menu: newMenu })
        } else {
            const container = this.state.menu.find((object) => (object['guid'] === result.destination.droppableId))
            const containerIndex = this.state.menu.indexOf(container)

            const sourceRemoved = container.children.splice(result.source.index, 1)
            const newChildren = sourceRemoved.splice(result.destination.index, 0, container.children.get(result.source.index))

            const newContainer = {
                guid: container.guid,
                title: container.title,
                url: container.url,
                children: newChildren
            }

            const newMenu = this.state.menu.set(containerIndex, newContainer)
            this.setState({ menu: newMenu })
        }

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    guid: result.draggableId,
                    sourcePosition: result.source.index,
                    destinationPosition: result.destination.index
                }
            }
        })
    }

    render() {
        const { match } = this.props
        const { entity } = this.props.data

        if (!entity) {
            return (
                <div />
            )
        }

        const children = this.state.menu.map((child, i) => {
            let subsubNav

            if (child.children) {
                subsubNav = <SubSubNav entity={child} />
            }

            return (
                <Draggable key={child.guid} draggableId={child.guid} index={i} isDragDisabled={!entity.canEdit}>
                    {(provided, snapshot) => (
                        <div ref={provided.innerRef} {...provided.draggableProps}>
                            <div className={classnames({"subnav__parent":subsubNav})} {...provided.dragHandleProps}>
                                <NavLink activeClassName="___is-active" className={classnames({"___is-grabbable":entity.canEdit})} to={child.url}>
                                    {child.title}
                                </NavLink>
                            </div>
                            {subsubNav}
                        </div>
                    )}
                </Draggable>
            )
        })

        return (
            <div className="subnav">
                <NavLink exact activeClassName="___is-active" to={entity.url}>{entity.title}</NavLink>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Droppable type="subnav" droppableId={entity.guid}>
                        {(provided, snapshot) => (
                            <div ref={provided.innerRef}>
                                {children}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>
        )
    }
}

const Query = gql`
    query SubNav($guid: Int!) {
        entity(guid: $guid) {
            guid
            ... on Wiki {
                canEdit
                title
                url
                children {
                    guid
                    title
                    canEdit
                    url
                    children {
                        guid
                        title
                        url
                    }
                }
            }
        }
    }
`

const Mutation = gql`
    mutation SubNav($input: reorderInput!) {
        reorder(input: $input) {
            container {
                guid
                ... on Wiki {
                    children {
                        guid
                        title
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(graphql(Query)(withRouter(SubNav)))