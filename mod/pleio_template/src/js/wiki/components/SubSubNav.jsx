import React from "react"
import { NavLink, withRouter } from "react-router-dom"
import { Droppable, Draggable } from "react-beautiful-dnd"
import classnames from "classnames"

class SubSubNav extends React.Component {
    render() {
        const { entity, match } = this.props

        return (
            <Droppable type={entity.guid} droppableId={entity.guid}>
                {(provided, snapshot) => (
                    <div ref={provided.innerRef} className="subnav__children">
                        {entity.children.map((child, i) => (
                            <Draggable key={child.guid} draggableId={child.guid} index={i} isDragDisabled={!entity.canEdit}>
                                {(provided, snapshot) => (
                                    <div {...provided.dragHandleProps} ref={provided.innerRef} {...provided.draggableProps}>
                                        <NavLink activeClassName="___is-active" className={classnames({"___is-grabbable":entity.canEdit})} to={child.url}>{child.title}</NavLink>
                                    </div>
                                )}
                            </Draggable>
                        ))}
                    </div>
                )}
            </Droppable>
        )
    }
}

export default withRouter(SubSubNav)