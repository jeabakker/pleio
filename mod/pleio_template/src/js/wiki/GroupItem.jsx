import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import WikiItem from "../wiki/Item"
import GroupContainer from "../group/components/GroupContainer"

class GroupItem extends React.Component {
    render() {
        const { match } = this.props
        const { viewer, entity } = this.props.data

        if (!viewer) {
            // Loading...
            return (
                <div />
            )
        }

        return (
            <GroupContainer match={this.props.match} compactHeader>
                <WikiItem match={match} viewer={viewer} group={entity} />
            </GroupContainer>
        )
    }
}

const Query = gql`
    query GroupItem($guid: Int!) {
        viewer {
            guid
            loggedIn
            canWriteToContainer(containerGuid: $guid, type: object, subtype: "wiki")
            user {
                guid
                name
                icon
                url
            }
        }
        entity(guid: $guid) {
            guid
            status
            ... on Group {
                guid
                name
                description
                canEdit
                plugins
                icon
                isClosed
                membership
                members(limit: 5) {
                    total
                    edges {
                        role
                        email
                        user {
                            guid
                            username
                            url
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.groupGuid
            }
        }
    }
}

export default graphql(Query, Settings)(GroupItem)