import React from 'react'
import Errors from '../../core/components/Errors'

export default ({ errors }) => (
  <div className='attend-error'>
      <Errors errors={errors} />
  </div>
)
