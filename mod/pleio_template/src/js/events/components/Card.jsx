import React from "react"
import { Link } from "react-router-dom"
import autobind from "autobind-decorator"
import { getVideoFromUrl, getVideoThumbnail } from "../../lib/helpers"
import { showEventDateTime } from "../../lib/showDate"
import VideoModal from "../../core/components/VideoModal"
import People from "../../core/components/People"
import Bookmark from "../../bookmarks/components/Bookmark"

export default class Card extends React.Component {
    playVideo = (e) => {
        e.preventDefault()

        if (this.refs.video) {
            this.refs.video.onToggle()
        }
    }

    render() {
        const { entity } = this.props

        let style
        if (entity.featured.image) {
            style = { backgroundImage: `url('${entity.featured.image}')` }
        } else if (entity.featured.video) {
            style = { backgroundImage: `url('${getVideoThumbnail(entity.featured.video)}')` }
        }

        let videoModal
        if (entity.featured.video) {
            const video = getVideoFromUrl(entity.featured.video)
            videoModal = video && <VideoModal ref="video" id={video.id} type={video.type} url={entity.featured.video} />
        }

        let playButton
        if (entity.featured.video && videoModal) {
            playButton = (
                <div className="play-button ___small" onClick={this.playVideo} />
            )
        }

        let picture
        picture = (
            <Link to={entity.url} className="card-event__picture" title={entity.title} style={style}>
                {playButton}
            </Link>
        )

        const attendees = (
            <Link to={entity.url}>
                <People users={entity.attendees} />
            </Link>
        )

        const meta = (
            <div className="card-topic__meta">
                {showEventDateTime(entity.startDate, entity.endDate)}
                {entity.group &&
                    <span> in <Link to={entity.group.url}>{entity.group.name}</Link></span>
                }
            </div>
        )

        return (
            <div className="card-event">
                {picture}
                <div className="card-event__content">
                    <Link to={entity.url} className="title">{entity.title}</Link>
                    {meta}
                    <div className="card-event__bottom">
                        <Bookmark entity={entity} />
                        {attendees}
                    </div>
                </div>
                {videoModal}
            </div>
        )
    }
}
