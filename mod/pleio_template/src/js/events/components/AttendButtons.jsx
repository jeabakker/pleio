import React, { Fragment } from 'react'
import autobind from 'autobind-decorator'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import Select from '../../core/components/NewSelect'
import LoggedInButton from '../../core/components/LoggedInButton'
import AttendErrors from './AttendError'
import classnames from 'classnames'
import { debug } from 'util';

class AttendButtons extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: []
        }
    }

    onSubmit = (state) => {
        const { entity } = this.props

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    guid: entity.guid,
                    state
                }
            },
            refetchQueries: ['AttendeesList']
        }).then(() => {
            this.setState({ errors: [] })
        }).catch((errors) => {
            this.setState({ errors })
        })
    }

    render() {
        const { entity, viewer } = this.props
        const { attendees, maxAttendees, isAttending } = entity
        const isFull = attendees.total > 0 && attendees.total >= Number(maxAttendees) && maxAttendees != null && maxAttendees != ""
        const eventFullMessage = isFull ? '(vol)' : ''

        if (isAttending) {
            // Construct select
            const options = new Map()
            if (!isFull) options.set('accept', 'Aanwezig')
            options.set('maybe', 'Misschien')
            options.set('reject', 'Afwezig')

            const placeholder = (isFull) ? 'Aanwezig (vol)' : null

            return (
                <Fragment>
                    <Select
                        name="attending"
                        options={options}
                        placeholder={placeholder}
                        onChange={this.onSubmit}
                        value={isAttending}
                        className={classnames({
                            'attend-select': true,
                            '___margin-top': this.props.marginTop
                        })}
                    />
                    {this.state.errors && <AttendErrors errors={this.state.errors} />}
                </Fragment>
            )
        } else {
            return (
                <Fragment>
                    <div className="flexer ___gutter ___padding-top">
                        <LoggedInButton
                            className="button"
                            disabled={isFull}
                            viewer={viewer}
                            onClick={() => this.onSubmit('accept')}
                        >
                            Accepteren {eventFullMessage}
                        </LoggedInButton>
                        <LoggedInButton
                            className="button ___grey ___grey-hover"
                            disabled={isFull}
                            viewer={viewer}
                            onClick={() => this.onSubmit('maybe')}
                        >
                            Misschien
                        </LoggedInButton>
                        <LoggedInButton
                            className="button ___grey ___grey-hover"
                            disabled={isFull}
                            viewer={viewer}
                            onClick={() => this.onSubmit('reject')}
                        >
                            Afwezig
                        </LoggedInButton>
                    </div>
                    {this.state.errors && <AttendErrors errors={this.state.errors} />}
                </Fragment>
            )
        }
    }
}

const Mutation = gql`
    mutation AttendButtons($input: attendEventInput!) {
        attendEvent(input: $input) {
            entity {
                guid
                ... on Object {
                    isAttending
                    attendees(limit: 5) {
                        total
                        totalMaybe
                        totalReject
                        edges {
                            guid
                            username
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(AttendButtons)
