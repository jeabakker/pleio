import "../less/style.less"

import React from "react"
import ReactDOM from "react-dom"
import { AppContainer } from "react-hot-loader"
import App from "./App"
import { Provider } from 'react-globally'

const initialState = {
    editMode: false,
    savedItem: false
}

const render = (Component) => {
    ReactDOM.render(
        <Provider globalState={initialState}>
            <AppContainer>
                <Component />
            </AppContainer>
        </Provider>,
        document.getElementById("react-root")
    )
}

render(App)

if (module.hot) {
    module.hot.accept('./App', () => {
        render(App)
    })
}