import React from "react"
import { withRouter } from "react-router-dom"
import autobind from "autobind-decorator"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import showDate from "../lib/showDate"
import Document from "../core/components/Document"

class Item extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            zoomedIn: false
        }
    }

    zoom = () => {
        this.setState({
            zoomedIn: !this.state.zoomedIn
        })
    }

    goBack = () => {
        window.history.back()
    }

    getLink = () => {
        const { entity } = this.props.data

        if (entity.subtype == "file") {
            if (entity.mimeType == "application/vnd.oasis.opendocument.text") {
                return `/file/view/${entity.guid}`
            } else {
                return `/file/download/${entity.guid}/${entity.title}`
            }
        }
    }

    render() {
        const { entity, viewer } = this.props.data
        const { zoomedIn } = this.state

        if (!entity) {
            return (
                <div />
            )
        }

        let fileType
        if (entity.subtype === "folder") {
            fileType = "folder"
        } else if (entity.mimeType.indexOf("image/") !== -1) {
            fileType = "image"
        } else if (entity.mimeType.indexOf("application/pdf") !== -1) {
            fileType = "pdf"
        } else {
            fileType = "document"
        }

        let content
        if (fileType === "image") {
            const imageStyle = {
                transform: zoomedIn ? "scale(1.5)" : ""
            }

            content = (
                <img src={this.getLink()} alt="" className="file-view_image" style={imageStyle} />
            )
        } else {
            content = (
                <div className="file-view_thumbnail">
                    <img src={entity.thumbnail} alt="" />
                    <a href={this.getLink()} target="_blank" className="button ___line ___download">Download</a>
                </div>
            )
        }

        return (
            <React.Fragment>
                <Document title={entity.title} />
                <div className="page-layout ___no-nav">
                    <main id="skip-navigation" className="page-layout__main">
                        <div className="container-fluid file-view_header">
                            <button className="button__icon ___back file-view_back" onClick={() => this.goBack()}></button>
                            <div>
                                <h1 className="file-view_title">
                                {entity.title}
                                </h1>
                                <div className="file-view_date">
                                    Gewijzigd op {showDate(entity.timeUpdated)}
                                </div>
                            </div>
                            <a href={this.getLink()} target="_blank" className="button ___download" style={{marginLeft: "auto"}}>Download</a>
                        </div>
                        <div className="file-view">
                            {content}
                            {fileType === "image" &&
                                <div className="file-view_controls-wrapper">
                                    <div className="file-view_controls">
                                        <span className="file-view_zoom-level">
                                            {zoomedIn ? "150%" : "100%"}
                                        </span>
                                        <button className="button__icon ___zoom" onClick={(e) => this.zoom()}>{zoomedIn ? "–" : "+"}</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </main>
                </div>
            </React.Fragment>
        )
    }
}

const Query = gql`
    query FileItem($guid: Int!) {
        entity(guid: $guid) {
            guid
            ... on Object {
                subtype
                title
                timeUpdated
                mimeType
                thumbnail
            }
        }
    }
`;

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.guid
            }
        }
    }
}

export default graphql(Query, Settings)(withRouter(Item))