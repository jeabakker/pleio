import React from "react"
import { Route, Switch } from "react-router-dom"
import Item from "./Item"

export default class Routes extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/files/view/:guid/:slug" component={Item} />
            </Switch>
        )
    }
}
