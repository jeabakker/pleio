import React from "react"
import { Router as ReactRouter } from "react-router-dom"
import { Route, Switch } from "react-router-dom"
import browserHistory from "./lib/browserHistory"

import SiteContainer from "./core/components/SiteContainer"
import ScrollToTop from "./core/components/ScrollToTop"

import NewsRoutes from "./news/Routes"
import BlogRoutes from "./blog/Routes"
import QuestionsRoutes from "./questions/Routes"
import DiscussionsRoutes from "./discussions/Routes"
import EventsRoutes from "./events/Routes"
import FilesRoutes from "./files/Routes"
import GroupRoutes from "./group/Routes"
import CmsRoutes from "./cms/Routes"
import WikiRoutes from "./wiki/Routes"
import TasksRoutes from "./tasks/Routes"
import ProfileRoutes from "./profile/Routes"
import SearchRoutes from "./search/Routes"
import MembersRoutes from "./members/Routes"
import PollsRoutes from "./polls/Routes"
import CoreRoutes from "./core/Routes"
import NotFound from "./core/NotFound"

export default class Router extends React.Component {
    render() {
        return (
            <ReactRouter history={browserHistory}>
                <ScrollToTop>
                    <SiteContainer>
                        <Switch>
                            <Route path="/news" component={NewsRoutes} />
                            <Route path="/blog" component={BlogRoutes} />
                            <Route path="/questions" component={QuestionsRoutes} />
                            <Route path="/discussion" component={DiscussionsRoutes} />
                            <Route path="/events" component={EventsRoutes} />
                            <Route path="/files" component={FilesRoutes} />
                            <Route path="/groups" component={GroupRoutes} />
                            <Route path="/cms" component={CmsRoutes} />
                            <Route path="/wiki" component={WikiRoutes} />
                            <Route path="/tasks" component={TasksRoutes} />
                            <Route path="/polls" component={PollsRoutes} />
                            <Route path="/profile" component={ProfileRoutes} />
                            <Route path="/search" component={SearchRoutes} />
                            <Route path="/members" component={MembersRoutes} />
                            <Route path="/" component={CoreRoutes} />
                            <Route component={NotFound} />
                        </Switch>
                    </SiteContainer>
                </ScrollToTop>
            </ReactRouter>
        )
    }
}