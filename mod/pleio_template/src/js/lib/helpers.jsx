import { Set } from "immutable"

export function getQueryVariable(variable, search) {
    if (!search) {
        var query = window.location.search.substring(1);
    } else {
        var query = search.substring(1);
    }

    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

export function logErrors(errors) {
    if (typeof Raven !== "undefined") {
        Raven.captureException(errors)
    }
}

export function getClassFromTags(inputTags) {
    const translate = {
        "In de klas": "klas",
        "Wetten en regels": "wetten",
        "Arbeidsvoorwaarden": "arbeidsvoorwaarden",
        "Blijven leren": "leren",
        "Actualiteit": "actualiteit",
        "Vernieuwing": "vernieuwing",
        "Handreikingen": "vernieuwing",
        "Jurisprudentie": "wetten",
        "Brochures": "wetten",
        "Forum": "leren",
        "Nieuwsberichten": "nieuws",
        "Blogs": "nieuws",
        "Overig": "overig"
    };

    let cssTag
    inputTags.forEach((tag) => {
        if (translate[tag]) {
            cssTag = translate[tag]
        }
    })

    if (cssTag) {
        return "___" + cssTag
    } else {
        return "___" + "overig"
    }
}

export function getFirstValueFromSet(inputTags, possibleOptions) {
    if (!inputTags || !possibleOptions) {
        return
    }

    let returnValue

    const inputTagsSet = new Set(inputTags)

    possibleOptions.forEach((option) => {
        if (inputTagsSet.contains(option)) {
            if (!returnValue) {
                returnValue = option
            }
        }
    })

    return returnValue
}

export function getValuesFromTags(inputTags, possibleOptions) {
    if (!inputTags) {
        return []
    }

    const options = new Set(possibleOptions)

    let value = []
    inputTags.forEach((tag) => {
        if (options.contains(tag)) {
            value.push(tag)
        }
    })

    return value
}

export function parseURL(url) {
    var parser = document.createElement("a");
    parser.href = url;
    return parser;
}

export function displayTags(tags) {
    return tags.join(", ")
}

export function isMobile() {
    let userAgent = (window.navigator.userAgent || window.navigator.vendor || window.opera),
        isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(userAgent);

    return isMobile ? true : false;
}

export function getAttribute(name, object, defaultValue) {
    if (typeof object[name] !== "undefined") {
        return object[name]
    }

    return defaultValue
}

export function arrayToObject(array) {
    let returnValue = {}

    array.forEach((item) => {
        returnValue[item] = item
    })

    return returnValue
}

export function getVideoFromUrl(input) {
    const url = parseURL(input)
    switch (url.hostname) {
        case "youtube.com":
        case "youtu.be":
        case "www.youtube.com":
            return { "type": "youtube", "id": getQueryVariable("v", url.search) }
        case "vimeo.com":
        case "www.vimeo.com":
            return { "type": "vimeo", "id": getQueryVariable("v", url.search) }
        default:
            return undefined
    }
}

export function getVideoThumbnail(input) {
    const url = parseURL(input)

    switch (url.hostname) {
        case "youtube.com":
        case "youtu.be":
        case "www.youtube.com":
            let v = getQueryVariable("v", url.search)
            return `https://img.youtube.com/vi/${v}/hqdefault.jpg`
    }

    return ""
}

export function loadScript(src) {
    return new Promise(function (resolve, reject) {
        let s
        s = document.createElement('script')
        s.src = src
        s.onload = resolve
        s.onerror = reject
        document.head.appendChild(s)
    })
}

export function humanFileSize(size) {
    const i = Math.floor(Math.log(size) / Math.log(1024))
    return (size / Math.pow(1024, i)).toFixed(1) * 1 + ['B', 'kB', 'MB', 'GB', 'TB'][i]
}

let i = 0
export function generateUniqueId(prefix) {
    i += 1
    return `${prefix}-${i}`
}
