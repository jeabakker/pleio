import React, { Fragment } from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import autobind from "autobind-decorator"
import classnames from "classnames"
import Accordeon from "../../core/components/Accordeon"
import Form from "../../core/components/Form"
import Errors from "../../core/components/Errors"
import RadioField from "../../core/components/RadioField"

class Poll extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            showResults: false,
            errors: []
        }
    }

    onSubmit = (guid) => {
        const values = this.refs.form.getValues()

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: "1",
                    guid: guid,
                    response: values.response
                }
            }
        }).then((data) => {
            this.setState({ showResults: true })
        }).catch((errors) => {
            this.setState({ errors })
        })
    }

    toggleResults = (e) => {
        e.preventDefault()
        this.setState({ showResults: !this.state.showResults })
    }

    render() {
        const { viewer, entities } = this.props.data

        if (!entities || entities.edges.length === 0) {
            return (
                <div />
            )
        }

        const poll = entities.edges[0]

        let votes = 0
        let maxVotes = 0
        poll.choices.map((choice) => {
            votes += choice.votes

            if (choice.votes > maxVotes) {
                maxVotes = choice.votes
            }
        })

        let content
        if (this.state.showResults || poll.hasVoted || !viewer.loggedIn) {
            const options = poll.choices.map((choice) => (
                <div key={choice.guid} className={classnames({"poll__result": true, "___most-votes": choice.votes === maxVotes})}>
                    <span>{Math.round(choice.votes / votes * 100) || 0}%</span>
                    <label>
                        <div style={{width: `${Math.round(choice.votes / votes * 100) || 0}%`}} className="poll__result-bar" />
                        <span>{choice.text}</span>
                    </label>
                </div>
            ))

            let backButton
            if (!poll.hasVoted && viewer.loggedIn) {
                backButton = (
                    <div className="flexer ___gutter">
                        <button className="button ___back" onClick={this.toggleResults}>Terug</button>
                    </div>
                )
            }

            content = (
                <React.Fragment>
                    <div className="card__subtitle">{poll.title}</div>
                    {options}
                    {backButton}
                    <label className="___colored">{votes} {votes === 1 ? "stem" : "stemmen"}</label>
                </React.Fragment>
            )
        } else {
            const options = poll.choices.map((choice) => {
                return {
                    name: choice.text,
                    value: choice.text
                }
            })

            content = (
                <Fragment>
                    <div className="card__subtitle">{poll.title}</div>
                    <Form ref="form" onSubmit={() => this.onSubmit(poll.guid)}>
                        <Errors errors={this.state.errors} />
                        <RadioField name="response" options={options} />
                        <div className="flexer ___gutter">
                            <button className="button" type="submit">Stem</button>
                            <button className="button ___line" onClick={this.toggleResults}>Tussenstand</button>
                        </div>
                        <label className="___colored">
                            {votes} {votes === 1 ? "stem" : "stemmen"}
                        </label>
                    </Form>
                </Fragment>
            )
        }

        return (
            <div className="col-sm-6 col-lg-12">
                <Accordeon title="Poll">
                    {content}
                </Accordeon>
            </div>
        )
    }
}

const Query = gql`
    query Poll {
        viewer {
            guid
            loggedIn
        }
        entities(subtype:"poll", limit:1) {
            total
            edges {
                guid
                ... on Poll {
                    title
                    url
                    hasVoted
                    choices {
                        guid
                        text
                        votes
                    }
                }
            }
        }
    }
`

const Mutation = gql`
    mutation Poll($input: voteOnPollInput!) {
        voteOnPoll(input: $input) {
            entity {
                guid
                ... on Poll {
                    hasVoted
                    choices {
                        guid
                        text
                        votes
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(graphql(Query)(Poll))