import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import Accordeon from "../../core/components/Accordeon"

class DirectLinks extends React.Component {
    render() {
        const { site } = this.props.data

        if (!site || !site.directLinks || site.directLinks.length === 0) {
            return (
                <div />
            )
        }

        const items = site.directLinks.map((item, i) => (
            <a key={i} href={item.link} title={item.title} className="goto">
                {item.title}
            </a>
        ))

        return (
            <div className="col-sm-6 col-lg-12">
                <Accordeon title="Direct naar">
                    {items}
                </Accordeon>
            </div>
        )
    }
}

const Query = gql`
    query DirectLinks {
        site {
            guid
            directLinks {
                title
                link
            }
        }
    }
`

export default graphql(Query)(DirectLinks)