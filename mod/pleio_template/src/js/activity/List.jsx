import React from "react"
import classnames from "classnames"
import ActivityList from "./containers/ActivityList"
import Lead from "./components/Lead"
import ContentFilters from "../core/containers/ContentFilters"
import UsersOnline from "../core/containers/UsersOnline"
import Card from "./components/Card"
import Poll from "./components/Poll"
import DirectLinks from "./components/DirectLinks"
import Recommended from "./components/Recommended"
import Trending from "./components/Trending"
import Initiative from "./components/Initiative"
import Footer from "./components/Footer"
import Document from "../core/components/Document"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import TagField from "../core/components/TagField"
import autobind from "autobind-decorator";

class Activity extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            tags: []
        }
    }

    onChangeFilter = (tags) => {
        const newTags = tags.slice(0) // clone array
        this.setState({ tags: newTags })
    }

    componentWillReceiveProps(nextProps) {
        if (window.__SETTINGS__['hasPredefinedTags']) {
            return
        }

        const { data } = nextProps

        if (data.viewer && data.viewer.loggedIn && this.state.tags.length === 0) {
            this.setState({ tags: ["mine"] })
        }
    }

    render() {
        const { site, viewer } = this.props.data

        if (!viewer) {
            return (
                <div />
            )
        }

        let leader, initiative
        if (site && site.showLeader) {
            leader = (
                <Lead />
            )
        }

        if (site) {
            initiative = (
                <Initiative site={site} />
            )
        }

        return (
            <React.Fragment>
            <section className={classnames({
                "section":true,
                "___less-padding-top": true,
                "___no-padding-bottom": true
            })}>
                <Document title="Activiteiten" />
                <div className="container">
                    {leader}
                    <div className="row">
                        {!window.__SETTINGS__['hasPredefinedTags'] ?
                            <ContentFilters onChange={this.onChangeFilter} onActivity />
                        :
                            <div className="col-sm-6 middle-sm">
                                <TagField placeholder="Filter.." icon onChange={this.onChangeFilter} />
                            </div>
                        }
                        <div className="col-right middle-lg">
                            <UsersOnline isGrey={true} />
                        </div>
                    </div>
                </div>
            </section>
            <section className="section ___grey">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-lg-4 last-lg top-lg">
                            <div className="row">
                                <Poll />
                                <DirectLinks />
                                <Recommended />
                                <Trending />
                                {initiative}
                                <Footer />
                            </div>
                        </div>
                        <ActivityList containerClassName="col-sm-12 col-lg-8" childClass={Card} tags={this.state.tags} offset={0} limit={20} />
                    </div>
                </div>
            </section>
            </React.Fragment>
        )
    }
}

const Query = gql`
    query ActivityList {
        viewer {
            guid
            loggedIn
        }
        site {
            guid
            showLeader
            showInitiative
            initiativeImage
            logo
            initiatorLink
        }
    }
`
export default graphql(Query)(Activity)
