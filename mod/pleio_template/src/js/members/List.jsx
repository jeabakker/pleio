import React from "react"
import autobind from "autobind-decorator"
import Document from "../core/components/Document"
import UserList from "./containers/UserList"
import Card from "./components/Card"

export default class List extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            q: "",
            search: ""
        }
    }


    onChange = (e) => {
        const q = e.target.value

        this.setState({ q })

        if (this.changeTimeout) {
            clearTimeout(this.changeTimeout)
        }

        this.changeTimeout = setTimeout(() => {
            this.setState({ search: q })
        }, 100)
    }

    render() {
        return (
            <React.Fragment>
                <Document title="Leden" />
                <section className="section ___grey ___grow">
                    <div className="container">
                        <div className="card">
                            <div className="card__content">
                                <div className="search-bar ___margin-bottom">
                                    <input type="text" name="q" onChange={this.onChange} placeholder="Zoek gebruikers..." autoComplete="off" value={this.state.q} />
                                    <div className="search-bar__button"></div>
                                </div>
                                <UserList containerClassName="list-members ___scrollable" childClass={Card} q={this.state.search} offset={0} limit={40} />
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}
