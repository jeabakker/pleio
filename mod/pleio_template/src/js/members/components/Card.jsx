import React from "react"
import UserCard from "../../core/components/UserCard"

export default class Card extends React.Component {
    render() {
        const { entity, viewer } = this.props

        if (!entity) {
            return (
                <div />
            )
        }

        return (
          <UserCard
              key={entity.guid}
              entity={entity}
          />
      )
    }
}
