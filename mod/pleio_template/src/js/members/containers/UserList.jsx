import React from "react"
import InfiniteList from "../../core/components/InfiniteList"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

const Query = gql`
    query InfiniteList($offset: Int!, $limit: Int!, $q: String!) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
        }
        users(offset: $offset, limit: $limit, q: $q) {
            edges {
                guid
                name
                url
                icon
            }
            total
        }
    }
`

export default graphql(Query)(InfiniteList)
