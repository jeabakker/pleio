import React from "react"
import { Route, Switch } from "react-router-dom"
import Container from "../core/components/Container"

import List from "./List"

import NotFound from "../core/NotFound"

export default class Routes extends React.Component {
    render() {
        return (
            <Container>
                <Switch>
                    <Route exact path="/members" component={List} />
                </Switch>
            </Container>
        )
    }
}