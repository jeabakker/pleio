import React from "react"
import { Link } from "react-router-dom"
import { graphql, gql } from "react-apollo"
import ContentHeader from "../core/components/ContentHeader"
import PollsList from "./containers/PollsList"
import Card from "./components/Card"
import Document from "../core/components/Document"

class List extends React.Component {
    constructor(props) {
        super(props)

        this.onChangeCanWrite = (canWrite) => this.setState({canWrite})
        this.onChangeFilter = (tags) => this.setState({ tags })

        this.state = {
            tags: []
        }
    }

    render() {
        const { viewer } = this.props.data

        let add
        if (viewer && viewer.canWriteToContainer) {
            add = (
                <Link to={`polls/add`} className="button ___large ___add ___stick">
                    <span>Poll toevoegen</span>
                </Link>
            )
        }

        return (
            <div>
                <Document title="Polls" />
                <ContentHeader>
                    <div className="row">
                        <div className="col-sm-6">
                            <h3 className="main__title">Polls</h3>
                        </div>
                        <div className="col-sm-6 end-sm">
                            {add}
                        </div>
                    </div>
                </ContentHeader>
                <section className="section ___grey ___grow">
                    <PollsList childClass={Card} subtype="poll" offset={0} limit={20} tags={this.state.tags} hasRows />
                </section>
            </div>
        )
    }
}

const Query = gql`
    query PollsList {
        viewer {
            guid
            loggedIn
            canWriteToContainer(type: object, subtype: "poll")
        }
    }
`

export default graphql(Query)(List)