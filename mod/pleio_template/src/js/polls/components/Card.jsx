import React from "react"
import { Link } from "react-router-dom"

export default class Card extends React.Component {
    render() {
        const { entity } = this.props

        return (
            <div className="card-blog-post">
                <div className="card-blog-post__post">
                    <Link to={entity.url} className="card-blog-post__title">
                        {entity.title}
                    </Link>
                    <div className="card-topic__meta">
                        <span>
                            Poll&nbsp;
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}