import React from "react"
import autobind from "autobind-decorator"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { logErrors } from "../lib/helpers"
import ActionContainer from "../core/components/ActionContainer"
import NotFound from "../core/NotFound"
import DeleteCore from "../core/Delete"
import Form from "../core/components/Form"
import Errors from "../core/components/Errors"
import InputField from "../core/components/InputField"
import MultipleInputField from "../core/components/MultipleInputField"
import AccessField from "../core/components/AccessField"

class Edit extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: []
        }
    }

    onSubmit = () => {
        this.setState({
            errors: []
        })

        const { entity } = this.props.data

        const values = this.refs.form.getValues()

        let input = {
            clientMutationId: 1,
            guid: entity.guid,
            title: values.title,
            choices: values.choices,
            accessId: values.accessId
        }

        this.props.mutate({
            variables: {
                input
            },
            refetchQueries: this.props.refetchQueries
        }).then(({data}) => {
            window.location.href = `/polls/view/${entity.guid}/${values.title}`
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors
            })
        })

    }

    onDelete = (e) => {
        e.preventDefault()
        this.refs.deleteModal.toggle()
    }

    afterDelete = () => {
        window.location.href = '/polls'
    }

    onClose = () => {
        const { entity } = this.props.data

        this.props.history.push(`/polls/view/${entity.guid}/${entity.title}`)
    }

    render() {
        const { entity } = this.props.data

        if (!entity) {
            return (
                <div />
            )
        }

        if (entity.status == 404) {
            return (
                <NotFound />
            )
        }

        return (
            <ActionContainer title="Poll bewerken" onClose={this.onClose}>
                <DeleteCore title="Poll verwijderen" ref="deleteModal" entity={entity} afterDelete={this.afterDelete} />
                <Form ref="form" onSubmit={this.onSubmit}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                                <div className="form">
                                    <Errors errors={this.state.errors} />
                                    <InputField defaultSize={2} name="title" type="text" placeholder="Titel" className="form__input" rules="required" autofocus value={entity.title} />
                                    <div className="form__label">Antwoordopties</div>
                                    <MultipleInputField defaultSize={2} name="choices" type="text" placeholder="Optie" className="form__input" rules="required" value={entity.choices.map((choice) => choice.text)} />
                                    <AccessField name="accessId" label="Leesrechten" />
                                    <div className="buttons ___space-between">
                                        <button className="button" type="submit">
                                            Opslaan
                                        </button>
                                        <button className="button ___link" onClick={this.onDelete}>
                                            Verwijderen
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            </ActionContainer>
        )
    }
}

const Query = gql`
    query EditPoll($guid: Int!) {
        entity(guid: $guid) {
            guid
            status
            ... on Poll {
                title
                url
                hasVoted
                choices {
                    guid
                    text
                    votes
                }
            }
        }
    }
`

const Mutation = gql`
    mutation EditPoll($input: editPollInput!) {
        editPoll(input: $input) {
            entity {
                guid
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.guid
            }
        }
    }
}

export default graphql(Query, Settings)(graphql(Mutation)(Edit))