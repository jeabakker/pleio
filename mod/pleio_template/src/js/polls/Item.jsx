import React from "react"
import { graphql } from "react-apollo"
import { Link } from "react-router-dom"
import gql from "graphql-tag"
import NotFound from "../core/NotFound"
import showDate from "../lib/showDate"
import Document from "../core/components/Document"
import classnames from "classnames"

class Item extends React.Component {
    render() {
        let { entity, viewer } = this.props.data
        let edit, featured, source

        if (!entity) {
            // Loading...
            return (
                <div></div>
            )
        }

        if (entity.status == 404) {
            return (
                <NotFound />
            )
        }

        if (entity.canEdit) {
            edit = (
                    <Link to={`/polls/edit/${entity.guid}`}>
                        <div className="button__text article-action ___edit-post">
                            Bewerken
                        </div>
                    </Link>
            )
        }

        let votes = 0
        let maxVotes = 0
        entity.choices.map((choice) => {
            votes += choice.votes

            if (choice.votes > maxVotes) {
                maxVotes = choice.votes
            }
        })

        const options = entity.choices.map((choice) => (
            <div key={choice.guid} className={classnames({"poll__result": true, "___most-votes": choice.votes === maxVotes})}>
                <span>{Math.round(choice.votes / votes * 100) || 0}%</span>
                <label>
                    <div style={{width: `${Math.round(choice.votes / votes * 100) || 0}%`}} className="poll__result-bar" />
                    <span>{choice.text}</span>
                </label>
            </div>
        ))

        return (
            <div>
                <Document title={entity.title} />
                <section className="section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                                <article className="article">
                                    <h3 className="article__title">{entity.title}</h3>
                                    <div className="article-meta">
                                        <div className="article-meta__date">
                                            {showDate(entity.timeCreated)}
                                        </div>
                                    </div>
                                    {options}
                                    <label className="___colored">{votes} {votes === 1 ? "stem" : "stemmen"}</label>
                                    <div className="article-actions">
                                        {edit}
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const Query = gql`
    query PollsItem($guid: Int!) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
        }
        entity(guid: $guid) {
            guid
            status
            ... on Poll {
                title
                timeCreated
                canEdit
                choices {
                    guid
                    text
                    votes
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.match.params.guid
            }
        }
    }
}

export default graphql(Query, Settings)(Item)