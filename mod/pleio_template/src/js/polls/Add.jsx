import React from "react"
import autobind from "autobind-decorator"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { logErrors } from "../lib/helpers"
import ActionContainer from "../core/components/ActionContainer"
import Form from "../core/components/Form"
import Errors from "../core/components/Errors"
import InputField from "../core/components/InputField"
import MultipleInputField from "../core/components/MultipleInputField"
import AccessField from "../core/components/AccessField"

class Add extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            errors: [],
        }
    }

    onSubmit = () => {
        this.setState({
            errors: []
        })

        const values = this.refs.form.getValues()

        let input = {
            clientMutationId: 1,
            title: values.title,
            choices: values.choices,
            accessId: values.accessId
        }

        this.props.mutate({
            variables: {
                input
            },
            refetchQueries: this.props.refetchQueries
        }).then(({data}) => {
            window.location.href = "/polls"
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors
            })
        })

    }

    onClose = () => {
        this.props.history.push("/polls")
    }

    render() {
        return (
            <ActionContainer title="Poll toevoegen" onClose={this.onClose}>
                <Form ref="form" onSubmit={this.onSubmit}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                                <div className="form">
                                    <Errors errors={this.state.errors} />
                                    <InputField defaultSize={2} name="title" type="text" placeholder="Titel" className="form__input" rules="required" autofocus />
                                    <div className="form__label">Antwoordopties</div>
                                    <MultipleInputField defaultSize={2} name="choices" type="text" placeholder="Optie" className="form__input" rules="required" />
                                    <AccessField name="accessId" label="Leesrechten" />
                                    <div className="buttons ___end ___margin-top">
                                        <button className="button" type="submit">
                                            Toevoegen
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            </ActionContainer>
        )
    }
}


const Mutation = gql`
    mutation AddPoll($input: addPollInput!) {
        addPoll(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default graphql(Mutation)(Add)