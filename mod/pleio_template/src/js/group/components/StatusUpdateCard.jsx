import React from "react"
import { Link } from "react-router-dom"
import { logErrors } from "../../lib/helpers"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { convertToRaw } from "draft-js"
import classnames from "classnames"
import showDate from "../../lib/showDate"
import Form from "../../core/components/Form"
import DeleteModal from "../../core/Delete"
import Bookmark from "../../bookmarks/components/Bookmark"
import Errors from "../../core/components/Errors"
import RichTextView from "../../core/components/RichTextView"
import RichTextField from "../../core/components/RichTextField"
import autobind from "autobind-decorator"
import CardComments from "../../core/components/CardComments"
import ShowMore from "../../core/components/ShowMore"

class StatusUpdateCard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isEditing: false,
            commentsVisible: false,
            errors: []
        }
    }

    toggleEdit = (e) => {
        this.setState({ isEditing: !this.state.isEditing })
    }

    onEdit = (e) => {
        const { entity } = this.props

        this.setState({
            errors: []
        })

        let values = this.refs.form.getValues()

        this.props.mutate({
            variables: {
                input: {
                    clientMutationId: 1,
                    guid: entity.guid,
                    description: values.description.getPlainText(),
                    richDescription: JSON.stringify(convertToRaw(values.description))
                }
            }
        }).then(({ data }) => {
            this.setState({
                isEditing: false
            })
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors
            })
        })
    }

    onDelete = (e) => {
        e.preventDefault()
        this.refs.deleteModal.toggle()
    }

    afterDelete() {
        location.reload()
    }

    render() {
        const { entity, viewer } = this.props
        const { isEditing } = this.state
        const { owner } = entity

        let ownerLink
        if (entity.group) {
            ownerLink = (
                <span>
                    <Link to={owner.url} className="card-blog-post__user">
                        {owner.name}
                    </Link>
                    &nbsp;in <Link to={entity.group.url}>{entity.group.name}</Link>
                </span>
            )
        } else {
            ownerLink = (
                <span>
                    <Link to={owner.url} className="card-blog-post__user">
                        {owner.name}
                    </Link>
                </span>
            )
        }

        const meta = (
            <div className="card-topic__meta">
                <div>
                    <span>
                        Update door&nbsp;
                    </span>
                    <Link to={entity.owner.url} className="card-topic__user">
                        {entity.owner.name}
                    </Link>
                    <span> op {showDate(entity.timeCreated)}</span>
                    {entity.group &&
                        <span> in <Link to={entity.group.url}>{entity.group.name}</Link></span>
                    }
                </div>
            </div>
        )

        const commentCount = (
            <button
                className={classnames({
                    'count-comments button__text': true,
                    '___show-comments': this.state.commentsVisible
                })}
                onClick={() => this.setState({commentsVisible: !this.state.commentsVisible})}
            >
                {entity.commentCount} {(entity.commentCount === 1) ? " reactie" : " reacties"}
            </button>
        )

        const actions = (
            <div className="card-topic__actions">
                <Bookmark entity={entity} />
                {commentCount}
            </div>
        )

    let content, editButton
        if (isEditing) {
            content = (
                <div className="form">
                    <Errors errors={this.state.errors} />
                    <Form ref="form" onSubmit={this.onEdit}>
                        <RichTextField name="description" richValue={entity.richDescription} value={entity.description} minimal />
                        <div className="flexer ___end ___gutter">
                            <button className="button" type="submit">Opslaan</button>
                            <button className="button ___link" onClick={this.onDelete}>Verwijderen</button>
                        </div>
                    </Form>
                    <DeleteModal ref="deleteModal" entity={entity} afterDelete={this.afterDelete} />
                </div>
            )
        } else {
            editButton = (
                <div className="button ___small ___line card__edit" onClick={(e) => this.toggleEdit()}>Bewerk</div>
            )
            content = (
                <ShowMore lines={10}>
                    <RichTextView richValue={entity.richDescription} value={entity.description} />
                </ShowMore>
            )
        }

        return (
            <div className={classnames({"card-blog-post": true, "___can-edit": entity.canEdit})}>
                <Link to={owner.url} title={owner.name} style={{backgroundImage: `url(${owner.icon})`}} className="card-blog-post__picture" />
                <div className="card-blog-post__post">
                    {entity.canEdit &&
                        editButton
                    }
                    {meta}
                    {content}
                </div>

                {actions}

                {this.state.commentsVisible &&
                    <CardComments
                        entity={entity}
                        viewer={viewer}
                    />
                }
            </div>
       )
    }
}

const Mutation = gql`
    mutation StatusUpdateCard($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on Object {
                    title
                    description
                    richDescription
                    url
                    accessId
                    writeAccessId
                    source
                    isBookmarked
                    canBookmark
                    isFeatured
                    isRecommended
                    featured {
                        image
                        video
                        positionY
                    }
                    startDate
                    endDate
                    tags
                }
            }
        }
    }
`

export default graphql(Mutation)(StatusUpdateCard)
