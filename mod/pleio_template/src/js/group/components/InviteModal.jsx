import React from "react"
import Modal from "../../core/components/NewModal"
import InviteAutoComplete from "./InviteAutoComplete"
import InviteList from "./InviteList"
import InvitedList from "./InvitedList"
import autobind from "autobind-decorator"
import { Set } from "immutable"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import { logErrors } from "../../lib/helpers"
import Errors from "../../core/components/Errors"
import Tabber from "../../core/components/Tabber"
import classnames from "classnames"
import SwitchField from "../../core/components/SwitchField"

class InviteForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: new Set(),
            directAdd: false,
            hasUserWithEmailOnly: false,
            loading: false
        }
    }

    onSelect = (user) => {
        let newState = { completed: false, users: this.state.users.add(user) }

        if (!this.state.hasEmail && !user.guid) {
            newState.directAdd = false;
            newState.hasUserWithEmailOnly = true;
        }

        this.setState(newState)
    }

    onDeselect = (user) => {
        let newState = { users: this.state.users.delete(user) }

        if (this.state.hasEmail) {
            let hasEmail = newState.users.find(user => {
                return !user.guid
            })

            if (!hasEmail) {
                newState.hasEmail = false
            }
        }

        this.setState(newState)
    }

    onSubmit = (e) => {
        e.preventDefault()

        if (this.state.users.size === 0) {
            return
        }

        const { group } = this.props

        const input = {
            clientMutationId: 1,
            guid: group.guid,
            directAdd: this.state.directAdd,
            users: this.state.users.toJS().map((user) => ({guid: user.guid, email: user.email}))
        }

        this.setState({ loading: true })

        this.props.mutate({
            variables: {
                input
            },
            refetchQueries: ["InvitedList"]
        }).then(({data}) => {
            this.setState({
                users: new Set(),
                completed: true,
                loading: false
            })
        }).catch((errors) => {
            logErrors(errors)
            this.setState({
                errors: errors,
                loading: false
            })
        })
	}

	updateDirectAdd = (selected) => {
		this.setState({ directAdd: selected})
	}

    render () {
        const { group, viewer } = this.props

        let list
        if (this.state.completed) {
            list = (
                <div style={{paddingTop:"1em"}}>
                    De gebruikers zijn succesvol uitgenodigd.
                </div>
            )
        } else {
            list = (
                <InviteList group={group} users={this.state.users} onDeselect={this.onDeselect} />
            )
        }

        let button
        if (this.state.loading) {
            button = (
                <button className={classnames({"button": true, "___is-loading": true})} type="submit" disabled={this.state.users.size === 0}>
                    Uitnodigen
                    <div className="button__loader"></div>
                </button>
            )
        } else {
            button = (
                <button className={classnames({"button": true})} type="submit" disabled={this.state.users.size === 0}>
                    Uitnodigen
                </button>
            )
		}

		let directAddSwitch
		if (viewer && viewer.isAdmin) {
			directAddSwitch = (
				<SwitchField disabled={this.state.hasUserWithEmailOnly} name="directAdd" onChange={this.updateDirectAdd} type="text" label="Voeg de gebruikers direct toe aan de groep" />
			)
		}

        return (
            <form method="POST" onSubmit={this.onSubmit}>
                <Errors errors={this.state.errors} />
                <InviteAutoComplete group={group} onSelect={this.onSelect} />
				{list}
				{directAddSwitch}
                <div className="buttons ___end ___margin-top">
                    {button}
                </div>
            </form>
        )
    }
}

const Mutation = gql`
    mutation InviteItem($input: inviteToGroupInput!){
      inviteToGroup(input: $input) {
        group {
            ... on Group {
                guid
            }
        }
      }
    }
`

const InviteFormWithMutation = graphql(Mutation)(InviteForm)


export default class InviteModal extends React.Component {
    toggle = () => {
        this.refs.modal.toggle()
    }

    render() {
        const items = [
            { title: "Nieuwe leden", content: <InviteFormWithMutation group={this.props.entity} viewer={this.props.viewer} /> },
            { title: "Reeds uitgenodigd", content: <InvitedList group={this.props.entity} /> }
        ]

        return (
            <Modal ref="modal" title="Leden uitnodigen">
                <div className="group-info">
                    <div className="group-info__content">
                        <Tabber items={items} />
                    </div>
                </div>
            </Modal>
        )
    }
}
