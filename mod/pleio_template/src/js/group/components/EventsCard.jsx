import React from "react"
import Accordeon from "../../core/components/Accordeon"
import { showDateTime } from "../../lib/showDate"
import { Link } from "react-router-dom"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

class EventsCard extends React.Component {
    render() {
        const { data } = this.props

        if (!data.events || data.events.edges.length === 0) {
            return (
                <div />
            )
        }

        const items = data.events.edges.map((entity) => (
            <Link key={entity.guid} to={entity.url} className="card__item">
                <strong className="___colored">{entity.title}</strong>
                <div className="card-topic__meta">{showDateTime(entity.startDate)}</div>
            </Link>
        ))

        return (
            <Accordeon title="Agenda">
                {items}
                <div className="card__bottom">
                    <Link to={`${this.props.entity.url}/events`} className="read-more">
                        <div className="read-more__circle" />
                        <span>Alles</span>
                    </Link>
                </div>
            </Accordeon>
        )
    }
}

const Query = gql`
    query EventsCard($containerGuid: Int!) {
        events(containerGuid: $containerGuid, offset: 0, limit: 5) {
            edges {
                ... on Object {
                    guid
                    title
                    startDate
                    url
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                containerGuid: ownProps.entity.guid
            }
        }
    }
}

export default graphql(Query, Settings)(EventsCard)