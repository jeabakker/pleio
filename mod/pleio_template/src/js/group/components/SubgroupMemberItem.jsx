import React from "react"
import { graphql } from "react-apollo"
import gql from "graphql-tag"
import CheckField from "../../core/components/CheckField"
import Errors from "../../core/components/Errors"

class SubgroupMemberItem extends React.Component {
    constructor(props) {
        super(props)

        const { member } = this.props

        this.state = {
            role: member.role,
            errors: []
        }
    }

    componentWillUpdate(nextProps) {
        if (nextProps !== this.props) {
            this.setState({ role: nextProps.member.role })
        }
    }

    render() {
        const { member, selectable } = this.props

        return (
            <div className="list-members__member">
                <Errors errors={this.state.errors} />
                {selectable &&
                    <CheckField
                        onChange={(e) => this.props.onSelect(e, member.user.guid)}
                        checked={this.props.selected}
                    />
                }
                <a className="list-members__link" href={member.user.url}>
                    <div style={{backgroundImage: `url('${member.user.icon}')`}} className="list-members__picture" />
                    <div className="list-members__name"><b>{member.user.name}</b></div>
                </a>
            </div>
        )
    }
}

const Mutation = gql`
    mutation MemberItem($input: changeGroupRoleInput!){
      changeGroupRole(input: $input) {
        group {
            ... on Group {
                guid
            }
        }
      }
    }
`

export default graphql(Mutation)(SubgroupMemberItem)