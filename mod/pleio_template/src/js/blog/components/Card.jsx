import React from "react"
import { Link } from "react-router-dom"
import classnames from "classnames"
import showDate from "../../lib/showDate"
import Featured from "../../core/components/Featured"
import Bookmark from "../../bookmarks/components/Bookmark"
import CardComments from "../../core/components/CardComments"

export default class Card extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            commentsVisible: false
        }
    }

    render() {
        const { entity, viewer, inActivityFeed } = this.props
        const { owner } = entity

        const commentCount = (
            <button
                className={classnames({
                    'count-comments button__text': true,
                    '___show-comments': this.state.commentsVisible
                })}
                onClick={() => this.setState({commentsVisible: !this.state.commentsVisible})}
            >
                {entity.commentCount} {(entity.commentCount === 1) ? " reactie" : " reacties"}
            </button>
        )

        const icon = (
            <Link to={owner.url} title={owner.name} style={{backgroundImage: `url(${owner.icon})`}} className="card-blog-post__picture" />
        )

        const meta = (
            <div className="card-topic__meta">
                <span>
                    Blog gemaakt door&nbsp;
                </span>
                <Link to={entity.owner.url} className="card-topic__user">
                    {entity.owner.name}
                </Link>
                <span> op {showDate(entity.timeCreated)}</span>
                {entity.group &&
                    <span> in <Link to={entity.group.url}>{entity.group.name}</Link></span>
                }
            </div>
        )

        return (
            <div className="card-blog-post">
                {icon}
                <div className="card-blog-post__post">
                    <Link to={entity.url} className="card-blog-post__title">
                        {entity.title}
                    </Link>
                    {meta}
                    <Featured entity={entity} inCard="blog" to={entity.url} />

                    <div className="card-blog-post__content">
                        {entity.excerpt}
                    </div>
                </div>

                <div className="card-blog-post__actions">
                    <Bookmark entity={entity} />
                    {commentCount}
                </div>

                {this.state.commentsVisible &&
                    <CardComments
                        entity={entity}
                        viewer={viewer}
                        inActivityFeed={inActivityFeed}
                    />
                }
            </div>
       )
    }
}