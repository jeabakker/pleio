import React from "react"
import { Link } from "react-router-dom"
import { graphql, gql, withApollo } from "react-apollo"
import classnames from "classnames"
import autobind from "autobind-decorator"
import { withRouter } from "react-router-dom"
import { timeSince } from "../../lib/showDate"

class Notification extends React.Component {
    navigateTo = (e, url) => {
        e.preventDefault()

        const { notification, client } = this.props

        if (notification.isUnread) {
            this.props.mutate({
                variables: {
                    input: {
                        clientMutationId: "1",
                        id: notification.id
                    }
                }
            }).then(() => {
                window.location = url
            })
        } else {
            this.props.history.push(url)
        }
    }

    render() {
        const { notification } = this.props

        let url, message

        switch (notification.action) {
            case "commented":
                if (!notification.entity) {
                    return (
                        <div />
                    )
                }

                url = notification.entity.url

                let title
                if (notification.entity.title) {
                    title = notification.entity.title
                } else if (notification.entity.name) {
                    title = notification.entity.name
                } else {
                    title = 'een statusupdate'
                }


                message = (
                    <div>
                        <strong>{notification.performer.name}</strong>
                        &nbsp;heeft gereageerd op&nbsp;
                        {title}
                        &nbsp;&nbsp;<span className="___greyed">{timeSince(notification.timeCreated)}</span>
                    </div>
                )
                break
            case "created":
                if (!notification.entity || !notification.container) {
                    return (
                        <div />
                    )
                }
                url = notification.entity.url

                let created_title
                if (notification.entity.title) {
                    created_title = "'" + notification.entity.title + "'"
                } else if (notification.entity.name) {
                    created_title = "'" + notification.entity.name + "'"
                } else {
                    created_title = ''
                }

                let subtype
                if (notification.entity.subtype == 'blog') {
                    subtype = 'heeft een nieuw Blog gemaakt'
                } else if (notification.entity.subtype === 'discussion') {
                    subtype = 'is een nieuwe Discussie gestart'
                } else if (notification.entity.subtype === 'event') {
                    subtype = 'heeft een nieuw Agenda-item gemaakt'
                } else if (notification.entity.subtype === 'thewire') {
                    subtype = 'heeft een Update geplaatst'
                } else if (notification.entity.subtype === 'question') {
                    subtype = 'heeft een Vraag gesteld'
                }  else {
                    subtype = 'heeft een item geplaatst'
                }

                message = (
                    <div>
                        <strong>{notification.performer.name}</strong>
                        &nbsp;{subtype}
                        &nbsp;{created_title}
                        &nbsp;in
                        &nbsp;<strong>{notification.container.name}</strong>
                        &nbsp;&nbsp;<span className="___greyed">{timeSince(notification.timeCreated)}</span>
                    </div>
                )
                break
            case "welcome":
                url = `/profile/${notification.performer.username}/interests`
                message = "Welkom op deze site. Klik hier om je meldingen in te stellen."
                break
        }

        return (
            <a className={classnames({ notification: true, "___is-unread": notification.isUnread })} onClick={(e) => this.navigateTo(e, url)} href={url}>
                <div className="face" style={{ backgroundImage: `url('${notification.performer.icon}')` }} />
                {message}
            </a>
        )
    }
}

const Mutation = gql`
    mutation Notification($input: markAsReadInput!) {
        markAsRead(input: $input) {
            success
            notification {
                id
                isUnread
            }
        }
    }
`

export default graphql(Mutation)(withRouter(withApollo(Notification)))