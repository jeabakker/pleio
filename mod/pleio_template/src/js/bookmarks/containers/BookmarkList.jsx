import React from "react"
import InfiniteList from "../../core/components/InfiniteList"
import { graphql } from "react-apollo"
import gql from "graphql-tag"

const Query = gql`
    query BookmarkList($offset: Int!, $limit: Int!, $tags: [String], $subtype: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
        }
        bookmarks(offset: $offset, limit: $limit, tags: $tags, subtype: $subtype) {
            total
            edges {
                guid
                ... on Wiki {
                    title
                    url
                    isBookmarked
                    canBookmark
                    timeCreated
                    group {
                        url
                        name
                    }
                }
                ... on Object {
                    guid
                    title
                    subtype
                    url
                    excerpt
                    description
                    startDate
                    endDate
                    votes
                    hasVoted
                    isBookmarked
                    canBookmark
                    tags
                    isFeatured
                    featured {
                        image
                        video
                        positionY
                    }
                    timeCreated
                    views
                    canEdit
                    commentCount
                    comments {
                        guid
                        description
                        richDescription
                        timeCreated
                        canEdit
                        votes
                        hasVoted
                        owner {
                            guid
                            username
                            name
                            icon
                            url
                        }
                    }
                    owner {
                        guid
                        name
                        icon
                        url
                    }
                }
            }
        }
    }
`

const withQuery = graphql(Query)
export default withQuery(InfiniteList)