import React from "react"
import { Link } from "react-router-dom"
import { getVideoFromUrl, getVideoThumbnail, getClassFromTags, getFirstValueFromSet } from "../../lib/helpers"
import showDate from "../../lib/showDate"
import VideoModal from "../../core/components/VideoModal"
import classnames from "classnames"
import Truncate from 'react-truncate';
import Bookmark from "../../bookmarks/components/Bookmark"
import CardComments from "../../core/components/CardComments"

export default class Card extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            commentsVisible: false
        }

        this.playVideo = this.playVideo.bind(this)
    }

    playVideo(e) {
        e.preventDefault()
        if (this.refs.video) {
            this.refs.video.onToggle()
        }
    }

    render() {
        const { entity, viewer, inActivityFeed } = this.props
        const { featured } = entity

        let backgroundImage = ""
        if (featured.image) {
            backgroundImage = featured.image
        } else if (featured.video) {
            backgroundImage = getVideoThumbnail(featured.video)
        }

        const imageStyle = {
            "backgroundImage": `url(${backgroundImage})`,
            "backgroundPositionY": featured.positionY + "%"
        }

        let videoModal
        if (featured.video) {
            const video = getVideoFromUrl(featured.video)
            videoModal = video && <VideoModal ref="video" id={video.id} type={video.type} />
        }

        let playButton
        if (featured.video && videoModal) {
            playButton = (
                <button className="play-button" onClick={this.playVideo} />
            )
        }

        let commentCount
        if (inActivityFeed) {
            commentCount = (
                <button
                    className={classnames({
                        'count-comments button__text': true,
                        '___show-comments': this.state.commentsVisible
                    })}
                    onClick={() => this.setState({commentsVisible: !this.state.commentsVisible})}
                >
                    {entity.commentCount} {(entity.commentCount === 1) ? " reactie" : " reacties"}
                </button>
            )
        }
        else {
            commentCount = (
                <Link to={entity.url}
                    className={classnames({
                        'count-comments button__text': true
                    })}
                >
                    {entity.commentCount} {(entity.commentCount === 1) ? " reactie" : " reacties"}
                </Link>
            )
        }

        const actions = (
            <div className="card-topic__actions">
                <Bookmark entity={entity} />
                {commentCount}
            </div>
        )

        let tags
        if (window.__SETTINGS__['hasPredefinedTags']) {
            tags = (
                <div className="card__subject">
                    {getFirstValueFromSet(entity.tags, window.__SETTINGS__['predefinedTags'].map((predefinedTag) => predefinedTag.tag))}
                </div>
            )
        }

        let card
        if (!window.__SETTINGS__['showExcerptInNewsCard']) {
            card = (
                <div className={classnames({
                    "card": true,
                    "___has-image": backgroundImage,
                    "___is-highlighted": inActivityFeed && entity.isHighlighted,
                    [getClassFromTags(entity.tags)]: true
                })}>
                    <div to={entity.url} className="card__image" style={backgroundImage ? imageStyle : {}}>
                        <div className="card__content">
                            <div className="flexer ___space-between">
                                <div className="card-topic__meta">
                                    {featured.video &&
                                        <button className="card__video" onClick={this.playVideo} />
                                    }
                                    {showDate(entity.timeCreated)}
                                </div>
                                {tags}
                            </div>
                            <div>
                            <Link to={entity.url} className={classnames({
                                "card__title": true,
                                "___with-video": featured.video
                            })}>
                                <Truncate
                                lines={3}
                                ellipsis={(
                                    <span>...</span>
                                    )}
                                    trimWhitespace
                                    >
                                    {entity.title}
                                </Truncate>
                            </Link>
                                </div>
                        </div>
                    </div>
                    <div className="card__footer">
                        {actions}
                        {inActivityFeed && this.state.commentsVisible &&
                            <CardComments
                                entity={entity}
                                viewer={viewer}
                            />
                        }
                    </div>
                    {videoModal}
                </div>
            )
        } else {
            card = (
                <div className={classnames({
                    "card": true,
                    "___has-image": backgroundImage,
                    "___is-highlighted": inActivityFeed && entity.isHighlighted,
                    [getClassFromTags(entity.tags)]: true
                })}>
                    <div className="card__header" style={backgroundImage ? imageStyle : {}}>
                        {playButton}
                        <div className="card__content">
                            <div className="card__subject">{tags}</div>
                        </div>
                    </div>
                    <div className="card__content">
                        <Link to={entity.url} className="card__title">
                            <Truncate
                                lines={2}
                                ellipsis={(
                                    <span>...</span>
                                )}
                                trimWhitespace
                            >
                                {entity.title}
                            </Truncate>
                        </Link>
                        <div className="card-topic__meta">
                            <span>
                                Nieuws geplaatst op {showDate(entity.timeCreated)}
                            </span>
                        </div>
                        <p className="card__excerpt">
                            <Truncate
                                lines={3}
                                ellipsis={(
                                    <span>...</span>
                                )}
                                trimWhitespace
                            >
                                {entity.excerpt}
                            </Truncate>
                        </p>
                        {actions}
                        {inActivityFeed && this.state.commentsVisible &&
                            <CardComments
                                entity={entity}
                                viewer={viewer}
                            />
                        }
                    </div>
                    {videoModal}
                </div>
            )
        }

        if (inActivityFeed) {
            return (
                card
            )
        }
        else {
            return (
                <div className={entity.isHighlighted ? "col-sm-12 col-lg-8" : "col-sm-6 col-lg-4"}>
                    {card}
                </div>
            )
        }
    }
}
