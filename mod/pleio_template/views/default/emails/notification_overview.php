<?php
$notifications = elgg_extract("notifications", $vars);
$site = elgg_extract("site", $vars);
?>

Je hebt nieuwe notificaties ontvangen op <?php echo $site->name; ?>. Ga naar <?php echo $site->url; ?> om de details te bekijken.

<?php foreach ($notifications as $notification): ?><?php
        $subtype = get_subtype_from_id($notification['entity']->subtype);
        $item = "heeft een item geplaatst";
        if ($subtype === 'blog') {
            $item = 'heeft een nieuw Blog gemaakt';
        } elseif ($subtype === 'discussion') {
            $item = 'is een nieuwe Discussie gestart';
        } elseif ($subtype === 'event') {
            $item = 'heeft een nieuw Agenda-item gemaakt';
        } elseif ($subtype === 'thewire') {
            $item = 'heeft een Update geplaatst';
        } elseif ($subtype === 'question') {
            $item = 'heeft een Vraag gesteld';
        }

    echo "<hr style=\"border:none;border-bottom:1px solid #ececec;margin:1.5rem 0;width:100%\">";
    if ($notification['action'] === "created") {
        echo "<b>{$notification['performer']->name}</b> {$item}: <b>{$notification['entity']->title}</b> in <b>{$notification['container']->name}</b>.";
    } elseif ($notification['action'] === "commented") {
        echo "<b>{$notification['performer']->name}</b> heeft gereageerd op <b>{$notification['entity']->title}</b>.";
    }
?><?php endforeach; ?>