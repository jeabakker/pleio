<?php
function pleio_template_plugins_settings_hook($hook, $type, $return_value, $params) {
    $site = elgg_get_site_entity();
    $plugin_id = get_input("plugin_id");

    if ($plugin_id !== "pleio_template") {
        return $return_value;
    }

    $name = get_input("filterName", []);
    $values = get_input("filterValues", []);
    $required = get_input("filterRequired", []);

    $menu = get_input("menu", []);

    $directLinksTitle = get_input("directLinksTitle", []);
    $directLinksLink = get_input("directLinksLink", []);

    $profileKey = get_input("profileKey", []);
    $profileName = get_input("profileName", []);

    $directLinks = [];
    foreach ($directLinksLink as $i => $link) {
        $directLinks[] = [
            "title" => $directLinksTitle[$i],
            "link" => $directLinksLink[$i]
        ];
    }

    $profile = [];
    foreach ($profileKey as $i => $key) {
        if (in_array($profileKey[$i], ["guid", "type", "subtype", "owner_guid", "site_guid", "container_guid", "access_id", "time_created", "time_updated", "last_action", "enabled", "name", "username", "password", "salt", "password_hash", "email", "language", "code", "banned", "admin", "last_action", "prev_last_action", "last_login", "prev_last_login"])) {
            continue;
        }

        if (preg_match("/^a-z/", $profileKey[$i])) {
            continue;
        }

        $profile[] = [
            "key" => $profileKey[$i],
            "name" => $profileName[$i]
        ];
    }

    $filters = [];
    foreach ($name as $i => $name) {
        if (!$name || !$values[$i]) {
            continue;
        }

        $filters[] = [
            "name" => $name,
            "required" => ($required[$i] === "yes") ? true : false,
            "values" => $values[$i]
        ];
    }

    $footerTitle = get_input("footerTitle", []);
    $footerLink = get_input("footerLink", []);
    $predefinedTags = get_input("predefinedTags", []);

    $footer = [];
    foreach ($footerLink as $i => $link) {
        $footer[] = [
            "title" => $footerTitle[$i],
            "link" => $footerLink[$i]
        ];
    }

    $params = get_input("params");
    $params["menu"] = json_encode($menu);
    $params["directLinks"] = json_encode($directLinks);
    $params["profile"] = json_encode($profile);
    $params["filters"] = json_encode($filters);
    $params["predefinedTags"] = json_encode($predefinedTags);
    $params["footer"] = json_encode($footer);
    set_input("params", $params);

    $file = new \ElggFile();
    $file->owner_guid = $site->guid;
    $file->access_id = ACCESS_PUBLIC;
    $file->setFilename("pleio_template/{$site->guid}_logo.jpg");

    $logo = get_resized_image_from_uploaded_file("logo", 404, 231, false, true);
    $remove_logo = get_input("remove_logo");

    if ($logo) {
        $file->open("write");
        $file->write($logo);
        $file->close();

        $site->logotime = time();
        $site->save();
    } elseif ($remove_logo === "1") {
        $file->delete();

        unset($site->logotime);
        $site->save();
    }
}

function pleio_template_permissions_check_hook($hook_name, $entity_type, $return_value, $parameters) {
    $user = $parameters['user'];
    $entity = $parameters['entity'];

    if (!$user | !$entity) {
        return $return_value;
    }

    if (!$entity instanceof ElggObject) {
        return $return_value;
    }

    $subtype = $entity->getSubtype();

    switch ($subtype) {
        case "news":
        case "page":
        case "row":
        case "page_widget":
            return ($user->isAdmin() || pleio_template_is_subeditor($user));
        case "file":
        case "folder":
        case "wiki":
            return pleio_template_extend_write_array($return_value, $entity, $user);
        case "default":
            return $return_value;
    }
}

function pleio_template_container_permissions_check_hook($hook, $type, $return_value, $params) {
    $user = elgg_extract("user", $params);
    $container = elgg_extract("container", $params);
    $subtype = elgg_extract("subtype", $params);

    if (!$user) {
        return $return_value;
    }

    switch ($subtype) {
        case "news":
        case "page":
        case "row":
        case "page_widget":
            return ($user->isAdmin() || pleio_template_is_subeditor($user));
        case "default":
            return $return_value;
    }
}

function pleio_template_user_hover_hook($hook, $type, $return_value, $params) {
    $result = $return_value;

    $site = elgg_get_site_entity();
    $user = elgg_extract("entity", $params);

    if (check_entity_relationship($user->guid, "is_subeditor", $site->guid)) {
        $text = elgg_echo("pleio_template:toggle_subeditor:revoke");
    } else {
        $text = elgg_echo("pleio_template:toggle_subeditor:grant");
    }


    if ($user->isAdmin()) {
        // admins are always subeditor
        return $result;
    }

    $result[] = ElggMenuItem::factory(array(
        "name" => "toggle_subeditor",
        "text" => $text,
        "href" => "action/admin/toggle_subeditor?user_guid={$user->guid}",
        "confirm" => elgg_echo("pleio_template:toggle_subeditor:confirm")
    ));

    return $result;
}
