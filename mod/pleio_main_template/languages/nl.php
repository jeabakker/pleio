<?php
/**
 * Theme Dutch language file
 */

$dutch = array(
	'subsites' => 'Deelsites',
	'pleio_main_template:menu:sidebar:top' => 'Naar boven',
	'pleio_main_template:menu:sidebar:toggle' => 'Klap menu in',
	'pleio_main_template:menu:tools' => "Activiteiten",
	'pleio_main_template:menu:share' => "Bestanden delen",
  'pleio_main_template:menu:learn' => "Leren",
	'pleio_main_template:menu:colofon' => "Colofon",
	'pleio_main_template:personal_menu:title' => 'Mijn menu',
	'pleio_main_template:personal_menu:add' => 'Voeg huidige pagina toe aan menu',
	'pleio_main_template:personal_menu:unable' => "Kan huidige pagina niet aan het menu toevoegen",
	'pleio_main_template:personal_menu:missing_input' => "Het is niet mogelijk om het menu item aan te maken want er ontbreken gegevens.",
	'pleio_main_template:registered' => "Je bent succesvol geregistreerd. Controleer je e-mail en volg de activatielink om je account te activeren.",
	'pleio_main_template:accept_terms' => "Je dient de voorwaarden van Pleio te accepteren.",
	'pleio_main_template:forgotpassword:requested' => "We hebben een e-mail gestuurd met een speciale link. Volg deze link om een nieuw wachtwoord in te stellen.",
	'pleio_main_template:login_using' => "Inloggen met",
	'pleio_main_template:i_agree_with' => "Ik ga akkoord met de",
	'pleio_main_template:change_password' => "Verander wachtwoord",
	'pleio_main_template:reset_password_confirm' => "Kies een nieuw wachtwoord voor je account en bevestig het daarna nogmaals.",
	'pleio_main_template:passwordreset:subject' => "Wachtwoord aangepast.",
	'pleio_main_template:passwordreset:email' => "Hallo %s,

	Het wachtwoord van je account is succesvol aangepast.

	Heb je jouw wachtwoord niet aangepast? Neem dan contact op met een van de beheerders van www.pleio.nl.",
	'pleio_main_template:resetpassword:changed' => "Het wachtwoord van je account is succesvol aangepast. Log nu in met je nieuwe wachtwoord om door te gaan.",
	'pleio_main_template:resetpassword:link_expired' => "De link om een nieuw wachtwoord aan te vragen is verlopen. Vraag een nieuw wachtwoord aan.",
	'pleio_main_template:terms' => "voorwaarden",
	'pleio_main_template:subscribe_newsletter' => "Ik wil de nieuwsbrief ontvangen",
	'pleio_main_template:or' => "of",
	'pleio_main_template:sent' => 'Bericht verzonden',
	'pleio_main_template:fill_all_fields' => 'Vul alle velden in',
	'pleio_main_template:registration:explanation' => 'Vul de onderstaande velden in om een account aan te maken op Pleio.',
	'pleio_main_template:welcome' => 'Welkom op Pleio. Vul je gegevens in om in te loggen.',
	'pleio_main_template:principles' => 'Uitgangspunten',
	'pleio_main_template:examples' => 'Voorbeelden',
	'pleio_main_template:organisation' => 'Organisatie',
	'pleio_main_template:help' => 'Help',
	'pleio_main_template:subsites' => 'Deelsites',
	'pleio_main_template:collaborating' => 'Samenwerken aan de publieke zaak',
	'pleio_main_template:intro' => 'Pleio is een online samenwerkingsplatform voor alle overheidsorganisaties en bestuurslagen in Nederland. Gebruikers kunnen met hun eigen account inloggen op Pleio-deelsites, waar samenwerking, online ontmoetingen en uitwisseling van kennis plaatsvinden. Pleio heeft ruim <b>388.000</b> gebruikers en meer dan <b>400</b> deelsites. Onder andere de Belastingdienst, het Ministerie van Economische Zaken, het Nationaal Archief, het Samenwerkingsplatform Provincies en diverse gemeenten maken gebruik van Pleio.',
	'pleio_main_template:open_source' => 'Open source en hergebruik',
	'pleio_main_template:open_source:desc' => 'Pleio is gebouwd als open source software en gebruikt open standaarden. Dat betekent dat overheden de ontwikkelde software gratis kunnen hergebruiken, bijvoorbeeld bij het ontwikkelen van intranetvoorzieningen, community’s, projectomgevingen, evenementen en eenvoudige websites.',
	'pleio_main_template:development' => 'Ontwikkeling',
	'pleio_main_template:development:desc' => 'Pleio bouwt regelmatig nieuwe functionaliteiten. Zo zal in de nabije toekomst de optie tot twee-factor-authenticatie worden aangeboden. De ontwikkelaars van Pleio werken samen met ontwikkelaars van de <a href="https://www.canada.ca/en/treasury-board-secretariat/campaigns/gctools-hackathon/gctools.html">Canadese overheid</a> aan gemeenschappelijke componenten die zowel door Pleio als Canadese overheidsportal kunnen worden ingezet.',
	'pleio_main_template:accessible' => 'Gemakkelijk, veilig en betrouwbaar',
	'pleio_main_template:accessible:desc' => 'Pleio is toegankelijk en eenvoudig te gebruiken. Pleio wordt gehost op servers die in Nederland staan en dus zijn onderworpen aan de Nederlandse en Europese privacywetgeving. Alle communicatie via het platform wordt standaard versleuteld. Het platform heeft een beschikbaarheidspercentage van meer dan 99,9%.',
	'pleio_main_template:across_borders' => 'Over grenzen heen',
	'pleio_main_template:across_borders:desc' => 'Dankzij Pleio kunnen ambtenaren niet alleen met andere ambtenaren binnen en buiten de eigen overheidsorganisatie samenwerken, maar ook met bedrijven en burgers. Bovendien is het gemakkelijk om deskundigen op specifieke expertisegebieden binnen andere overheidsorganisaties te vinden, waarmee Pleio samenwerking tussen verschillende overheidsorganisaties bevordert.',
	'pleio_main_template:examples_subsites' => 'Voorbeelden van deelsites',
	'pleio_main_template:examples_subsites:desc' => 'Enkele veelbezochte deelsites zijn Leraar.nl, Forum Fiscaal Dienstverleners en Forum Salaris. <a href="https://www.leraar.nl">Leraar.nl</a> is een community voor docenten die geïnitieerd is vanuit het Ministerie van Onderwijs. Via de community <a href="https://ffd.pleio.nl">Forum Fiscaal Dienstverleners</a> kunnen accountants en belastingadviseurs vragen aan elkaar en aan de Belastingdienst stellen. <a href="https://fsa.pleio.nl">Forum Salaris</a> ten slotte is een samenwerkingsomgeving met betrekking tot de aangifte Loonheffingen van de Belastingdienst en het UWV.',
	'pleio_main_template:organisation_pleio' => 'Organisatie van Pleio',
	'pleio_main_template:organisation_pleio:desc' => '<p>De beheer- en ontwikkelorganisatie achter het platform is Stichting Pleio. Deze organisatie zorgt ervoor dat Pleio up to date blijft door nieuwe releases uit te brengen en aanpassingen zeer snel uit te rollen. Daarnaast verzorgt de stichting de communicatie naar gebruikers en deelsitebeheerders, onder andere door best practices en handreikingen uit te brengen, bijeenkomsten te organiseren en hulp en ondersteuning te bieden.</p>
	<p>Pleio is in 2010 opgericht door een groep ambtenaren vanuit de Belastingdienst en Ambtenaar 2.0 en is in de loop der jaren uitgegroeid tot een rijksbreed samenwerkingsplatform. De stichting wordt bestuurd door ambtenaren uit verschillende overheidsorganisaties.</p>',
	'pleio_main_template:help_support' => 'Hulp en ondersteuning',
	'pleio_main_template:help_support:desc' => '<p>Via de helpdesk <a href="mailto:support@pleio.nl">support@pleio.nl</a> worden vragen op het gebied van beheer en ontwikkeling beantwoord. Op de <a href="https://mijndeelsite.pleio.nl/">online helppagina</a> biedt de community onder meer best practices en handreikingen. Ervaringen over Pleio worden online uitgewisseld, maar ook offline bij de 2-maandelijkse bijeenkomsten voor deelsitebeheerders.</p>',
	'pleio_main_template:request_subsite' => 'Deelsite aanvragen',
	'pleio_main_template:request_subsite:desc' => '<p>Overheidsorganisaties kunnen een eigen deelsite aanmaken en kunnen deze vervolgens vrij uitbreiden. De deelsitebeheerder krijgt toegang tot een set van basisfunctionaliteiten, waaronder een helpdeskfunctie, instructies en templates voor een intranetsites, community’s en websites. Uiteraard verzorgt Pleio de hosting.</p>
	<p>Nieuwe gebruikers kunnen deelnemen aan de kleinschalige startersbijeenkomsten (maximaal vijf personen). In 1 tot 2 dagdelen behandelt een medewerker Gebruikersondersteuning van Pleio alle mogelijkheden en neemt gebruikers stap voor stap mee in het inrichten van een deelsite.</p>
	<p>Overstappen van Pleio naar een andere partij is eenvoudig. Op verzoek ontvangt de deelsitebeheerder kosteloos een complete back-up.</p>
	<p>Het gebruik van het platform is het eerste jaar gratis. Daarna bedragen de jaarlijkse kosten voor een standaard site € 2.250 per jaar. Er worden geen overstap- en uitfaseerkosten in rekening gebracht.</p>',
	'pleio_main_template:more_info' => 'Vul het onderstaande formulier in om meer informatie over Pleio aan te vragen.',
	'pleio_main_template:request_more_info' => 'Vraag meer informatie aan',
	'pleio_main_template:or_send_an_email' => 'of stuur een e-mail naar <a href="mailto:support@pleio.nl">support@pleio.nl</a>',
	'pleio_main_template:jobs' => 'Werken bij Pleio',
    'pleio_main_template:notification:explain' => "Je ontvangt deze e-mail omdat je aangemeld bent voor de notificaties.",
    'pleio_main_template:notification:link' => "Klik <a href=\"%s\">hier</a> om je instellingen aan te passen."
);

add_translation('nl', $dutch);
